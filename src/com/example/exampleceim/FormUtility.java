package com.example.exampleceim;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

@SuppressWarnings("unused")
public class FormUtility implements OnCheckedChangeListener {

	//hola daniel
	
	public ArrayList <View> campos = new ArrayList<View>();
	
	private ArrayList <FormObject> values = new ArrayList<FormObject>();
	
	public JSONArray jsonFill = new JSONArray();
	
	public static JSONObject recepcionDatosCertificacion = new JSONObject();
	
	public float latitud = 0.0f;
	public float longitud = 0.0f;
	
	public FormUtility() { }
	
	public void setEnabledAllFields(ViewGroup grupo, boolean yes) {
		
		if ( campos.size() ==  0 ) {
		
			this.recorrer(grupo);
		} 
		
		for ( int i =0; i<campos.size(); i++) {
			
			View campo = campos.get(i);
	
			if ( campo instanceof RadioGroup ) {
				
				RadioGroup group = (RadioGroup ) campo;
					
					for ( int b =0; b<group.getChildCount(); b++ ) {
					
						RadioButton button = (RadioButton) group.getChildAt(b);
						
						button.setEnabled(yes);
					} 
				
			} else {
				
				campo.setEnabled( yes );	
			}
		}
	}
	
	
	public boolean validar( ViewGroup grupo ) {
		
		boolean resultado = true;
		
		campos.clear();
		values.clear();
		
		
		this.recorrer(grupo);
		
		for ( int i =0; i<campos.size(); i++) {
			
			View campo = campos.get(i);
			
			if ( campo instanceof EditText ) {
				
				EditText texto = (EditText) campo;
				
				if ( texto.length() == 0 ) {
					
					texto.setHint("Campo Obligatorio");
					
					texto.setHintTextColor(Color.RED);
					
					resultado = false;
					
				} else {
					
					texto.setTextColor( Color.DKGRAY );
					//values.add(  texto.getText().toString() );
				}
			}
			if ( campo instanceof RadioGroup ) {
				
				RadioGroup group = (RadioGroup ) campo;
					
					for ( int b =0; b<group.getChildCount(); b++ ) {
					
						RadioButton button = (RadioButton) group.getChildAt(b);
						
						button.setOnCheckedChangeListener(this);
							
						if ( group.getCheckedRadioButtonId() == -1 ) {
							
							button.setTextColor(Color.RED);
		
							resultado = false;
							
						} else {
							
							button.setTextColor(Color.DKGRAY);
						}
					
					} 
			}	
		}
		
		
		 
		return resultado;
	}
	

	public void recuperarDatos( ViewGroup grupo, JSONArray jsonArray ) throws JSONException {
	
		
		if ( campos.size() ==  0 ) {
			
			this.recorrer(grupo);
		} 
		
		
		values.clear();
		
		for ( int j=0; j<jsonArray.length(); j++ ) {
			
			JSONObject jObj = jsonArray.getJSONObject(j);
			
			FormObject obj = new FormObject();
			
			obj.nombre =  jObj.getString("nombre");
			obj.data =  jObj.getString("data");
			
		}
	
		
		
		for ( int i =0; i<campos.size(); i++) {
			
			View campo = campos.get(i);
	
			if ( campo instanceof RadioGroup ) {
				
				RadioGroup group = (RadioGroup ) campo;
					
					String valor =  this.getItemByCampo(campo.getTag().toString());
				
					for ( int b =0; b<group.getChildCount(); b++ ) {
					
						RadioButton button = (RadioButton) group.getChildAt(b);
					
						if ( button.getText().equals(valor) ) {
							
							button.setSelected(true);
							
						}	
					} 
				
			} else {
				
				EditText texto = (EditText) campo;
				
				texto.setText(  this.getItemByCampo( campo.getTag().toString() )   );
				
			}
		}
	}

	public ArrayList<FormObject> obtenerValores( ViewGroup grupo ) throws JSONException {
		
		campos.clear();
		values.clear();
		
		jsonFill = null;
		
		jsonFill = new JSONArray();
		
		
		
		this.recorrer(grupo);
		
		
	
		for ( int i =0; i<campos.size(); i++) {
			
			View campo = campos.get(i);
			
			if ( campo instanceof EditText ) {
				
				EditText texto = (EditText) campo;
				
				FormObject obj = new FormObject();
				
				obj.nombre =  texto.getTag().toString();
				obj.data =  texto.getText().toString();
				
				JSONObject jobj = new JSONObject();
				jobj.put("nombre", obj.nombre);
				jobj.put("data", obj.data);
				
				values.add(obj);
				jsonFill.put(jobj);
				
			}
			if ( campo instanceof RadioGroup ) {
				
				RadioGroup group = (RadioGroup ) campo;
				
				FormObject obj = new FormObject();
				JSONObject jobj = new JSONObject();
			
				
				
				obj.nombre =  group.getTag().toString();

				jobj.put("nombre", obj.nombre);
					
				for ( int b =0; b<group.getChildCount(); b++ ) {
					
						RadioButton button = (RadioButton) group.getChildAt(b);
						
						if ( group.getCheckedRadioButtonId() != -1 ) {
							
							RadioButton radio =  (RadioButton) group.findViewById( group.getCheckedRadioButtonId() );

							obj.data= radio.getText().toString();
							
							
						} else {
							
							obj.data=  "-1";
						}
				} 
					jobj.put("data", obj.data);
					jsonFill.put(jobj);
					values.add(obj);
			}	
		}
		

		return  values;
		
	}
	
	
	public void prepareToSendData( JSONArray datosPruebasCertificacionEquipos, 	JSONArray datosPruebasCertificacionEmpleados ) throws JSONException {
		
		JSONObject recepcionDatosCertificacion = new JSONObject();
		
		//JSONArray datosPruebasCertificacionEquipos = new JSONArray();
		
		recepcionDatosCertificacion.put( "DatosPruebasCertificacionEquipos", datosPruebasCertificacionEquipos );
		recepcionDatosCertificacion.put( "DatosPruebasCertificacionEmpleados", datosPruebasCertificacionEmpleados );
	}
	
	public JSONObject getJSONEquipo() throws JSONException {
		
			JSONObject recepcionCertificacionEquipo = new JSONObject();
			recepcionCertificacionEquipo.put("idProgramacionCertificacion", 1);
			
			JSONObject equipo = new JSONObject();
			equipo.put("Marca",  this.getItemByCampo("Marca") );
			equipo.put("Modelo",  this.getItemByCampo("Modelo") );
			equipo.put("Ano", this.getItemByCampo("Ano") );
			equipo.put("PlacaPatente", this.getItemByCampo("PlacaPatente"));
			equipo.put("CodigoInterno", this.getItemByCampo("CodigoInterno"));
			equipo.put("Color", this.getItemByCampo("Color"));
			equipo.put("NumeroMotor", this.getItemByCampo("NumeroMotor"));
			equipo.put("NumeroSerie", this.getItemByCampo("NumeroSerie"));
			equipo.put("NumeroFactura", this.getItemByCampo("NumeroFactura"));
			equipo.put("NumeroGuiaDespacho",this.getItemByCampo("PlacaPatente"));
			equipo.put("Capacidad", this.getItemByCampo("Capacidad"));
			equipo.put("Potencia", this.getItemByCampo("Potencia"));
			equipo.put("FechaCertificacion", this.getItemByCampo("FechaCertificacion"));
			
			Log.i("equipo", equipo.toString() );
			
			recepcionCertificacionEquipo.put("Equipo", equipo);
			
			JSONObject certificacionEquipo = new JSONObject();
			certificacionEquipo.put("EmpresaResponsable",  this.getItemByCampo("EmpresaResponsable") );
			certificacionEquipo.put("LugarInspeccion", this.getItemByCampo("LugarInspeccion"));
			certificacionEquipo.put("NPPA", this.getItemByCampo("NPPA"));
			certificacionEquipo.put("Cotizacion", this.getItemByCampo("Cotizacion"));
			certificacionEquipo.put("Item", this.getItemByCampo("Item"));
			certificacionEquipo.put("Horometro", this.getItemByCampo("Horometro"));
			certificacionEquipo.put("Kilometraje", this.getItemByCampo("Kilometraje") );
			certificacionEquipo.put("TipoCertificacion", this.getItemByCampo("TipoCertificacion"));
			certificacionEquipo.put("Distintivo", this.getItemByCampo("Distintivo") );
			certificacionEquipo.put("FechaEntregaDocumentacion", this.getItemByCampo("FechaEntregaDocumentacion"));
			certificacionEquipo.put("FechaAprobacion", this.getItemByCampo("FechaAprobacion"));
			certificacionEquipo.put("EquipoOpera",  this.getItemByCampo("EquipoOpera") );
			certificacionEquipo.put("FechaCertificacion", this.getItemByCampo("FechaCertificacion"));
		
			recepcionCertificacionEquipo.put("CertificacionEquipo", certificacionEquipo);
			
			JSONArray instanciaCertificacionEquipos = new JSONArray();
			
			JSONObject instanciaCertificacionEquipo = new JSONObject();
			instanciaCertificacionEquipo.put("NumeroInstanciaCertificacion", this.getItemByCampo("NumeroInstanciaCertificacion"));
			instanciaCertificacionEquipo.put("FechaInstancia", this.getItemByCampo("FechaInstancia"));
			instanciaCertificacionEquipo.put("Lugar", this.getItemByCampo("Lugar"));
			instanciaCertificacionEquipo.put("SituacionAprobacion", this.getItemByCampo("SituacionAprobacion") );
			instanciaCertificacionEquipo.put("ObservacionSituacionEquipo", this.getItemByCampo("ObservacionSituacionEquipo"));
			instanciaCertificacionEquipo.put("OperadorEquipoCertificador",  this.getItemByCampo("OperadorEquipoCertificador")  );
			instanciaCertificacionEquipo.put("ObservacionFinal",  this.getItemByCampo("ObservacionFinal"));
			instanciaCertificacionEquipo.put("ResultadoCertificacion",  this.getItemByCampo("ResultadoCertificacion"));
			instanciaCertificacionEquipo.put("Latitud", this.latitud );
			instanciaCertificacionEquipo.put("Longitud", this.longitud );
			
			JSONArray imagenesInstanciaEquipo = new JSONArray();
			
			for ( int img = 0; img<5; img++) {
				
				JSONObject imagenInstanciaEquipo = new JSONObject();
				
				imagenInstanciaEquipo.put("IdTipoImagenInstanciaEquipo", 1);
				imagenInstanciaEquipo.put("Nombre", "string");
				imagenInstanciaEquipo.put("Imagen", "bytes");
				
				imagenesInstanciaEquipo.put(imagenInstanciaEquipo);
			}
			
			instanciaCertificacionEquipo.put("ImagenesInstanciaEquipo", imagenesInstanciaEquipo);
			
			JSONArray respuestasPautas = new JSONArray();
			
			ArrayList <FormObject> itemsPautas = this.getItemPautas();
			
			for (  FormObject fo : itemsPautas ) {
				
				JSONObject itemPauta = new JSONObject();

				itemPauta.put("IdItemPauta", fo.nombre);
				
				itemPauta.put("Afirmativo", ( fo.data.equals("C") && !fo.data.equals("NA") )  );
				
				itemPauta.put("NoAplica", fo.data.equals("NA") );
				
				itemPauta.put("Observacion", "sin observaciones");
			
				respuestasPautas.put(itemPauta);
				
			}
			
			instanciaCertificacionEquipo.put("RespuestasPautas", respuestasPautas);
			
			recepcionCertificacionEquipo.put("InstanciaCertificacionEquipo", instanciaCertificacionEquipo);
			
		try {
			FormUtility.writeLog( recepcionDatosCertificacion.toString() );
	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return recepcionDatosCertificacion;
		
	}
	
	
	private ArrayList <FormObject> getItemPautas() {
		
		ArrayList <FormObject> items = new ArrayList<FormObject>();
		
		String control = "itemPauta*";
	
		for ( FormObject item : values ) {
			
			if ( item.nombre.contains( control ) ) {
				
				item.nombre = item.nombre.replace(control, "");
				
				items.add(item);	
			}
		}
		
		return items;	
	}
	
	private String getItemByCampo(String campo) {
		
		for ( int i = 0; i<values.size(); i++ ) {
			
			FormObject item = values.get(i);
			
			if ( item.nombre.equals(campo) ){
				
				return item.data;
			}
		}
		
		return "none";
	}
	
	private void recorrer( ViewGroup grupo) {
		
		for ( int i = 0; i< grupo.getChildCount(); i++ ) {
			
			if ( ( grupo.getChildAt(i) instanceof LinearLayout || grupo.getChildAt(i) instanceof RelativeLayout) && grupo.getChildAt(i) instanceof RadioGroup == false    ) {
			
				recorrer( (ViewGroup) grupo.getChildAt(i));
				
			} else {
				
				if (  grupo.getChildAt(i) instanceof EditText  ||  grupo.getChildAt(i) instanceof RadioGroup ) {
			
					//Log.d("log", "Obteniendo: "+ grupo.getChildAt(i) );
					campos.add( grupo.getChildAt(i));
				}
			}
		}
	}

	
	public static void writeLog(String txt ) throws IOException {
		
		 File myFile = new File("/sdcard/log.txt");
         myFile.createNewFile();
         FileOutputStream fOut = new FileOutputStream(myFile);
         OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
         myOutWriter.append(txt);
         myOutWriter.close();
         fOut.close();
         Log.i("log", "Done writing SD");
		
	}
	
	
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		
		
		RadioButton radio = (RadioButton ) buttonView;
		
		RadioGroup group = (RadioGroup) radio.getParent();
		
		for ( int b =0; b<group.getChildCount(); b++ ) {
			
			RadioButton child = (RadioButton) group.getChildAt(b);
			
			child.setTextColor(Color.DKGRAY);
		}
		
	}
	

}
