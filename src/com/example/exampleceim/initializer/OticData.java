package com.example.exampleceim.initializer;


public class OticData {
	
	private int mID;
	private String mNombreOtic;
	private byte[] mImagenLogoOTIC;
	private String mDescripcionOTIC;
	
	public OticData(int $ID, String _NombreOtic, byte[] decodedByte, String _DescOtic) {
		
		setmID($ID);
		setmNombreOtic(_NombreOtic);
		setmImagenLogoOTIC(decodedByte);
		setmDescripcionOTIC(_DescOtic);
		
	}

	public int getmID() {
		return mID;
	}

	public void setmID(int mID) {
		this.mID = mID;
	}

	public String getmNombreOtic() {
		return mNombreOtic;
	}

	public void setmNombreOtic(String mNombreOtic) {
		this.mNombreOtic = mNombreOtic;
	}

	public byte[] getmImagenLogoOTIC() {
		return mImagenLogoOTIC;
	}

	public void setmImagenLogoOTIC(byte[] decodedByte) {
		this.mImagenLogoOTIC = decodedByte;
	}

	public String getmDescripcionOTIC() {
		return mDescripcionOTIC;
	}

	public void setmDescripcionOTIC(String mDescripcionOTIC) {
		this.mDescripcionOTIC = mDescripcionOTIC;
	}
	

}