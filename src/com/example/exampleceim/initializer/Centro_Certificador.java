package com.example.exampleceim.initializer;

public class Centro_Certificador {
	
	private int mIdCentroCertificador;
	private int m$ID;
	private byte[] mImgLogo;
	private String mNombreCertificador;

	public Centro_Certificador(int idCentroCertificador, int $id, byte[] imgLogo, String nombreCertificador) {
		
		setmIdCentroCertificador(idCentroCertificador); 
		setM$ID($id);
		setmImgLogo(imgLogo);
		setmNombreCertificador(nombreCertificador);
		
	}

	public int getmIdCentroCertificador() {
		return mIdCentroCertificador;
	}

	public void setmIdCentroCertificador(int mIdCentroCertificador) {
		this.mIdCentroCertificador = mIdCentroCertificador;
	}

	public int getM$ID() {
		return m$ID;
	}

	public void setM$ID(int m$ID) {
		this.m$ID = m$ID;
	}

	public byte[] getmImgLogo() {
		return mImgLogo;
	}

	public void setmImgLogo(byte[] mImgLogo) {
		this.mImgLogo = mImgLogo;
	}

	public String getmNombreCertificador() {
		return mNombreCertificador;
	}

	public void setmNombreCertificador(String mNombreCertificador) {
		this.mNombreCertificador = mNombreCertificador;
	}

}
