package com.example.exampleceim.initializer;

import org.json.JSONObject;
import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.example.exampleceim.BaseParser;
import com.example.exampleceim.DatabaseHandler;

public class DataInitializer_Parser extends BaseParser{
	
	public static final String LOGTAG = DataInitializer_Parser.class.getName() + " LOG_TAG";
	
	private final static String JSON_KEY_CENTROCERTIFICADOR = "CentroCertificador";
	
	private final static String JSON_KEY_NOMBRE = "Nombre";
	private final static String JSON_KEY_IMAGENLOGO = "ImagenLogo";
	private final static String JSON_KEY_IDCENTROCERTIFICADOR = "IdCentroCertificador";
	
	private final static String JSON_KEY_OTICDATA = "OticData";

	private final static String JSON_KEY_NOMBREOTIC = "NombreOTIC";
	private final static String JSON_KEY_IMAGENLOGOOTIC = "ImagenLogoOTIC";
	private final static String JSON_KEY_$ID = "$id";
	private final static String JSON_KEY_DESCRIPCIONOTIC = "DescripcionOTIC";
	
	public DataInitializer_Parser(Context context) {
		super();
	}
	
	public void guardarDatosInicializacion(JSONObject jObject, Context context, DatabaseHandler db){
		
		db.open();
		
		OticData data;
		JSONObject jOticData = getJSONObject(jObject, JSON_KEY_OTICDATA);
		
		byte[] imgLogo = Base64.decode(getString(jOticData, JSON_KEY_IMAGENLOGOOTIC), Base64.DEFAULT);
		//Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length); 
		
		data = new OticData(getInt(jOticData, JSON_KEY_$ID), 
							getString(jOticData, JSON_KEY_NOMBREOTIC), 
							imgLogo,
							getString(jOticData, JSON_KEY_DESCRIPCIONOTIC));
		
		JSONObject jCentroCertificador = getJSONObject(jObject, JSON_KEY_CENTROCERTIFICADOR);
		Centro_Certificador centro_Certificador;
		
		centro_Certificador = new Centro_Certificador(getInt(jCentroCertificador, JSON_KEY_IDCENTROCERTIFICADOR), 
													  getInt(jCentroCertificador, JSON_KEY_$ID), 
													  getLogoImage(getString(jCentroCertificador, JSON_KEY_IMAGENLOGO)), 
													  getString(jCentroCertificador, JSON_KEY_NOMBRE));
		
		db.insertCertificadorData(centro_Certificador);
		db.insertOticData(data);
		db.close();
	}
	
	public static byte[] getLogoImage(String img){
		
	     try {
	    	 byte[] decodedString = Base64.decode(img, Base64.DEFAULT);
	             return decodedString;
	             
	     } catch (Exception e) {
	             Log.e(LOGTAG, "Error: " + e.toString());
	             e.printStackTrace();
	     }
	     return null;
	}

}