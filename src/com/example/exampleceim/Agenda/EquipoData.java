package com.example.exampleceim.Agenda;

public class EquipoData {
	
	private int idEquipo;
	private String marca;
	private String modelo;
	private String ano;
	private String placaPatente;
	private String codigoInterno;
	private String color;
	private String numeroMotor;
	private String numeroSerie;
	private String numeroFactura;
	private String numeroGuiaDespacho;
	private String capacidad;
	private String potencia;
	private String fechaCertificacion;
	private String tipoAccion;
	
	public EquipoData() {
	}
	
	public EquipoData(int _idEquipo, String _marca, String _modelo, String _ano, String _placaPatente, String _codigoInterno,
						String _color, String _numeroMotor, String _numeroSerie, String _numeroFactura, String _numeroGuiaDespacho,
						String _capacidad, String _potencia, String _fechaCertificacion, String _tipoAccion) {
		
		setIdEquipo(_idEquipo);
		setMarca(_marca);
		setModelo(_modelo);
		setAno(_ano);
		setPlacaPatente(_placaPatente);
		setCodigoInterno(_codigoInterno);
		setColor(_color);
		setNumeroMotor(_numeroMotor);
		setNumeroSerie(_numeroSerie);
		setNumeroFactura(_numeroFactura);
		setNumeroGuiaDespacho(_numeroGuiaDespacho);
		setCapacidad(_capacidad);
		setPotencia(_potencia);
		setFechaCertificacion(_fechaCertificacion);
		setTipoAccion(_tipoAccion);
	}

	public int getIdEquipo() {
		return idEquipo;
	}

	public void setIdEquipo(int idEquipo) {
		this.idEquipo = idEquipo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getPlacaPatente() {
		return placaPatente;
	}

	public void setPlacaPatente(String _placaPatente) {
		this.placaPatente = _placaPatente;
	}

	public String getCodigoInterno() {
		return codigoInterno;
	}

	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getNumeroMotor() {
		return numeroMotor;
	}

	public void setNumeroMotor(String numeroMotor) {
		this.numeroMotor = numeroMotor;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(String _numeroFactura) {
		this.numeroFactura = _numeroFactura;
	}

	public String getNumeroGuiaDespacho() {
		return numeroGuiaDespacho;
	}

	public void setNumeroGuiaDespacho(String numeroGuiaDespacho) {
		this.numeroGuiaDespacho = numeroGuiaDespacho;
	}

	public String getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(String capacidad) {
		this.capacidad = capacidad;
	}

	public String getPotencia() {
		return potencia;
	}

	public void setPotencia(String potencia) {
		this.potencia = potencia;
	}

	public String getFechaCertificacion() {
		return fechaCertificacion;
	}

	public void setFechaCertificacion(String fechaCertificacion) {
		this.fechaCertificacion = fechaCertificacion;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

}
