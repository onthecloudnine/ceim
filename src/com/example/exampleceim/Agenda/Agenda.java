package com.example.exampleceim.Agenda;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.NetworkOnMainThreadException;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.exampleceim.DatabaseHandler;
import com.example.exampleceim.R;
import com.example.exampleceim.pautas_certificacion.Pauta_Empleado;
import com.example.exampleceim.pautas_certificacion.Pauta_Equipo;
import com.example.exampleceim.usuarios.Persona;
import com.loopj.android.http.AsyncHttpClient;

public class Agenda extends Activity {

	private Agenda mContext;
	DatabaseHandler db;
	ArrayAdapter<String> populateListViewTrabajdores = null;
	ArrayAdapter<String> populateListViewMaquinas = null;
	AsyncHttpClient client = new AsyncHttpClient();
	private static String LOGTAG = Agenda.class.getName() + " LOGCAT";
	
	private static String urlEnviarDatosCertificaciones = "http://webapitest2014.azurewebsites.net/api/Certificacion/SetDatosCertificaciones";
	
	static final String KEY_FECHA = "fecha";
	static final String KEY_EMPRESA = "empresa";
	static final String KEY_PAUTA = "pauta";
	static final String KEY_EQUIPO = "equipo";
	static final String KEY_TIPO_CERTIFICACION = "TipoCertificacion";
	
	ProgressDialog barProgressDialog;
	
	public InputStream is;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_agenda);
		
		mContext = this;
		
		alertAgenda();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.agenda, menu);
		return true;
	}
	
	public void alertAgenda(){
		
		  AlertDialog.Builder builder = new AlertDialog.Builder(this);
		  	builder.setTitle("Por favor espere ...");
		    builder.setMessage("Estamos actualizando la información de su Agenda ...")
		           .setCancelable(false)
		           .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		            	   
		            	   	cargarLogo(mContext);
			           		cargarAgenda(mContext);
//			           		obtenerDatosAgenda();
			           		llenarTablaEquipo();
			           		llenarTablaPersonas();
		            	   
		               }
		           });
		           
		    AlertDialog alert = builder.create();
		    alert.show();
		
	}
	
	public static void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
         
        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
 
        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
 
        // Showing Alert Message
        alertDialog.show();
    }
	
	void postData(final String jsonData){
		//Log.d("JSON a Enviar", jsonData1);
		// Agregar autentificacion basica con los datos del certificador, user y password, debe respetar estructura definida en la clase.
		class PostData extends AsyncTask<String, String, Void>{
			Dialog dialog;
			@Override
			protected Void doInBackground(String... params) {
				 // Create a new HttpClient and Post Header
			    HttpClient httpclient = new DefaultHttpClient();
			    HttpPost httppost = new HttpPost(urlEnviarDatosCertificaciones);
			    
			    try {
			        // Add your data
			        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			        nameValuePairs.add(new BasicNameValuePair("json", jsonData));
			        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
			        
			        // Execute HTTP Post Request
			        HttpResponse response = httpclient.execute(httppost);
			        HttpEntity httpEntity = response.getEntity();
			        is = httpEntity.getContent();

        			runOnUiThread(new Runnable() {
        			    public void run() {
//        			    	
//        			    	AlertDialog.Builder procesando = new AlertDialog.Builder(Main_Fragment.this);
//							TextView myMsg = new TextView(Main_Fragment.this);
							
//							myMsg.setText(getResString(R.string.procesando_solicitud));
//							myMsg.setTextSize(18);
//							myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
//							myMsg.setPadding(0, 10, 0, 10);
//							procesando.setView(myMsg)
//							       .setCancelable(false)
//							       .setPositiveButton(getResString(R.string.dialog_aceptar), new DialogInterface.OnClickListener() {
//							           public void onClick(DialogInterface dialog, int id) {
//							        	   dialog.cancel();
//							        	   
//							        	   mDrawerLayout.closeDrawer(drawer_rightLayout);
//							        	   
//							        	   Welcome w1 = new Welcome();
//							         		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//							         		transaction.replace(R.id.content_frame, w1);
//							         		transaction.commit();
//							        	   
//							           }
//							       });
//							dialog = procesando.create();
//							dialog.show();
        			    }
        			});
        			
					
			    }catch (NetworkOnMainThreadException e) { 
			    	e.printStackTrace();
			    }catch (ClientProtocolException e) {
			    	
			    } catch (IOException e) {
			    }catch (Exception e) {
			    	e.printStackTrace();
			    }
				return null;
			}
			
		}
		
		PostData _postData = new PostData();
		_postData.execute(jsonData);
	   
	} 

	public void cargarLogo(Context context){
		
		db = new DatabaseHandler(context);
		db.open();
		
		Resources res = getResources();
		BitmapDrawable icon = new BitmapDrawable(res, db.getImgLogo());
		getActionBar().setIcon(icon);
		
		db.close();
		
	}
	
	public void cargarAgenda(Context context){
		
		   Intent intent = getIntent();
			final String user = intent.getStringExtra("user");
//			final String pass = intent.getStringExtra("pass");
			TextView txtNombre = (TextView) findViewById(R.id.txtNombre);
		
//		ArrayList<String> resultsPautaMaquina = new ArrayList<String>();
//		ArrayList<String> resultsPautaTrabajador = new ArrayList<String>();
		db = new DatabaseHandler(context);
		db.open();
		
//		String imprimir = db.sample_query_01();
//		Log.i(LOGTAG, imprimir);
		
		Persona persona = db.getPersona(user);
		
		txtNombre.setText(persona.getNombres()+ " " + persona.getPrimerApellido() + " " + persona.getSegundoApellido());
		
		List<AgendaData> agendaAdapter = db.getAgenda();
		
		for(final AgendaData pc : agendaAdapter){
			
			ArrayList<HashMap<String, String>> agendaList = new ArrayList<HashMap<String, String>>();
			
			int tipoCertificacion = pc.getTipoCertificacion();
			
//			ArrayList<HashMap<String, String>> agendaList = new ArrayList<HashMap<String, String>>();
			
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(KEY_FECHA, pc.getFechaProgramacion());
			map.put(KEY_TIPO_CERTIFICACION, String.valueOf(pc.getTipoCertificacion()));
			map.put(KEY_EMPRESA, "Empresa");
			map.put(KEY_PAUTA, String.valueOf(pc.getIdPautaCertificacion()));
			map.put(KEY_EQUIPO, "Equipo");
			agendaList.add(map);
//			llenarTablaEquipo();
//       		llenarTablaEmpleado();
			
			if(tipoCertificacion == 1){
				//String nombrePauta = pc.getNombre();
				//resultsPautaMaquina.add(nombrePauta);
				
				
//				llenarTablaEquipo(agendaList);
				
			}else{
				//String nombrePauta = pc.getNombre();
				//resultsPautaTrabajador.add(nombrePauta);
//				llenarTablaEmpleado(agendaList);
			}
			
		}

		 
//		 final ListView lvTrabajadores = (ListView) findViewById(R.id.lvTrabajadores);
//		 populateListViewTrabajdores = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_black_text,R.id.list_content, resultsPautaTrabajador);
//		// populateListViewTrabajdores = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, resultsPautaTrabajador);
//		 lvTrabajadores.setAdapter(populateListViewTrabajdores);
//		 lvTrabajadores.setOnItemClickListener(new OnItemClickListener() { 
//	    		
//	 			@Override
//	 			public void onItemClick(AdapterView<?> pariente, View view, int posicion, long id) {
//	 				
//	 				String str = lvTrabajadores.getItemAtPosition(posicion).toString();
//						
//					            lvTrabajadores.addFooterView(view);
//				   		
//	 			}
//	         });
		 
		 
//		 final ListView lvMaquinas = (ListView) findViewById(R.id.lvMaquinas);
//		 populateListViewTrabajdores = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_black_text,R.id.list_content, resultsPautaMaquina);
//		// populateListViewTrabajdores = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, resultsPautaTrabajador);
//		 lvMaquinas.setAdapter(populateListViewTrabajdores);
		
		
		db.close();
	}
	
	void llenarTablaEquipo() {
		 
		Log.d(LOGTAG, "Cargando Agenda, Tabla Equipos ...");
		
        //Converting to dip unit
        final int dip = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                (float) 1, getResources().getDisplayMetrics());
        final TableLayout equipo_table = (TableLayout)findViewById(R.id.equipo_table);
        
		  TableRow row;
		  TextView t1;
		  TextView t2;
		  TextView t3;
		  TextView t4;
//		  TextView t5;

		  db = new DatabaseHandler(mContext);
					  db.open();
					  
//		Intent intent = getIntent();
//		final String user = intent.getStringExtra("user");  
//		Persona persona = db.getPersona(user);
//		Log.d(LOGTAG, String.valueOf(persona.getIdUsuario()));
					
			  List<AgendaData> agendaAdapter = db.getAgenda(); //persona.getIdUsuario()
			  
			  for(final AgendaData pc : agendaAdapter){
				  
			  Log.d(LOGTAG, "Tipo Certificacion: " + pc.getTipoCertificacion());
			  
				 if(pc.getTipoCertificacion() == 1 ){
					 
		            row = new TableRow(getApplicationContext());
		            
		            t1 = new TextView(getApplicationContext());
		            t1.setTextColor(0xff000000);
		            t2 = new TextView(getApplicationContext());
		            t2.setTextColor(0xff000000);
		            t3 = new TextView(getApplicationContext());
		            t3.setTextColor(0xff000000);
		            t4 = new TextView(getApplicationContext());
		            t4.setTextColor(0xff000000);
//				            t5 = new TextView(getApplicationContext());
		            
		            final String idPautaCertificacion = String.valueOf(pc.getIdPautaCertificacion());
		            final String jsonRelleno = pc.getJsonRellenar();
		            
		            	Log.d("id Pauta", idPautaCertificacion);
		            
		            if(jsonRelleno != null){
		            	Log.d("jsonRelleno", jsonRelleno);
		            }
		            
		            
		            row.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							
							if(pc.getTipoCertificacion() == 1 ){
								
								Intent intent = new Intent(getApplicationContext(), Pauta_Equipo.class);
								intent.putExtra("IdPautaCertificacion", idPautaCertificacion);
								intent.putExtra("recuperarDatos", jsonRelleno);
								startActivity(intent);
								
							}
							
						}
					});
		 
		            int idEquipo = pc.getIdEquipo();
//				    Log.d(LOGTAG, "id Equipo: " + String.valueOf(idEquipo));
		            
		            EquipoData equipoData = db.getEquipo(idEquipo);
		            String fecha = stringFrom("T", pc.getFechaProgramacion());
		            
		            t1.setText(fecha); //agendaList.get(current).get(KEY_FECHA)
		            t2.setText("Empresa");
		            
		            String nombrePautaCertificacion = db.getNombrePauta(Integer.valueOf(idPautaCertificacion));
		            
		            t3.setText(nombrePautaCertificacion); //agendaList.get(current).get(KEY_PAUTA)
		            t4.setText("Patente: " + equipoData.getPlacaPatente()); //agendaList.get(current).get(KEY_EQUIPO)
		            
//		            Drawable img = getResources().getDrawable( R.drawable.search );
//				    t5.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);
		            
		            t1.setTextSize(15);
		            t2.setTextSize(15);
		            t3.setTextSize(15);
		            t4.setTextSize(15);
		 
		            t1.setWidth(100 * dip);
		            t2.setWidth(90 * dip);
		            t3.setWidth(190 * dip);
		            t4.setWidth(130 * dip);
//				    t5.setWidth(90 * dip);
//					t5.getLayoutParams().width = 100;
		            
		            t1.setPadding(5 * dip, 0, 0, 0);
		            t2.setPadding(5 * dip, 0, 0, 0);
		            t3.setPadding(5 * dip, 0, 0, 0);
		            t4.setPadding(25 * dip, 0, 0, 0);
		            row.setPadding(0, 20, 0, 20);
		            
		            row.addView(t1);
		            row.addView(t2);
		            row.addView(t3);
		            row.addView(t4);
//				    row.addView(t5);
		            
		            equipo_table.addView(row, new TableLayout.LayoutParams(
		                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
				 }
			  }

//				File myFile = new File("/sdcard/AgendaJson.txt");
//				myFile.createNewFile();
//				FileOutputStream fOut = new FileOutputStream(myFile);
//				OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
//				myOutWriter.append(response.toString());
//				myOutWriter.close();
//				fOut.close();
//			            
//				Log.i(TAG_APP, "Done writing SD");
		
 
    }
	
	void llenarTablaPersonas(){
		
		//Converting to dip unit
        final int dip = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                (float) 1, getResources().getDisplayMetrics());
        final TableLayout empleado_table = (TableLayout)findViewById(R.id.empleado_table);
        
					  TableRow row;
					  TextView t1;
					  TextView t2;
					  TextView t3;
					  TextView t4;
					  TextView t5;

					  db = new DatabaseHandler(mContext);
					  db.open();
					  
					  Intent intent = getIntent();
					  final String user = intent.getStringExtra("user");
					  
					  Persona persona = db.getPersona(user);
//					  Log.d(LOGTAG, String.valueOf(persona.getIdUsuario()));
					  
					  List<AgendaData> agendaAdapter = db.getAgenda();
					  for(final AgendaData pc : agendaAdapter){
							
							
							int tipoCertificacion = pc.getTipoCertificacion();
							if( tipoCertificacion == 0 ){
					            row = new TableRow(getApplicationContext());
					            
					            t1 = new TextView(getApplicationContext());
					            t1.setTextColor(0xff000000);
					            t2 = new TextView(getApplicationContext());
					            t2.setTextColor(0xff000000);
					            t3 = new TextView(getApplicationContext());
					            t3.setTextColor(0xff000000);
					            t4 = new TextView(getApplicationContext());
					            t4.setTextColor(0xff000000);
//					            t5 = new TextView(getApplicationContext());
					            
					            final String idPautaCertificacion = String.valueOf(pc.getIdPautaCertificacion());
					            
//					            t5.setOnClickListener(new OnClickListener() {
//									
//									@Override
//									public void onClick(View v) {
//										
//										if(pc.getTipoCertificacion() == 0){
//											
//											Intent intent = new Intent(getApplicationContext(), Pauta_Empleado.class);
//											intent.putExtra("IdPautaCertificacion", idPautaCertificacion);
////											intent.putExtra("user", user);
////											intent.putExtra("pass", pass);
//											startActivity(intent);
//											
//										}
//										
//									}
//								});
					            
					            row.setOnClickListener(new OnClickListener() {
									
									@Override
									public void onClick(View v) {
										
										if(pc.getTipoCertificacion() == 0){
											
											Intent intent = new Intent(getApplicationContext(), Pauta_Empleado.class);
											intent.putExtra("IdPautaCertificacion", idPautaCertificacion);
//											intent.putExtra("user", user);
//											intent.putExtra("pass", pass);
											startActivity(intent);
											
										}
										
									}
								});
					            
					            int idEmpleado = pc.getIdEmpleado();
					            
					            Log.d(LOGTAG, "id Empleado: " + String.valueOf(idEmpleado));
					            
					            EmpleadoData empleadoData = db.getEmpleado(idEmpleado);
					 
					            String fecha = stringFrom("T", pc.getFechaProgramacion());
					            
					            t1.setText(fecha); //agendaList.get(current).get(KEY_FECHA)
					            t2.setText("Empresa");
					            
					            String nombrePautaCertificacion = db.getNombrePauta(Integer.valueOf(idPautaCertificacion));
					            
					            t3.setText(nombrePautaCertificacion); //agendaList.get(current).get(KEY_PAUTA)
					            
					        	String Nombre = empleadoData.getNombres();
		        				String Apellido = empleadoData.getPrimerApellido();
		        				String Empleado = Nombre +" "+ Apellido;
					        	
					            t4.setText(Empleado); //agendaList.get(current).get(KEY_EQUIPO)
					            
					            Drawable img = getResources().getDrawable( R.drawable.search );
//					            t5.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);
					            
					            t1.setTextSize(15);
					            t2.setTextSize(15);
					            t3.setTextSize(15);
					            t4.setTextSize(15);
					 
					            t1.setWidth(100 * dip);
					            t2.setWidth(90 * dip);
					            t3.setWidth(195 * dip);
					            t4.setWidth(120 * dip);
//					            t5.setWidth(90 * dip);
					            //t5.getLayoutParams().width = 100;
					            
					            t1.setPadding(5 * dip, 0, 0, 0);
					            t2.setPadding(5 * dip, 0, 0, 0);
					            t3.setPadding(5 * dip, 0, 0, 0);
					            t4.setPadding(25 * dip, 0, 0, 0);
					            row.setPadding(0, 20, 0, 20);
					            
					            row.addView(t1);
					            row.addView(t2);
					            row.addView(t3);
					            row.addView(t4);
//					            row.addView(t5);
					            empleado_table.addView(row, new TableLayout.LayoutParams(
					                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
					 
					        }
							
						}
		
	}
	
	public static String stringFrom(String search, String string) {

		int lastIndex = string.lastIndexOf(search);

		return string.substring(0, lastIndex);	
		}
	

}
