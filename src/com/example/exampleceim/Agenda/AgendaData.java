package com.example.exampleceim.Agenda;

public class AgendaData {
	/*
	 * 	{	
		    "IdProgramacionCertificacion": 1,
		    "IdPautaCertificacion": 2,
		    "IdCertificador": 5,
		    "TipoCertificacion": 1,
		    "FechaRequerimento": "2014-04-01T00:00:00",
		    "FechaProgramacion": "2014-04-10T00:00:00",
		    "Ubicacion": "ANTOFAGASTA-LA NEGRA",
		    "Contacto": "MIGUEL JUAREZ",
		    "TipoAccion": 4
  		}
	 */

	private int idProgramacionCertificacion;
	private int idPautaCertificacion;
	private int idEquipo;
	private int idEmpleado;
	private int idCertificador;
	private int tipoCertificacion;
	private String fechaRequerimento;
	private String fechaProgramacion;
	private String ubicacion;
	private String contacto;
	private int tipoAccion;
	private String jsonEnviar;
	private String jsonRellenar;
	private String finalizado;
	
	
	public AgendaData() {
	}

	public AgendaData(int _idPautaCertificacion, String _jsonEnviar, String _jsonRellenar, String _finalizado) {
	
		setIdPautaCertificacion(_idPautaCertificacion);
		setJsonEnviar(_jsonEnviar);
		setJsonRellenar(_jsonRellenar);
		setFinalizado(_finalizado);
	
	}
	
	public AgendaData(int _idProgramacionCertificacion, int _idPautaCertificacion, int _idEquipo, int _idEmpleado,int _idCertificador, int _tipoCertificacion,
						String _fechaRequerimento, String _fechaProgramacion, String _ubicacion, String _contacto, int _tipoAccion, String _jsonEnviar, String _jsonRellenar, String _finalizado) {
		
		setIdProgramacionCertificacion(_idProgramacionCertificacion);
		setIdPautaCertificacion(_idPautaCertificacion);
		setIdEquipo(_idEquipo);
		setIdEmpleado(_idEmpleado);
		setIdCertificador(_idCertificador);
		setTipoCertificacion(_tipoCertificacion);
		setFechaRequerimento(_fechaRequerimento);
		setFechaProgramacion(_fechaProgramacion);
		setUbicacion(_ubicacion);
		setContacto(_contacto);
		setTipoAccion(_tipoAccion);
		setJsonEnviar(_jsonEnviar);
		setJsonRellenar(_jsonRellenar);
		setFinalizado(_finalizado);
		
	}

	public int getIdProgramacionCertificacion() {
		return idProgramacionCertificacion;
	}


	public void setIdProgramacionCertificacion(int idProgramacionCertificacion) {
		this.idProgramacionCertificacion = idProgramacionCertificacion;
	}


	public int getIdPautaCertificacion() {
		return idPautaCertificacion;
	}


	public void setIdPautaCertificacion(int idPautaCertificacion) {
		this.idPautaCertificacion = idPautaCertificacion;
	}


	public int getIdCertificador() {
		return idCertificador;
	}


	public void setIdCertificador(int idCertificador) {
		this.idCertificador = idCertificador;
	}


	public int getTipoCertificacion() {
		return tipoCertificacion;
	}


	public void setTipoCertificacion(int tipoCertificacion) {
		this.tipoCertificacion = tipoCertificacion;
	}


	public String getFechaRequerimento() {
		return fechaRequerimento;
	}


	public void setFechaRequerimento(String fechaRequerimento) {
		this.fechaRequerimento = fechaRequerimento;
	}


	public String getFechaProgramacion() {
		return fechaProgramacion;
	}


	public void setFechaProgramacion(String fechaProgramacion) {
		this.fechaProgramacion = fechaProgramacion;
	}


	public String getUbicacion() {
		return ubicacion;
	}


	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}


	public String getContacto() {
		return contacto;
	}


	public void setContacto(String contacto) {
		this.contacto = contacto;
	}


	public int getTipoAccion() {
		return tipoAccion;
	}


	public void setTipoAccion(int tipoAccion) {
		this.tipoAccion = tipoAccion;
	}


	public int getIdEquipo() {
		return idEquipo;
	}


	public void setIdEquipo(int idEquipo) {
		this.idEquipo = idEquipo;
	}


	public int getIdEmpleado() {
		return idEmpleado;
	}


	public void setIdEmpleado(int idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public String getFinalizado() {
		return finalizado;
	}


	public void setFinalizado(String finalizado) {
		this.finalizado = finalizado;
	}


	public String getJsonRellenar() {
		return jsonRellenar;
	}


	public void setJsonRellenar(String jsonRellenar) {
		this.jsonRellenar = jsonRellenar;
	}


	public String getJsonEnviar() {
		return jsonEnviar;
	}


	public void setJsonEnviar(String jsonEnviar) {
		this.jsonEnviar = jsonEnviar;
	}

}
