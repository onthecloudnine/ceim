package com.example.exampleceim.Agenda;

public class CertificacionEmpleadoData {
	
	private int IdCertificacionEmpleado;
	private int IdPautaCertificacion;
	private int IdEmpleado;
	private String EquipoAsociado;
	private String EquipoAsociadoTonelada;
	private String LugarEvaluacion;
	private int NumeroConstancia;
	private int NumeroNivelAprobacion;
	private String Fecha;
	private String FechaCertificacion;
	private String FechaVigencia;
	private int TipoAccion;
	
	CertificacionEmpleadoData(){
		
	}
	
	CertificacionEmpleadoData(int _idCertificacionEmpleado, int _idPautaCertificacion, int _idEmpleado, String _equipoAsociado,
								String _equipoAsociadoTonelada, String _lugarEvaluacion, int _numeroConstancia, int _numeroNivelAprobacion,
								String _fecha, String _fechaCertificacion, String _fechaVigencia, int _tipoAccion){
		
		setIdCertificacionEquipo(_idCertificacionEmpleado);
		setIdPautaCertificacion(_idPautaCertificacion);
		setIdEmpleado(_idEmpleado);
		setEquipoAsociado(_equipoAsociado);
		setEquipoAsociadoTonelada(_equipoAsociadoTonelada);
		setLugarEvaluacion(_lugarEvaluacion);
		setNumeroConstancia(_numeroConstancia);
		setNumeroNivelAprobacion(_numeroNivelAprobacion);
		setFecha(_fecha);
		setFechaCertificacion(_fechaCertificacion);
		setFechaVigencia(_fechaVigencia);
		setTipoAccion(_tipoAccion);
		
		
	}

	public int getIdCertificacionEmpleado() {
		return IdCertificacionEmpleado;
	}

	public void setIdCertificacionEquipo(int idCertificacionEmpleado) {
		IdCertificacionEmpleado = idCertificacionEmpleado;
	}

	public int getIdPautaCertificacion() {
		return IdPautaCertificacion;
	}

	public void setIdPautaCertificacion(int idPautaCertificacion) {
		IdPautaCertificacion = idPautaCertificacion;
	}

	public int getIdEmpleado() {
		return IdEmpleado;
	}

	public void setIdEmpleado(int idEmpleado) {
		IdEmpleado = idEmpleado;
	}

	public String getEquipoAsociado() {
		return EquipoAsociado;
	}

	public void setEquipoAsociado(String equipoAsociado) {
		EquipoAsociado = equipoAsociado;
	}

	public String getEquipoAsociadoTonelada() {
		return EquipoAsociadoTonelada;
	}

	public void setEquipoAsociadoTonelada(String equipoAsociadoTonelada) {
		EquipoAsociadoTonelada = equipoAsociadoTonelada;
	}

	public String getLugarEvaluacion() {
		return LugarEvaluacion;
	}

	public void setLugarEvaluacion(String lugarEvaluacion) {
		LugarEvaluacion = lugarEvaluacion;
	}

	public int getNumeroConstancia() {
		return NumeroConstancia;
	}

	public void setNumeroConstancia(int numeroConstancia) {
		NumeroConstancia = numeroConstancia;
	}

	public int getNumeroNivelAprobacion() {
		return NumeroNivelAprobacion;
	}

	public void setNumeroNivelAprobacion(int numeroNivelAprobacion) {
		NumeroNivelAprobacion = numeroNivelAprobacion;
	}

	public String getFecha() {
		return Fecha;
	}

	public void setFecha(String fecha) {
		Fecha = fecha;
	}

	public String getFechaCertificacion() {
		return FechaCertificacion;
	}

	public void setFechaCertificacion(String fechaCertificacion) {
		FechaCertificacion = fechaCertificacion;
	}

	public String getFechaVigencia() {
		return FechaVigencia;
	}

	public void setFechaVigencia(String fechaVigencia) {
		FechaVigencia = fechaVigencia;
	}

	public int getTipoAccion() {
		return TipoAccion;
	}

	public void setTipoAccion(int tipoAccion) {
		TipoAccion = tipoAccion;
	}

	
	        
	        
}
