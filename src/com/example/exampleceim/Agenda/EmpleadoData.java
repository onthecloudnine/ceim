package com.example.exampleceim.Agenda;

public class EmpleadoData {
	
	 private int IdEmpleado;  
	 private int TipoAccion;
     private int RUN;
     private String DigitoVerificador;
     private String Nombres;
     private String PrimerApellido;
     private String SegundoApellido;
     private String Telefono;
     private String Celular;
     
     /*
      * 	   		"RUN": 16239404,
                    "Telefono": null,
                    "DigitoVerificador": "5",
                    "Nombres": "ALEXIS",
                    "PrimerApellido": "CEA",
                    "IdEmpleado": 1,
                    "TipoAccion": 4,
                    "Celular": "5555632",
                    "SegundoApellido": "ROJAS"
      * 
      */

	public EmpleadoData(int idEmpleado, int tipoAccion, int run,
				   String digitoVerificador, String nombres, String primerApellido, String segundoApellido,
				   String telefono, String celular) {
		
		setIdEmpleado(idEmpleado);
		setTipoAccion(tipoAccion);
		setRUN(run);
		setDigitoVerificador(digitoVerificador);
		setNombres(nombres);
		setPrimerApellido(primerApellido);
		setSegundoApellido(segundoApellido);
		setTelefono(telefono);
		setCelular(celular);
		
	}

	public EmpleadoData() {
		// TODO Auto-generated constructor stub
	}

	public int getRUN() {
		return RUN;
	}

	public void setRUN(int rUN) {
		RUN = rUN;
	}

	public String getDigitoVerificador() {
		return DigitoVerificador;
	}

	public void setDigitoVerificador(String digitoVerificador) {
		DigitoVerificador = digitoVerificador;
	}

	public String getNombres() {
		return Nombres;
	}

	public void setNombres(String nombres) {
		Nombres = nombres;
	}

	public String getPrimerApellido() {
		return PrimerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		PrimerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return SegundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		SegundoApellido = segundoApellido;
	}

	public String getTelefono() {
		return Telefono;
	}

	public void setTelefono(String telefono) {
		Telefono = telefono;
	}

	public String getCelular() {
		return Celular;
	}

	public void setCelular(String celular) {
		Celular = celular;
	}

	public int getTipoAccion() {
		return TipoAccion;
	}

	public void setTipoAccion(int tipoAccion) {
		TipoAccion = tipoAccion;
	}

	public int getIdEmpleado() {
		return IdEmpleado;
	}

	public void setIdEmpleado(int idEmpleado) {
		IdEmpleado = idEmpleado;
	}

}
