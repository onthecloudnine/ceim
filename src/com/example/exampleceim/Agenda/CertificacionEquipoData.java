package com.example.exampleceim.Agenda;

public class CertificacionEquipoData {
	
	/*
	 * CertificacionEquipo": {
        "IdCertificacionEquipo": 0,
        "IdEquipoCertificar": 0,
        "IdPautaCertificacion": 0,
        "EmpresaResponsable": null,
        "LugarInspeccion": null,
        "NPPA": null,
        "Cotizacion": null,
        "Item": null,
        "Horometro": 0,
        "Kilometraje": 0,
        "TipoCertificacion": null,
        "Distintivo": 0,
        "FechaEntregaDocumentacion": "0001-01-01T00:00:00",
        "FechaAprobacion": "0001-01-01T00:00:00",
        "EquipoOpera": null,
        "FechaVigencia": null,
        "TipoAccion": 0
      }
	 * 		
	 */
	
	private int idCertificacionEquipo;
	private int idEquipoCertificar;
	private int idPautaCertificacion;
	private String empresaResponsable;
	private String lugarInspeccion;
	private String nppa;
	private String cotizacion;
	private String item;
	private String horometro;
	private String kilometraje;
	private String tipoCertificacion;
	private String fechaEntregaDocumentacion;
	private String fechaAprobacion;
	private String equipoOpera;
	private String fechaVigencia;
	private String tipoAccion;
	
	public CertificacionEquipoData() {
		// TODO Auto-generated constructor stub
	}
	
	public CertificacionEquipoData(int _idCertificacionEquipo, int _idEquipoCertificar, int _idPautaCertificacion, 
									String _empresaResponsable, String _lugarInspeccion, String _nppa, String _cotizacion,
									String _item, String _horometro, String _kilometraje, String _tipoCertificacion, 
									String _fechaEntregaDocumentacion, String _fechaAprobacion, String _equipoOpera, String _fechaVigencia,
									String _tipoAccion) {

		setIdCertificacionEquipo(_idCertificacionEquipo);
		setIdEquipoCertificar(_idEquipoCertificar);
		setIdPautaCertificacion(_idPautaCertificacion);
		setEmpresaResponsable(_empresaResponsable);
		setLugarInspeccion(_lugarInspeccion);
		setNppa(_nppa);
		setCotizacion(_cotizacion);
		setItem(_item);
		setHorometro(_horometro);
		setKilometraje(_kilometraje);
		setTipoCertificacion(_tipoCertificacion);
		setFechaEntregaDocumentacion(_fechaEntregaDocumentacion);
		setFechaAprobacion(_fechaAprobacion);
		setEquipoOpera(_equipoOpera);
		setFechaVigencia(_fechaVigencia);
		setTipoAccion(_tipoAccion);
		
		
	}

	public int getIdCertificacionEquipo() {
		return idCertificacionEquipo;
	}

	public void setIdCertificacionEquipo(int idCertificacionEquipo) {
		this.idCertificacionEquipo = idCertificacionEquipo;
	}

	public int getIdEquipoCertificar() {
		return idEquipoCertificar;
	}

	public void setIdEquipoCertificar(int idEquipoCertificar) {
		this.idEquipoCertificar = idEquipoCertificar;
	}

	public int getIdPautaCertificacion() {
		return idPautaCertificacion;
	}

	public void setIdPautaCertificacion(int idPautaCertificacion) {
		this.idPautaCertificacion = idPautaCertificacion;
	}

	public String getEmpresaResponsable() {
		return empresaResponsable;
	}

	public void setEmpresaResponsable(String empresaResponsable) {
		this.empresaResponsable = empresaResponsable;
	}

	public String getLugarInspeccion() {
		return lugarInspeccion;
	}

	public void setLugarInspeccion(String lugarInspeccion) {
		this.lugarInspeccion = lugarInspeccion;
	}

	public String getNppa() {
		return nppa;
	}

	public void setNppa(String nppa) {
		this.nppa = nppa;
	}

	public String getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(String cotizacion) {
		this.cotizacion = cotizacion;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getHorometro() {
		return horometro;
	}

	public void setHorometro(String horometro) {
		this.horometro = horometro;
	}

	public String getKilometraje() {
		return kilometraje;
	}

	public void setKilometraje(String kilometraje) {
		this.kilometraje = kilometraje;
	}

	public String getTipoCertificacion() {
		return tipoCertificacion;
	}

	public void setTipoCertificacion(String tipoCertificacion) {
		this.tipoCertificacion = tipoCertificacion;
	}

	public String getFechaEntregaDocumentacion() {
		return fechaEntregaDocumentacion;
	}

	public void setFechaEntregaDocumentacion(String fechaEntregaDocumentacion) {
		this.fechaEntregaDocumentacion = fechaEntregaDocumentacion;
	}

	public String getFechaAprobacion() {
		return fechaAprobacion;
	}

	public void setFechaAprobacion(String fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}

	public String getEquipoOpera() {
		return equipoOpera;
	}

	public void setEquipoOpera(String equipoOpera) {
		this.equipoOpera = equipoOpera;
	}

	public String getFechaVigencia() {
		return fechaVigencia;
	}

	public void setFechaVigencia(String fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	public String getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(String tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	
	
	
}
