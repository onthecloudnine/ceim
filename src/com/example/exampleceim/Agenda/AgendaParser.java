package com.example.exampleceim.Agenda;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.example.exampleceim.BaseParser;
import com.example.exampleceim.DatabaseHandler;

public class AgendaParser extends BaseParser {
	
	public static final String LOGTAG = AgendaParser.class.getName() + " LOG_TAG ";
	public static final String JSON_KEY_ID_PROGRAMACION_CERTIFICACION 	= "IdProgramacionCertificacion";
	public static final String JSON_KEY_ID_PAUTA_CERTIFICACION 			= "IdPautaCertificacion";
	public static final String JSON_KEY_ID_CERTIFICADOR					= "IdCertificador";
	public static final String JSON_KEY_TIPO_CERTIFICACION				= "TipoCertificacion";
	public static final String JSON_KEY_FECHA_REQUERIMIENTO				= "FechaRequerimento";
	public static final String JSON_KEY_FECHA_PROGRAMACION				= "FechaProgramacion";
	public static final String JSON_KEY_UBICACION						= "Ubicacion";
	public static final String JSON_KEY_CONTACTO						= "Contacto";
	public static final String JSON_KEY_TIPO_ACCION						= "TipoAccion";
	
	public AgendaParser(Context context) {
		super();
	}
	
	public AgendaParser() {
		// TODO Auto-generated constructor stub
	}

	public void guardarDatosAgenda(JSONObject jObject, Context context, DatabaseHandler db){
		
		try {
			db.open();
			
			JSONArray jAgendaArray = jObject.getJSONArray("$values");
			
			for (int i = 0; i < jAgendaArray.length(); i++){
				
				JSONObject jItemAgenda = jAgendaArray.getJSONObject(i);
				AgendaData agendaData;
				
				int idPautaCertificacion = getInt(jItemAgenda, JSON_KEY_ID_PAUTA_CERTIFICACION);
				int idEquipo = -1;
				int idEmpleado = -1;
				
				if(jItemAgenda.getString("DataCertificacionEquipo") != "null"){
					
					JSONObject jItemDataCertificaion = getJSONObject(jItemAgenda, "DataCertificacionEquipo");
					JSONObject jItemEquipo = getJSONObject(jItemDataCertificaion, "Equipo");
					idEquipo = getInt(jItemEquipo, "IdEquipo");

/*
                    "NumeroSerie": "4444",
                    "Capacidad": 50,
                    "NumeroGuiaDespacho": null,
                    "Ano"	: null,
                    "TipoAccion": 4,
                    "Potencia": "80",
                    "NumeroFactura": null,
                    "IdEquipo": 2,
                    "$id": "4"					
 */
					EquipoData equipoData;
					equipoData = new EquipoData(getInt(jItemEquipo, "IdEquipo"), 
							getString(jItemEquipo, "Marca"), 
							getString(jItemEquipo, "Modelo"), 
							getString(jItemEquipo, "Ano"), 
							getString(jItemEquipo, "PlacaPatente"), 
							getString(jItemEquipo, "CodigoInterno"),
							getString(jItemEquipo, "Color"),
							getString(jItemEquipo, "NumeroMotor"), 
							getString(jItemEquipo, "NumeroSerie"), 
							getString(jItemEquipo, "NumeroFactura"), 
							getString(jItemEquipo, "NumeroGuiaDespacho"),
							getString(jItemEquipo, "Capacidad"), 
							getString(jItemEquipo, "Potencia"),
							"0/0/0", 
							getString(jItemEquipo, "TipoAccion"));
					
					db.insertEquipo(equipoData);
					
					JSONObject jItemCertificacionEquipo = getJSONObject(jItemDataCertificaion, "CertificacionEquipo");

					CertificacionEquipoData certificacionEquipoData;
					certificacionEquipoData = new CertificacionEquipoData(	getInt(jItemCertificacionEquipo, "IdPautaCertificacion"), 
																			idEquipo,
																			idPautaCertificacion, 
																			getString(jItemCertificacionEquipo, "EmpresaResponsable"), 
																			getString(jItemCertificacionEquipo, "LugarInspeccion"), 
																			getString(jItemCertificacionEquipo, "NPPA"), 
																			getString(jItemCertificacionEquipo, "Cotizacion"), 
																			getString(jItemCertificacionEquipo, "Item"), 
																			getString(jItemCertificacionEquipo, "Horometro"), 
																			getString(jItemCertificacionEquipo, "Kilometraje"), 
																			getString(jItemCertificacionEquipo, "TipoCertificacion"), 
																			getString(jItemCertificacionEquipo, "FechaEntregaDocumentacion"),
																			getString(jItemCertificacionEquipo, "FechaAprobacion"), 
																			getString(jItemCertificacionEquipo, "EquipoOpera"), 
																			getString(jItemCertificacionEquipo, "FechaVigencia"), 
																			getString(jItemCertificacionEquipo, "TipoAccion"));
					
					
					db.insertCertificacionEquipoData(certificacionEquipoData);
					
				}else {
					Log.d("Data Equipo Log", "No hay datos de Equipo en esta Pauta");
				}
					
				if(jItemAgenda.getString("DataCertificacionEmpleado") != "null"){
					
					Log.d("Data Equipo Log", "Ingresando Datos Empleados");
					
					JSONObject jItemDataCertificaion = getJSONObject(jItemAgenda, "DataCertificacionEmpleado");
					JSONObject jItemEquipo = getJSONObject(jItemDataCertificaion, "Empleado");
					idEmpleado = getInt(jItemEquipo, "IdEmpleado");
					
					/*
					 * "RUN": 16239404,
	                    "Telefono": null,
	                    "DigitoVerificador": "5",
	                    "Nombres": "ALEXIS",
	                    "PrimerApellido": "CEA",
	                    "IdEmpleado": 1,
	                    "TipoAccion": 4,
	                    "$id": "9",
	                    "Celular": "5555632",
	                    "SegundoApellido": "ROJAS"
					 * 
					 */
					EmpleadoData empleadoData;
					empleadoData = new EmpleadoData(idEmpleado,
													getInt(jItemEquipo, "TipoAccion"), 
													getInt(jItemEquipo, "RUN"), 
													getString(jItemEquipo, "DigitoVerificador"), 
													getString(jItemEquipo, "Nombres"), 
													getString(jItemEquipo, "PrimerApellido"), 
													getString(jItemEquipo, "SegundoApellido"), 
													getString(jItemEquipo, "Telefono"), 
													getString(jItemEquipo, "Celular"));
					
					db.insertEmpleado(empleadoData);
					
				}else{
					Log.d("Data Equipo Log", "No hay datos de Empleado en esta Pauta");
				}
				
				Log.d(LOGTAG, "===============================================================================");
				Log.d("Data Agenda Log", "ID Empleado: "+String.valueOf(idEmpleado));
				Log.d("Data Agenda Log", "ID Equipo: " + String.valueOf(idEquipo));
				Log.d(LOGTAG, "===============================================================================");
				
				agendaData = new AgendaData(getInt(jItemAgenda, JSON_KEY_ID_PROGRAMACION_CERTIFICACION), 
						getInt(jItemAgenda, JSON_KEY_ID_PAUTA_CERTIFICACION), 
						idEquipo,
						idEmpleado,
						getInt(jItemAgenda, JSON_KEY_ID_CERTIFICADOR), 
						getInt(jItemAgenda, JSON_KEY_TIPO_CERTIFICACION),
						getString(jItemAgenda, JSON_KEY_FECHA_REQUERIMIENTO),
						getString(jItemAgenda, JSON_KEY_FECHA_PROGRAMACION), 
						getString(jItemAgenda, JSON_KEY_UBICACION), 
						getString(jItemAgenda, JSON_KEY_CONTACTO), 
						getInt(jItemAgenda, JSON_KEY_TIPO_ACCION),
						null,
						null,
						"false");
						

				db.insertAgenda(agendaData);
				
			}
			
			db.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		
	}
	
	

}
