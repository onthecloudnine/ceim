package com.example.exampleceim;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.exampleceim.Agenda.AgendaData;
import com.example.exampleceim.Agenda.CertificacionEmpleadoData;
import com.example.exampleceim.Agenda.CertificacionEquipoData;
import com.example.exampleceim.Agenda.EmpleadoData;
import com.example.exampleceim.Agenda.EquipoData;
import com.example.exampleceim.initializer.Centro_Certificador;
import com.example.exampleceim.initializer.OticData;
import com.example.exampleceim.pautas_certificacion.GrupoPauta;
import com.example.exampleceim.pautas_certificacion.ItemPauta;
import com.example.exampleceim.pautas_certificacion.PautasCertificacion;
import com.example.exampleceim.pautas_certificacion.TipoCertificacionExternaEquipo;
import com.example.exampleceim.pautas_certificacion.TipoDocumentoObligatorioEmpleado;
import com.example.exampleceim.pautas_certificacion.TipoDocumentoObligatorioEquipo;
import com.example.exampleceim.usuarios.Persona;
import com.example.exampleceim.usuarios.Usuario;

public class DatabaseHandler{
	
	/************     TAG Logcat       **********/
    public static final String LOGTAG = DatabaseHandler.class.getName() + " LOG_TAG";
    public static final String QUERY_LOGTAG = "QUERY_TAG";

    /************      VERSION DB         **********/
	public static final int DATABASE_VERSION = 1;

	/************      Nombre DB      **********/
	public static final String DATABASE_NAME = "certificadorDB";

	/************      Tablas        **********/
	
	//Nombre de las Tablas.
	
	public static final String CENTER_OTIC_DATA_TABLE	 						= "center_oticdata";
	public static final String CENTER_CENTRO_CERTIFICADOR_TABLE 				= "center_centro_certificador";
	public static final String CENTER_PERSONAS_TABLE 							= "center_personas";
	public static final String CENTER_USUARIOS_TABLE 							= "center_usuarios";
	public static final String CENTER_PAUTAS_CERTIFICACION_TABLE				= "center_pautas";
	public static final String CENTER_PAUTAS_GRUPO_CERTIFICACION_TABLE			= "center_pautasGrupo";
	public static final String CENTER_PAUTAS_ITEM_CERTIFICACION_TABLE			= "center_pautasItems";
	public static final String CENTER_PAUTAS_DOC_OBLIGATORIOS_EQUIPO_TABLE		= "center_doc_obligatorio_equipo";
	public static final String CENTER_PAUTAS_DOC_OBLIGATORIOS_EMPLEADO_TABLE	= "center_doc_obligatorio_empleado";
	public static final String CENTER_PAUTAS_CERT_EXTERNAS_EQUIPO_TABLE			= "center_cert_externas_equipo";
	public static final String CENTER_FECHAS_SINCRONIZACION_TABLE				= "center_fechas_sincronizacion";
	
	
	// Tablas para guardar los datos de las Certificaciones.
	
//	public static final String CENTER_CERTIFICACION_EQUIPO_TABLE				= "center_certificacion_equipo";
	
	public static final String CENTER_AGENDA_TABLE										= "center_agenda";
	public static final String CENTER_PAUTA_DATA_ITEM_PAUTA_TABLE						= "center_pauta_data_item_pauta";			
	
	public static final String CENTER_PAUTA_DATA_EQUIPO_TABLE							= "center_pauta_data_equipo";
	public static final String CENTER_PAUTA_DATA_CERTIFICACION_EQUIPO_TABLE				= "center_pauta_data_certificacion_equipo";
	public static final String CENTER_PAUTA_DATA_INSTANCIA_CERTIFICACION_EQUIPO_TABLE	= "center_pauta_data_instancia_certificacion_equipo";
	
	public static final String CENTER_PAUTA_DATA_EMPLEADO_TABLE							= "center_pauta_data_empleado";
	public static final String CENTER_PAUTA_DATA_CERTIFICACION_EMPLEADO_TABLE			= "center_pauta_data_certificacion_empleado";
	public static final String CENTER_PAUTA_DATA_INSTANCIA_CERTIFICACION_EMPLEADO_TABLE	= "center_pauta_data_instancia_certificacion_empleado";
	
	public static final String CENTER_PAUTA_ITEMS_PAUTA_TABLE							= "center_pauta_items_pauta";
	
	
	/************      Tabla center_oticdata         **********/
	
	//Nombre Columnas Tabla: oticdata
	public static final String OTICDATA_ID_COL_NAME 			= "ID";
	public static final String OTICDATA_IMG_COL_NAME			= "IMG_LOGO_OTIC";
	public static final String OTICDATA_NOMBRE_OTIC_COL_NAME	= "NombreOtic";
	public static final String OTICDATA_DESC_COL_NAME		  	= "Descripcion";
	
	//N�mero Columnas Tabla: oticdata
	
	public static final int OTICDATA_ID_COL_NUM 	= 0;
	public static final int OTICDATA_IMG_COL_NUM	= 1;
	public static final int OTIDDATA_NOMBRE_COL_NUM	= 2;
	public static final int OTICDATA_DESC_COL_NUM 	= 3;
	
	
	/************      Tabla center_centro_certificador         **********/
	
	//Nombre Columnas tabla: centro_certificador
	
	public static final String CERTIFICADOR_ID_CENTRO_CERT_COL_NAME 	= "ID_CENTRO_CERTIFICADOR";
	public static final String CERTIFICADOR_IMG_COL_NAME 				= "IMG_LOGO";
	public static final String CERTIFICADOR_NOMBRE_COL_NAME 			= "Nombre";
	public static final String CERTIFICADOR_ID_COL_NAME					= "ID";
	
	//N�mero Columnas tabla: centro_certificador
	
	public static final int CERTIFICADOR_ID_CENTRO_CERT_COL_NUM	= 0;
	public static final int CERTIFICADOR_ID_COL_NUM				= 1;	
	public static final int CERTIFICADOR_IMG_COL_NUM			= 2;
	public static final int CERTIFICADOR_NOMBRE_COL_NUM			= 3;
	
	/************      Tabla center_usuarios         **********/
	
	// Nombre de las Columnas.
	
	public static final String USUARIO_ID_USUARIO_COL_NAME		= "ID_USUARIO";
	public static final String USUARIO_ID_COL_NAME 				= "ID";
	public static final String USUARIO_SALT_COL_NAME			= "Salt";
	public static final String USUARIO_NOMBRE_USUARIO_COL_NAME	= "NombreUsuario";
	public static final String USUARIO_PASS_COL_NAME			= "Password";
	
	//N�mero de las Columnas.
	
	public static final int USUARIO_ID_USUARIO_COL_NUM			= 0;
	public static final int USUARIO_ID_COL_NUM					= 1;
	public static final int USUARIO_SALT_COL_NUM				= 2;
	public static final int USUARIO_NOMBRE_USUARIO_COL_NUM		= 3;
	public static final int USUARIO_PASS_COL_NUM				= 4;

	
	/************      Tabla center_personas         **********/
	
	//Nombre de las columnas.
	
	public static final String PERSONA_ID_PERSONA_COL_NAME			= "ID_PERSONA";
	public static final String PERSONA_ID_CENTRO_CERT_COL_NAME 		= "ID_CENTRO_CERT";
	public static final String PERSONA_ID_USUARIO_COL_NAME			= "ID_USUARIO";
	public static final String PERSONA_TIPO_ACCION_COL_NAME			= "TipoAccion";
	public static final String PERSONA_ID_COL_NAME					= "ID";
	public static final String PERSONA_RUN_COL_NAME					= "RUN";
	public static final String PERSONA_DIGITO_VERIF_COL_NAME 		= "DigitoVerificador";
	public static final String PERSONA_NOMBRES_COL_NAME				= "Nombres";
	public static final String PERSONA_PRIMER_APELLIDO_COL_NAME		= "PrimerApellido";
	public static final String PERSONA_SEGUNDO_APELLIDO_COL_NAME	= "SegundoApellido";
	public static final String PERSONA_TELEFONO_COL_NAME 			= "Telefono";
	public static final String PERSONA_CELULAR_COL_NAME				= "Celular";
	
	//N�mero de las columnas.
	
	public static final int PERSONA_ID_PERSONA_COL_NUM				= 0;
	public static final int PERSONA_ID_CENTRO_CERT_COL_NUM			= 1;
	public static final int PERSONA_ID_USUARIO_COL_NUM				= 2;
	public static final int PERSONA_TIPO_ACCION_COL_NUM				= 3;
	public static final int PERSONA_ID_COL_NUM						= 4;
	public static final int PERSONA_RUN_COL_NUM						= 5;
	public static final int PERSONA_DIGITO_VERIF_COL_NUM			= 6;
	public static final int PERSONA_NOMBRES_COL_NUM					= 7;
	public static final int PERSONA_PRIMER_APELLIDO_COL_NUM			= 8;
	public static final int PERSONA_SEGUNDO_APELLIDO_COL_NUM		= 9;
	public static final int PERSONA_TELEFONO_COL_NUM				= 10;
	public static final int PERSONA_CELULAR_COL_NUM					= 11;
	
	/************      Tabla center_agenda        **********/
	
	public static final String AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NAME 	=	"IdProgramacionCertificacion";
	public static final String AGENDA_ID_PAUTA_CERTIFICACION_COL_NAME			=	"IdPautaCertificacion";
	public static final String AGENDA_ID_EQUIPO_COL_NAME						=	"IdEquipo";
	public static final String AGENDA_ID_EMPLEADO_COL_NAME						=	"IdEmpleado";
	public static final String AGENDA_ID_CERTIFICADOR_COL_NAME					=	"IdCertificador";
	public static final String AGENDA_TIPO_CERTIFICACION_COL_NAME				=	"TipoCertificacion";
	public static final String AGENDA_FECHA_REQUERIMIENTO_COL_NAME				=	"FechaRequerimento";
	public static final String AGENDA_FECHA_PROGRAMACION_COL_NAME				=	"FechaProgramacion";
	public static final String AGENDA_UBICACION_COL_NAME						=	"Ubicacion";
	public static final String AGENDA_CONTACTO_COL_NAME							=	"Contacto";
	public static final String AGENDA_TIPO_ACCION_COL_NAME						=	"TipoAccion";
	public static final String AGENDA_JSON_ENVIAR_COL_NAME						=	"JSONEnviar";
	public static final String AGENDA_JSON_RELLENAR_COL_NAME					=	"JSONRellenar";
	public static final String AGENDA_BO_FINALIZADO_COL_NAME					=	"Finalizado";
	
	
	//N� Columnas.
	public static final int AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NUM		=	0;
	public static final int AGENDA_ID_PAUTA_CERTIFICACION_COL_NUM				=	1;
	public static final int AGENDA_ID_EQUIPO_COL_NUM							=	2;
	public static final int AGENDA_ID_EMPLEADO_COL_NUM							=	3;
	public static final int AGENDA_ID_CERTIFICADOR_COL_NUM						=	4;
	public static final int AGENDA_TIPO_CERTIFICACION_COL_NUM					=	5;
	public static final int AGENDA_FECHA_REQUERIMIENTO_COL_NUM					=	6;
	public static final int AGENDA_FECHA_PROGRAMACION_COL_NUM					=	7;
	public static final int AGENDA_UBICACION_COL_NUM							=	8;
	public static final int AGENDA_CONTACTO_COL_NUM								=	9;
	public static final int AGENDA_TIPO_ACCION_COL_NUM							=	10;
	public static final int AGENDA_JSON_ENVIAR_COL_NUM							=	11;
	public static final int AGENDA_JSON_RELLENAR_COL_NUM						=	12;
	public static final int AGENDA_BO_FINALIZADO_COL_NUM						=	13;
	
	/************      Tabla center_pautas         **********/
	
	public static final String PAUTAS_CERT_ID_PAUTA_COL_NAME					= "ID_PAUTA_CERTIFICACION";
	public static final String PAUTAS_CERT_ID_CENTRO_CERT_COL_NAME				= "ID_CENTRO_CERTIFICADOR";
	public static final String PAUTAS_NOMBRE_PAUTA_CERT_COL_NAME				= "Nombre";
	public static final String PAUTAS_DESCRIPCION_COL_NAME						= "Descripcion";
	public static final String PAUTAS_TIPOCERTIFICACION_COL_NAME				= "TipoCertificacion";
	public static final String PAUTAS_CANTIDAD_INSTANCIAS_COL_NAME				= "CantidadInstancias";
	public static final String PAUTAS_INCLUYE_OBS_PORPREGUNTA_BOOLEAN_COL_NAME	= "ObservacionPorPreguntaBoolean";
	public static final String PAUTAS_INCLUYE_NO_APLICA_BOOLEAN_COL_NAME		= "IncluyeNoAplicaBoolean";
	public static final String PAUTAS_REVISION_COL_NAME							= "Revision";
	public static final String PAUTAS_IMAGEN_LOGO_CERTIFICACION_COL_NAME		= "ImgLogoCertificacion";
	public static final String PAUTAS_VENCIMIENTO_KM_HM_BOOLEAN_COL_NAME		= "VencimientoKM_HM_Boolean";
	public static final String PAUTAS_TIPO_ACCION_COL_NAME						= "TipoAccion";
	public static final String PAUTAS_INCLUYE__NUMERO_NIVEL_APROBACION_COL_NAME = "IncluyeNumeroNivelAprobacion";
	
	//N�mero de Columnas.
	
	public static final int PAUTAS_CERT_ID_PAUTA_COL_NUM					= 0;
	public static final int PAUTAS_CERT_ID_CENTRO_CERT_COL_NUM				= 1;
	public static final int PAUTAS_NOMBRE_PAUTA_CERT_COL_NUM				= 2;
	public static final int PAUTAS_DESCRIPCION_COL_NUM						= 3;
	public static final int PAUTAS_TIPOCERTIFICACION_COL_NUM				= 4;
	public static final int PAUTAS_CANTIDAD_INSTANCIAS_COL_NUM				= 5;
	public static final int PAUTAS_INCLUYE_OBS_PORPREGUNTA_BOOLEAN_COL_NUM	= 6;
	public static final int PAUTAS_INCLUYE_NO_APLICA_BOOLEAN_COL_NUM		= 7;
	public static final int PAUTAS_REVISION_COL_NUM							= 8;
	public static final int PAUTAS_IMAGEN_LOGO_CERTIFICACION_COL_NUM		= 9;
	public static final int PAUTAS_VENCIMIENTO_KM_HM_BOOLEAN_COL_NUM		= 10;
	public static final int PAUTAS_TIPO_ACCION_COL_NUM						= 11;
	public static final int PAUTAS_INCLUYE__NUMERO_NIVEL_APROBACION_COL_NUM	= 12;
	
	/************      Tabla center_pautasGrupos         **********/
	
	public static final String GRUPO_ID_GRUPOPAUTA_COL_NAME	= "ID_GRUPOPAUTA";
	public static final String GRUPO_ID_PAUTA_CERT_COL_NAME	= "ID_PAUTA_CERT";
	public static final String GRUPO_NOMBRE_COL_NAME		= "Nombre";
	public static final String GRUPO_ORDEN_COL_NAME			= "Orden";
	public static final String GRUPO_TIPOACCION_COL_NAME	= "TipoAccion";

	//N�mero de Columnas.
		
	public static final int GRUPO_ID_GRUPOPAUTA_COL_NUM		= 0;
	public static final int GRUPO_ID_PAUTA_CERT_COL_NUM		= 1;
	public static final int GRUPO_NOMBRE_COL_NUM			= 3;
	public static final int GRUPO_ORDEN_COL_NUM				= 4;
	public static final int GRUPO_TIPO_ACCION_COL_NUM		= 5;
	
	
	/************      Tabla center_pautasItems         **********/
	
	public static final String ITEM_ID_PAUTA_COL_NAME		= "ID_ITEM_PAUTA";	
	public static final String ITEM_ID_GRUPO_COL_NAME		= "ID_GRUPO_PAUTA";
	public static final String ITEM_ID_PAUTA_CERT_COL_NAME	= "ID_PAUTA_CERT";
	public static final String ITEM_NOMBRE_COL_NAME			= "Nombre";
	public static final String ITEM_ORDEN_COL_NAME			= "Orden";
	public static final String ITEM_TIPO_ACCION_COL_NAME	= "TipoAccion";
	
	//N�mero de Columnas.
	
	public static final int ITEM_ID_PAUTA_COL_NUM			= 0;
	public static final int ITEM_ID_GRUPO_COL_NUM			= 1;
	public static final int ITEM_ID_PAUTA_CERT_COL_NUM		= 2;
	public static final int ITEM_NOMBRE_COL_NUM				= 3;
	public static final int ITEM_ORDEN_COL_NUM				= 4;
	public static final int ITEM_TIPO_ACCION_COL_NUM		= 5;
	
	/************      Tabla center_doc_obligatorio_equipo         **********/
	
	public static final String DOC_EQUIPO_ID_TIPO_DOC_OBL_EQUIPO_COL_NAME		= "IdTipoDocumentoObligatorioEquipo";
	public static final String DOC_EQUIPO_ID_PAUTA_CERT_COL_NAME				= "IdPautaCertificacion";
	public static final String DOC_EQUIPO_NOMBRE_COL_NAME						= "Nombre";
	public static final String DOC_EQUIPO_ORDEN_COL_NAME						= "Orden";
	public static final String DOC_EQUIPO_INCLUYE_FECHA_VENCIMIENTO_COL_NAME	= "IncluyeFechaVencimiento";
	public static final String DOC_EQUIPO_TIPO_ACCION_COL_NAME					= "TipoAccion";
	
	//N�mero de Columnas.
	
	public static final int DOC_EQUIPO_ID_TIPO_DOC_OBL_EQUIPO_COL_NUM			= 1;
	public static final int DOC_EQUIPO_ID_PAUTA_CERT_COL_NUM					= 2;
	public static final int DOC_EQUIPO_NOMBRE_COL_NUM							= 3;
	public static final int DOC_EQUIPO_ORDEN_COL_NUM							= 4;
	public static final int DOC_EQUIPO_INCLUYE_FECHA_VENCIMIENTO_COL_NUM		= 5;
	public static final int DOC_EQUIPO_TIPO_ACCION_COL_NUM						= 6;
	
	/************      Tabla center_cert_externas_equipo        **********/
	
	public static final String CERT_EXT_EQUIPO_ID_TIPO_CERT_COL_NAME			= "IdTipoCertificacionExternaEquipo";
	public static final String CERT_EXT_EQUIPO_ID_PAUTA_CERT_COL_NAME			= "IdPautaCertificacion";
	public static final String CERT_EXT_EQUIPO_NOMBRE_COL_NAME					= "Nombre";
	public static final String CERT_EXT_EQUIPO_ORDEN_COL_NAME					= "Orden";
	public static final String CERT_EXT_EQUIPO_TIPO_ACCION_COL_NAME				= "TipoAccion";
	public static final String CERT_EXT_EQUIPO_TIPO_VENCIMIENTO_COL_NAME		= "TipoVencimiento";
	
	//N�mero de Columnas.
	
	public static final int CERT_EXT_EQUIPO_ID_TIPO_CERT_COL_NUM		= 1;
	public static final int CERT_EXT_EQUIPO_ID_PAUTA_CERT_COL_NUM		= 2;
	public static final int CERT_EXT_EQUIPO_NOMBRE_COL_NUM				= 3;
	public static final int CERT_EXT_EQUIPO_ORDEN_COL_NUM				= 4;
	public static final int CERT_EXT_EQUIPO_TIPO_ACCION_COL_NUM			= 5;
	public static final int CERT_EXT_EQUIPO_TIPO_VENCIMIENTO_COL_NUM	= 6;
	
	
	/************      Tabla center_doc_obligatorio_empleado        **********/
	
	public static final String DOC_EMPLEADO_ID_TIPO_DOC_OBL_EMPLEADO_COL_NAME   = "IdTipoDocumentoObligatorioEmpleado";
	public static final String DOC_EMPLEADO_ID_PAUTA_CERT_COL_NAME				= "IdPautaCertificacion";
	public static final String DOC_EMPLEADO_NOMBRE_COL_NAME						= "Nombre";
	public static final String DOC_EMPLEADO_ORDEN_COL_NAME						= "Orden";
	public static final String DOC_EMPLEADO_INCLUYE_FECHA_VENCIMIENTO_COL_NAME	= "IncluyeFechaVencimiento";
	public static final String DOC_EMPLEADO_TIPO_ACCION_COL_NAME				= "TipoAccion";
	
	//N�mero de Columnas.
	
	public static final int DOC_EMPLEADO_ID_TIPO_DOC_OBL_EMPLEADO_COL_NUM		= 1;
	public static final int DOC_EMPLEADO_ID_PAUTA_CERT_COL_NUM					= 2;
	public static final int DOC_EMPLEADO_NOMBRE_COL_NUM							= 3;
	public static final int DOC_EMPLEADO_ORDEN_COL_NUM							= 4;
	public static final int DOC_EMPLEADO_INCLUYE_FECHA_VENCIMIENTO_COL_NUM		= 5;
	public static final int DOC_EMPLEADO_TIPO_ACCION_COL_NUM					= 6;
	
	/************      Tabla center_fechas_sincronizacion         **********/
	
	public static String FECHA_ID_COL_NAME			= "ID_FECHA";
	public static String FECHA_TIPO_SINC_COL_NAME	= "TipoSync";
	public static String FECHA_DATE_COL_NAME		= "FechaSync";
	
	public static int FECHA_ID_SYNC_USUARIOS = 1;
	
	//N�mero de las columnas.
	
	public static final int FECHA_ID_COL_NUM		= 0;
	public static final int FECHA_TIPO_SINC_COL_NUM	= 1;
	public static final int FECHA_DATE_COL_NUM		= 2;
	
	
	/************ Tabla  center_pauta_data_item_pauta  ***********************/

	public static final String DATA_ITEM_PAUTA_ID_COL_NAME			= "IdItemPauta"; 
	public static final String DATA_ITEM_PAUTA_AFIRMATIVO_COL_NAME	= "Afirmativo";
	public static final String DATA_ITEM_PAUTA_NO_APLICA_COL_NAME	= "NoAplica";
	public static final String DATA_ITEM_PAUTA_OBSERVACION_COL_NAME	= "Observacion";
	
	//N�mero de Columnas
	
	public static final int DATA_ITEM_PAUTA_ID_COL_NUM			= 0;
	public static final int DATA_ITEM_PAUTA_AFIRMATIVO_COL_NUM	= 1;
	public static final int DATA_ITEM_PAUTA_NO_APLICA_COL_NUM 	= 2;
	public static final int DATA_ITEM_PAUTA_OBSERVACION_COL_NUM	= 3;
	
	/************ Tabla  center_pauta_data_equipo ********************/
	
	public static final String DATA_EQUIPO_ID_EQUIPO_COL_NAME 						= "IdEquipo";
	public static final String DATA_EQUIPO_MARCA_COL_NAME							= "Marca";
	public static final String DATA_EQUIPO_MODELO_COL_NAME							= "Modelo";
	public static final String DATA_EQUIPO_AÑO_COL_NAME								= "A�o";
	public static final String DATA_EQUIPO_PLACA_PATENTE_COL_NAME					= "PlacaPatente";
	public static final String DATA_EQUIPO_CODIGO_INTERNO_COL_NAME					= "CodigoInterno";
	public static final String DATA_EQUIPO_COLOR_COL_NAME							= "Color";
	public static final String DATA_EQUIPO_NUMERO_MOTOR_COL_NAME					= "NumeroMotor";
	public static final String DATA_EQUIPO_NUMERO_SERIE_COL_NAME					= "NumeroSerie";
	public static final String DATA_EQUIPO_NUMERO_FACTURA_COL_NAME					= "NumeroFactura";
	public static final String DATA_EQUIPO_NUMERO_GUIA_DESPACHO_COL_NAME			= "NumeroGuiaDespacho";
	public static final String DATA_EQUIPO_CAPACIDAD_COL_NAME						= "Capacidad";
	public static final String DATA_EQUIPO_POTENCIA_COL_NAME						= "Potencia";
	public static final String DATA_EQUIPO_FECHA_CERTIFICACION_COL_NAME				= "FechaCertificacion";
	public static final String DATA_EQUIPO_TIPO_ACCION_COL_NAME						= "TipoAccion";
	
	//N�meros columnas
	
	public static final int DATA_EQUIPO_ID_EQUIPO_COL_NUM						= 0;
	public static final int DATA_EQUIPO_MARCA_COL_NUM							= 1;
	public static final int DATA_EQUIPO_MODELO_COL_NUM							= 2;
	public static final int DATA_EQUIPO_AÑO_NUM							= 3;
	public static final int DATA_EQUIPO_PLACA_PATENTE_COL_NUM					= 4;
	public static final int DATA_EQUIPO_CODIGO_INTERNO_COL_NUM					= 5;
	public static final int DATA_EQUIPO_COLOR_COL_NUM							= 6;
	public static final int DATA_EQUIPO_NUMERO_MOTOR_COL_NUM					= 7;
	public static final int DATA_EQUIPO_NUMERO_SERIE_COL_NUM					= 8;
	public static final int DATA_EQUIPO_NUMERO_FACTURA_COL_NUM					= 9;
	public static final int DATA_EQUIPO_NUMERO_GUIA_DESPACHO_COL_NUM			= 10;
	public static final int DATA_EQUIPO_CAPACIDAD_COL_NUM						= 11;
	public static final int DATA_EQUIPO_POTENCIA_COL_NUM						= 12;
	public static final int DATA_EQUIPO_FECHA_CERTIFICACION_COL_NUM				= 13;
	public static final int DATA_EQUIPO_TIPO_ACCION_COL_NUM						= 14;
	
	/****************** Tabla center_pauta_data_certificacion_equipo *****************************/

	public static final String DATA_CERTIFICACION_EQUIPO_ID_CERTIFICACION_EQUIPO_COL_NAME		= "IdCertificacionEquipo";
	public static final String DATA_CERTIFICACION_EQUIPO_ID_EQUIPO_CERTIFICAR_COL_NAME			= "IdEquipoCertificar";
	public static final String DATA_CERTIFICACION_EQUIPO_ID_PAUTA_CERTIFICACION_COL_NAME		= "IdPautaCertificacion";
	public static final String DATA_CERTIFICACION_EQUIPO_EMPRESA_RESPONSABLE_COL_NAME			= "EmpresaResponsable";
	public static final String DATA_CERTIFICACION_EQUIPO_LUGAR_INSPECCION_COL_NAME				= "LugarInspeccion";
	public static final String DATA_CERTIFICACION_EQUIPO_NPPA_COL_NAME							= "NPPA";
	public static final String DATA_CERTIFICACION_EQUIPO_COTIZACION_COL_NAME					= "Cotizacion";
	public static final String DATA_CERTIFICACION_EQUIPO_ITEM_COL_NAME							= "Item";
	public static final String DATA_CERTIFICACION_EQUIPO_HOROMETRO_COL_NAME						= "Horometro";
	public static final String DATA_CERTIFICACION_EQUIPO_KILOMETRAJE_COL_NAME					= "Kilometraje";
	public static final String DATA_CERTIFICACION_EQUIPO_TIPO_CERTIFICACION_COL_NAME			= "TipoCertificacion";
	public static final String DATA_CERTIFICACION_EQUIPO_FECHA_ENTREGA_DOCUMENTACION_COL_NAME	= "FechaEntregaDocumentacion";
	public static final String DATA_CERTIFICACION_EQUIPO_FECHA_APROBACION_COL_NAME				= "FechaAprobacion";
	public static final String DATA_CERTIFICACION_EQUIPO_OPERA_COL_NAME							= "EquipoOpera";
	public static final String DATA_CERTIFICACION_EQUIPO_FECHA_VIGENCIA_COL_NAME				= "FechaVigencia";
	public static final String DATA_CERTIFICACION_EQUIPO_TIPO_ACCION_COL_NAME					= "TipoAccion";
	
	//N�mero Columnas.
	
	public static final int DATA_CERTIFICACION_EQUIPO_ID_CERTIFICACION_EQUIPO_COL_NUM		= 0;
	public static final int DATA_CERTIFICACION_EQUIPO_ID_EQUIPO_CERTIFICAR_COL_NUM			= 1;
	public static final int DATA_CERTIFICACION_EQUIPO_ID_PAUTA_CERTIFICACION_COL_NUM		= 2;
	public static final int DATA_CERTIFICACION_EQUIPO_EMPRESA_RESPONSABLE_COL_NUM			= 3;
	public static final int DATA_CERTIFICACION_EQUIPO_LUGAR_INSPECCION_COL_NUM				= 4;
	public static final int DATA_CERTIFICACION_EQUIPO_NPPA_COL_NUM							= 5;
	public static final int DATA_CERTIFICACION_EQUIPO_COTIZACION_COL_NUM					= 6;
	public static final int DATA_CERTIFICACION_EQUIPO_ITEM_COL_NUM							= 7;
	public static final int DATA_CERTIFICACION_EQUIPO_HOROMETRO_COL_NUM						= 8;
	public static final int DATA_CERTIFICACION_EQUIPO_KILOMETRAJE_COL_NUM					= 9;
	public static final int DATA_CERTIFICACION_EQUIPO_TIPO_CERTIFICACION_COL_NUM			= 10;
	public static final int DATA_CERTIFICACION_EQUIPO_FECHA_ENTREGA_DOCUMENTACION_COL_NUM	= 11;
	public static final int DATA_CERTIFICACION_EQUIPO_FECHA_APROBACION_COL_NUM				= 12;
	public static final int DATA_CERTIFICACION_EQUIPO_OPERA_COL_NUM							= 13;
	public static final int DATA_CERTIFICACION_EQUIPO_FECHA_VIGENCIA_COL_NUM				= 14;
	public static final int DATA_CERTIFICACION_EQUIPO_TIPO_ACCION_COL_NUM					= 15;
	
	/*********** Tabla center_pauta_data_instancia_certificacion_equipo *****************************/
	
	public static final String DATA_PAUTA_INSTANCIA_CERT_EQUIPO_ID_INST_CERT_EQUIPO_COL_NAME			= "IdInstanciaCertificacionEquipo";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EQUIPO_ID_CERT_EQUIPO_COL_NAME					= "IdCertificacionEquipo";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EQUIPO_ID_CERTIFICADOR_COL_NAME				= "IdCertificador"; // Id Certificador = ID Usuario;
	public static final String DATA_PAUTA_INSTANCIA_CERT_EQUIPO_ID_PROGRAMACION_CERT_COL_NAME			= "IdProgramacionCertificacion";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EQUIPO_NUMERO_INST_CERTIFICACION_COL_NAME		= "NumeroInstanciaCertificacion";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EQUIPO_FECHA_INSTANCIA_COL_NAME				= "FechaInstancia";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EQUIPO_LUGAR_COL_NAME							= "Lugar";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EQUIPO_SITUACION_APROBACION_COL_NAME			= "SituacionAprobacion";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EQUIPO_OBSERVACION_SITUACION_EQUIPO_COL_NAME	= "ObservacionSituacionEquipo";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EQUIPO_OBSERVACION_FINAL_COL_NAME				= "ObservacionFinal";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EQUIPO_RESULTADO_CERTIFICACION_COL_NAME		= "ResultadoCertificacion";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EQUIPO_LATITUD_COL_NAME						= "Latitud";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EQUIPO_LONGITUD_COL_NAME						= "Longitud";
	
	//Numero Tablas
	
	public static final int DATA_PAUTA_INSTANCIA_CERT_EQUIPO_ID_INST_CERT_EQUIPO_COL_NUM			= 0;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EQUIPO_ID_CERT_EQUIPO_COL_NUM					= 1;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EQUIPO_ID_CERTIFICADOR_COL_NUM				= 2;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EQUIPO_ID_PROGRAMACION_CERT_COL_NUM			= 3;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EQUIPO_NUMERO_INST_CERTIFICACION_COL_NUM		= 4;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EQUIPO_FECHA_INSTANCIA_COL_NUM				= 5;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EQUIPO_LUGAR_COL_NUM							= 6;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EQUIPO_SITUACION_APROBACION_COL_NUM			= 7;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EQUIPO_OBSERVACION_SITUACION_EQUIPO_COL_NUM	= 8;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EQUIPO_OBSERVACION_FINAL_COL_NUM				= 9;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EQUIPO_RESULTADO_CERTIFICACION_COL_NUM		= 10;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EQUIPO_LATITUD_COL_NUM						= 11;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EQUIPO_LONGITUD_COL_NUM						= 12;
	
	/***************** Tabla  center_pauta_data_empleado *****************************************************/
//	public static final String CENTER_PAUTA_DATA_EMPLEADO_TABLE							= "center_pauta_data_empleado";
	
	public static final String DATA_EMPLEADO_ID_COL_NAME					=	"IdEmpleado";
	public static final String DATA_EMPLEADO_RUN_COL_NAME 					=	"RUN";
	public static final String DATA_EMPLEADO_DIGITO_VERIFICADOR_COL_NAME	=	"DigitoVerificador";
	public static final String DATA_EMPLEADO_TELEFONO_COL_NAME 				=	"Telefono";
	public static final String DATA_EMPLEADO_NOMBRES_COL_NAME				=	"Nombres";
	public static final String DATA_EMPLEADO_PRIMER_APELLIDO_COL_NAME		=	"PrimerApellido";
	public static final String DATA_EMPLEADO_SEGUNDO_APELLIDO_COL_NAME		=	"SegundoApellido";
	public static final String DATA_EMPLEADO_CELULAR_COL_NAME				=	"Celular";
	public static final String DATA_EMPLEADO_TIPO_ACCION_COL_NAME			=	"TipoAccion";
	
	//N�mero Columnas.
	
	public static final int DATA_EMPLEADO_ID_COL_NUM					= 0;
	public static final int DATA_EMPLEADO_RUN_COL_NUM					= 1;
	public static final int DATA_EMPLEADO_DIGITO_VERIFICADOR_COL_NUM	= 2;
	public static final int DATA_EMPLEADO_TELEFONO_COL_NUM				= 3;
	public static final int DATA_EMPLEADO_NOMBRES_COL_NUM				= 4;
	public static final int DATA_EMPLEADO_PRIMER_APELLIDO_COL_NUM		= 5;
	public static final int DATA_EMPLEADO_SEGUNDO_APELLIDO_COL_NUM		= 6;
	public static final int DATA_EMPLEADO_CELULAR_COL_NUM				= 7;
	public static final int DATA_EMPLEADO_TIPO_ACCION_COL_NUM			= 8;
	
/************* 	Tabla center_pauta_data_certificacion_empleado ****************************************/
	
	public static final String DATA_CERTIFICACION_EMPLEADO_ID_CERTIFICACION_EMPLEADO_COL_NAME		= "IdCertificacionEmpleado";
	public static final String DATA_CERTIFICACION_EMPLEADO_ID_PAUTA_CERTIFICACION_COL_NAME			= "IdPautaCertificacion";
	public static final String DATA_CERTIFICACION_EMPLEADO_ID_EMPLEADO_COL_NAME						= "IdEmpleado";
	public static final String DATA_CERTIFICACION_EMPLEADO_EQUIPO_ASOCIADO_COL_NAME					= "EquipoAsociado";
	public static final String DATA_CERTIFICACION_EMPLEADO_EQUIPO_ASOCIADO_TONELADA_COL_NAME		= "EquipoAsociadoTonelada";
	public static final String DATA_CERTIFICACION_EMPLEADO_LUGAR_EVALUACION_COL_NAME				= "LugarEvaluacion";
	public static final String DATA_CERTIFICACION_EMPLEADO_NUMERO_CONSTANCIA_COL_NAME				= "NumeroConstancia";
	public static final String DATA_CERTIFICACION_EMPLEADO_NUMERO_NIVEL_APROBACION_COL_NAME			= "NumeroNivelAprobacion";
	public static final String DATA_CERTIFICACION_EMPLEADO_FECHA_COL_NAME							= "Fecha";
	public static final String DATA_CERTIFICACION_EMPLEADO_FECHA_CERTIFICACION_COL_NAME				= "FechaCertificacion";
	public static final String DATA_CERTIFICACION_EMPLEADO_FECHA_VIGENCIA_COL_NAME					= "FechaVigencia";
	public static final String DATA_CERTIFICACION_EMPLEADO_TIPO_ACCION_COL_NAME						= "TipoAccion";
	
	public static final int DATA_CERTIFICACION_EMPLEADO_ID_CERTIFICACION_EMPLEADO_COL_NUM		= 0;
	public static final int DATA_CERTIFICACION_EMPLEADO_ID_PAUTA_CERTIFICACION_COL_NUM			= 1;
	public static final int DATA_CERTIFICACION_EMPLEADO_ID_EMPLEADO_COL_NUM						= 2;
	public static final int DATA_CERTIFICACION_EMPLEADO_EQUIPO_ASOCIADO_COL_NUM					= 3;
	public static final int DATA_CERTIFICACION_EMPLEADO_EQUIPO_ASOCIADO_TONELADA_COL_NUM		= 4;
	public static final int DATA_CERTIFICACION_EMPLEADO_LUGAR_EVALUACIO_COL_NUM					= 5;
	public static final int DATA_CERTIFICACION_EMPLEADO_NUMERO_CONSTANCIA_COL_NUM				= 6;
	public static final int DATA_CERTIFICACION_EMPLEADO_NUMERO_NIVEL_APROBACION_COL_NUM			= 7;
	public static final int DATA_CERTIFICACION_EMPLEADO_FECHA_COL_NUM							= 8;
	public static final int DATA_CERTIFICACION_EMPLEADO_FECHA_CERTIFICACION_COL_NUM				= 9;
	public static final int DATA_CERTIFICACION_EMPLEADO_FECHA_VIGENCIA_COL_NUM					= 10;
	public static final int DATA_CERTIFICACION_EMPLEADO_TIPO_ACCION_COL_NUM						= 11;
	
	
/*********** Tabla center_pauta_data_instancia_certificacion_empleado *****************************/
	
	public static final String DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_ID_INST_CERT_EMPLEADO_COL_NAME			= "IdInstanciaCertificacionEmpleado";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_ID_CERT_EMPLEADO_COL_NAME					= "IdCertificacionEmpleado";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_ID_CERTIFICADOR_COL_NAME					= "IdCertificador"; // Id Certificador = ID Usuario;
	public static final String DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_ID_PROGRAMACION_CERT_COL_NAME				= "IdProgramacionCertificacion";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_NUMERO_INST_CERTIFICACION_COL_NAME		= "NumeroInstanciaCertificacion";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_FECHA_INSTANCIA_COL_NAME					= "FechaInstancia";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_LUGAR_COL_NAME							= "Lugar";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_SITUACION_APROBACION_COL_NAME				= "SituacionAprobacion";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_OBSERVACION_SITUACION_EQUIPO_COL_NAME		= "ObservacionSituacionEquipo";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_OBSERVACION_FINAL_COL_NAME				= "ObservacionFinal";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_RESULTADO_CERTIFICACION_COL_NAME			= "ResultadoCertificacion";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_LATITUD_COL_NAME							= "Latitud";
	public static final String DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_LONGITUD_COL_NAME							= "Longitud";
	
	//Numero Tablas
	
	public static final int DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_ID_INST_CERT_EMPLEADO_COL_NUM			= 0;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_ID_CERT_EQUIPO_COL_NUM					= 1;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_ID_CERTIFICADOR_COL_NUM					= 2;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_ID_PROGRAMACION_CERT_COL_NUM				= 3;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_NUMERO_INST_CERTIFICACION_COL_NUM		= 4;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_FECHA_INSTANCIA_COL_NUM					= 5;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_LUGAR_COL_NUM							= 6;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_SITUACION_APROBACION_COL_NUM				= 7;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_OBSERVACION_SITUACION_EQUIPO_COL_NUM		= 8;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_OBSERVACION_FINAL_COL_NUM				= 9;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_RESULTADO_CERTIFICACION_COL_NUM			= 10;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_LATITUD_COL_NUM							= 11;
	public static final int DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_LONGITUD_COL_NUM							= 12;
	
	//Variables estaticas.
	private SQLiteDatabase   	 mDb = null;	
	private centroInfoDBOpenHelper mDbHelper = null;
	
	private static class centroInfoDBOpenHelper extends SQLiteOpenHelper{

		public centroInfoDBOpenHelper(Context context, String name,
				CursorFactory factory, int version) {
			super(context, name, factory, version);
			// TODO Auto-generated constructor stub
		}
		
		static final String CENTER_OTIC_DATA_TABLE_CREATE =
				"create table " + CENTER_OTIC_DATA_TABLE + 
				" (" + 
				OTICDATA_ID_COL_NAME       	    + " integer primary key, " + 
				OTICDATA_IMG_COL_NAME      		+ " blob not null, " + 
				OTICDATA_NOMBRE_OTIC_COL_NAME   + " text not null, " +
				OTICDATA_DESC_COL_NAME 			+ " text not null " + 
				");";
		
		static final String CENTER_CENTRO_CERTIF_TABLE_CREATE =
				"create table " + CENTER_CENTRO_CERTIFICADOR_TABLE +
				" (" +
				CERTIFICADOR_ID_CENTRO_CERT_COL_NAME 	+ " integer primary key, "+
				CERTIFICADOR_IMG_COL_NAME				+ " blob not null, " +
				CERTIFICADOR_NOMBRE_COL_NAME			+ " text not null, " +
				CERTIFICADOR_ID_COL_NAME				+ " integer not null " +
				");";
		
		static final String CENTER_AGENDA_TABLE_CREATE	=
				"create table " + CENTER_AGENDA_TABLE +
				" (" +
				AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NAME 	+ " integer primary key, "+
				AGENDA_ID_PAUTA_CERTIFICACION_COL_NAME			+ " integer, " +
				AGENDA_ID_EQUIPO_COL_NAME						+ " integer, " +
				AGENDA_ID_EMPLEADO_COL_NAME						+ " integer, " +
				AGENDA_ID_CERTIFICADOR_COL_NAME					+ " integer, " +
				AGENDA_TIPO_CERTIFICACION_COL_NAME				+ " integer, " +
				AGENDA_FECHA_REQUERIMIENTO_COL_NAME				+ " text, " +
				AGENDA_FECHA_PROGRAMACION_COL_NAME				+ " text, " +
				AGENDA_UBICACION_COL_NAME						+ " text, " +
				AGENDA_CONTACTO_COL_NAME						+ " text, " +
				AGENDA_TIPO_ACCION_COL_NAME						+ " integer, " +
				AGENDA_JSON_ENVIAR_COL_NAME						+ " text, " +
				AGENDA_JSON_RELLENAR_COL_NAME					+ " text, " +
				AGENDA_BO_FINALIZADO_COL_NAME					+ " text " + 
				");";
		
		static final String CENTER_PERSONAS_TABLE_CREATE =
				"create table " + CENTER_PERSONAS_TABLE +
				" (" +
				PERSONA_ID_PERSONA_COL_NAME 		+ " integer primary key, "+
				PERSONA_ID_CENTRO_CERT_COL_NAME 	+ " integer not null, " +
				PERSONA_ID_USUARIO_COL_NAME			+ " integer not null, " +
				PERSONA_TIPO_ACCION_COL_NAME		+ " integer not null, " +
				PERSONA_ID_COL_NAME					+ " integer not null, " +
				PERSONA_RUN_COL_NAME				+ " integer not null, " +
				PERSONA_DIGITO_VERIF_COL_NAME		+ " text not null, " +
				PERSONA_NOMBRES_COL_NAME			+ " text not null, " +
				PERSONA_PRIMER_APELLIDO_COL_NAME	+ " text not null, " +
				PERSONA_SEGUNDO_APELLIDO_COL_NAME	+ " text not null, " +
				PERSONA_TELEFONO_COL_NAME			+ " text not null, " +
				PERSONA_CELULAR_COL_NAME			+ " text not null " +
				");";
		
		static final String CENTER_USUARIOS_TABLE_CREATE = 
				"create table " + CENTER_USUARIOS_TABLE +
				" (" + 
				USUARIO_ID_USUARIO_COL_NAME			+ " integer primary key, " +
				USUARIO_ID_COL_NAME 				+ " integer not null, "+
				USUARIO_SALT_COL_NAME				+ " text not null, " +
				USUARIO_NOMBRE_USUARIO_COL_NAME		+ " text, " +
				USUARIO_PASS_COL_NAME				+ " text not null " +
				");";
		
		static final String CENTER_FECHA_SYNC_TABLE_CREATE =
				"create table " + CENTER_FECHAS_SINCRONIZACION_TABLE +
				" (" +
				FECHA_ID_COL_NAME			+ " integer primary key autoincrement, " +
				FECHA_TIPO_SINC_COL_NAME	+ " text not null, " +
				FECHA_DATE_COL_NAME			+ " text not null " +
				");";
		
		static final String CENTER_PAUTAS_TABLE_CREATE =
				"create table " + CENTER_PAUTAS_CERTIFICACION_TABLE +
				" (" +
				PAUTAS_CERT_ID_PAUTA_COL_NAME 						+ " integer primary key, " +
				PAUTAS_CERT_ID_CENTRO_CERT_COL_NAME					+ " integer, " +
				PAUTAS_NOMBRE_PAUTA_CERT_COL_NAME					+ " text, " +
				PAUTAS_DESCRIPCION_COL_NAME							+ " text, "	+
				PAUTAS_TIPOCERTIFICACION_COL_NAME					+ " text, " + 
				PAUTAS_CANTIDAD_INSTANCIAS_COL_NAME					+ " text, " +
				PAUTAS_INCLUYE_OBS_PORPREGUNTA_BOOLEAN_COL_NAME     + " integer, " +
				PAUTAS_INCLUYE_NO_APLICA_BOOLEAN_COL_NAME			+ " integer, " +
				PAUTAS_REVISION_COL_NAME							+ " text, " +
				PAUTAS_IMAGEN_LOGO_CERTIFICACION_COL_NAME			+ " text, " +
				PAUTAS_VENCIMIENTO_KM_HM_BOOLEAN_COL_NAME			+ " integer, " +
				PAUTAS_TIPO_ACCION_COL_NAME							+ " integer, " +
				PAUTAS_INCLUYE__NUMERO_NIVEL_APROBACION_COL_NAME	+ " boolean " +
				");";
		
		static final String CENTER_PAUTAS_GRUPO_TABLE_CREATE =
				"create table " + CENTER_PAUTAS_GRUPO_CERTIFICACION_TABLE +
				" (" +
				GRUPO_ID_GRUPOPAUTA_COL_NAME	+ " integer primary key, " +
				GRUPO_ID_PAUTA_CERT_COL_NAME	+ " intenger, "+
				GRUPO_NOMBRE_COL_NAME			+ " text, " +
				GRUPO_ORDEN_COL_NAME			+ " integer, " +
				GRUPO_TIPOACCION_COL_NAME		+ " integer " +
				");";
		
		static final String CENTER_PAUTAS_ITEM_TABLE_CREATE = 
				"create table " + CENTER_PAUTAS_ITEM_CERTIFICACION_TABLE + 
				" (" +
				ITEM_ID_PAUTA_COL_NAME		+ " integer primary key, " +
				ITEM_ID_GRUPO_COL_NAME		+ " integer, " +
				ITEM_ID_PAUTA_CERT_COL_NAME	+ " integer, " +
				ITEM_NOMBRE_COL_NAME		+ " text, " +
				ITEM_ORDEN_COL_NAME			+ " integer, " +
				ITEM_TIPO_ACCION_COL_NAME	+ " integer " +
				");";
		
		static final String CENTER_DOCUMENTO_OBLIGATORIO_EQUIPO_TABLE_CREATE  =
				"create table " + CENTER_PAUTAS_DOC_OBLIGATORIOS_EQUIPO_TABLE +
				" (" +
				DOC_EQUIPO_ID_TIPO_DOC_OBL_EQUIPO_COL_NAME		+ " integer primary key, " +
				DOC_EQUIPO_ID_PAUTA_CERT_COL_NAME				+ " integer, " +
				DOC_EQUIPO_NOMBRE_COL_NAME						+ " text, " +
				DOC_EQUIPO_ORDEN_COL_NAME						+ " integer, " +
				DOC_EQUIPO_INCLUYE_FECHA_VENCIMIENTO_COL_NAME	+ " integer, " +
				DOC_EQUIPO_TIPO_ACCION_COL_NAME					+ " integer " +
				");";
		
		static final String CENTER_CERT_EXT_EQUIPO_TABLE_CREATE =
				"create table " + CENTER_PAUTAS_CERT_EXTERNAS_EQUIPO_TABLE +
				" (" +
				CERT_EXT_EQUIPO_ID_TIPO_CERT_COL_NAME			+ " integer primary key, " +
				CERT_EXT_EQUIPO_ID_PAUTA_CERT_COL_NAME			+ " integer, " +
				CERT_EXT_EQUIPO_NOMBRE_COL_NAME					+ " text, " +
				CERT_EXT_EQUIPO_ORDEN_COL_NAME					+ " integer, " +
				CERT_EXT_EQUIPO_TIPO_ACCION_COL_NAME			+ " integer, " +
				CERT_EXT_EQUIPO_TIPO_VENCIMIENTO_COL_NAME		+ " integer " +
				");";
		
		static final String CENTER_PAUTAS_DOC_OBLIGATORIOS_EMPLEADO_TABLE_CREATE =
			"create table " + CENTER_PAUTAS_DOC_OBLIGATORIOS_EMPLEADO_TABLE +
			" (" +
			DOC_EMPLEADO_ID_TIPO_DOC_OBL_EMPLEADO_COL_NAME		+ " integer primary key, " +
			DOC_EMPLEADO_ID_PAUTA_CERT_COL_NAME					+ " integer, " +
			DOC_EMPLEADO_NOMBRE_COL_NAME						+ " text, " +
			DOC_EMPLEADO_ORDEN_COL_NAME							+ " integer, " +
			DOC_EMPLEADO_INCLUYE_FECHA_VENCIMIENTO_COL_NAME		+ " integer, " +
			DOC_EMPLEADO_TIPO_ACCION_COL_NAME					+ " integer " +
			");";
		
		static final String CENTER_PAUTA_DATA_ITEM_PAUTA_TABLE_CREATE = 
			"create table " + CENTER_PAUTA_DATA_ITEM_PAUTA_TABLE +
			" (" +
			DATA_ITEM_PAUTA_ID_COL_NAME				+ " integer primary key, " +
			DATA_ITEM_PAUTA_AFIRMATIVO_COL_NAME		+ " text, " + 
			DATA_ITEM_PAUTA_NO_APLICA_COL_NAME		+ " text, " +
			DATA_ITEM_PAUTA_OBSERVACION_COL_NAME	+ " text " +
			");";
					
		
		static final String CENTER_PAUTA_DATA_EQUIPO_TABLE_CREATE = 
			"create table " + CENTER_PAUTA_DATA_EQUIPO_TABLE +
			" (" +
			DATA_EQUIPO_ID_EQUIPO_COL_NAME 						+ " integer primary key, " +
			DATA_EQUIPO_MARCA_COL_NAME							+ " text, " +
			DATA_EQUIPO_MODELO_COL_NAME							+ " text, " +
			DATA_EQUIPO_AÑO_COL_NAME							+ " text, " +
			DATA_EQUIPO_PLACA_PATENTE_COL_NAME					+ " text, " +
			DATA_EQUIPO_CODIGO_INTERNO_COL_NAME					+ " text, " +
			DATA_EQUIPO_COLOR_COL_NAME							+ " text, " +
			DATA_EQUIPO_NUMERO_MOTOR_COL_NAME					+ " text, " +
			DATA_EQUIPO_NUMERO_SERIE_COL_NAME					+ " text, " +
			DATA_EQUIPO_NUMERO_FACTURA_COL_NAME					+ " text, " +
			DATA_EQUIPO_NUMERO_GUIA_DESPACHO_COL_NAME			+ " text, " +
			DATA_EQUIPO_CAPACIDAD_COL_NAME						+ " text, " +
			DATA_EQUIPO_POTENCIA_COL_NAME						+ " text, " +
			DATA_EQUIPO_FECHA_CERTIFICACION_COL_NAME			+ " text " +
			");";
			
		static final String CENTER_PAUTA_DATA_CERTIFICACION_EQUIPO_TABLE_CREATE =
				"create table " + CENTER_PAUTA_DATA_CERTIFICACION_EQUIPO_TABLE +
				" ("+
				DATA_CERTIFICACION_EQUIPO_ID_CERTIFICACION_EQUIPO_COL_NAME		+ " integer primary key, " +
				DATA_CERTIFICACION_EQUIPO_ID_EQUIPO_CERTIFICAR_COL_NAME			+ " integer, " +
				DATA_CERTIFICACION_EQUIPO_ID_PAUTA_CERTIFICACION_COL_NAME		+ " integer, " +
				DATA_CERTIFICACION_EQUIPO_EMPRESA_RESPONSABLE_COL_NAME			+ " text, " +
				DATA_CERTIFICACION_EQUIPO_LUGAR_INSPECCION_COL_NAME				+ " text, " +
				DATA_CERTIFICACION_EQUIPO_NPPA_COL_NAME							+ " text, " +
				DATA_CERTIFICACION_EQUIPO_COTIZACION_COL_NAME					+ " text, " +
				DATA_CERTIFICACION_EQUIPO_ITEM_COL_NAME							+ " text, " +
				DATA_CERTIFICACION_EQUIPO_HOROMETRO_COL_NAME					+ " text, " +
				DATA_CERTIFICACION_EQUIPO_KILOMETRAJE_COL_NAME					+ " text, " +
				DATA_CERTIFICACION_EQUIPO_TIPO_CERTIFICACION_COL_NAME			+ " text, " +
				DATA_CERTIFICACION_EQUIPO_FECHA_ENTREGA_DOCUMENTACION_COL_NAME	+ " text, " +
				DATA_CERTIFICACION_EQUIPO_FECHA_APROBACION_COL_NAME				+ " text, " +
				DATA_CERTIFICACION_EQUIPO_OPERA_COL_NAME						+ " text, " +
				DATA_CERTIFICACION_EQUIPO_FECHA_VIGENCIA_COL_NAME				+ " text, " +
				DATA_CERTIFICACION_EQUIPO_TIPO_ACCION_COL_NAME					+ " text " +
				");";
		
		static final String CENTER_PAUTA_DATA_INSTANCIA_CERTIFICACION_EQUIPO_TABLE_CREATE = 
				"create table " + CENTER_PAUTA_DATA_INSTANCIA_CERTIFICACION_EQUIPO_TABLE + 
				" (" +
				DATA_PAUTA_INSTANCIA_CERT_EQUIPO_ID_INST_CERT_EQUIPO_COL_NAME			+ " integer primary key, " +
				DATA_PAUTA_INSTANCIA_CERT_EQUIPO_ID_CERT_EQUIPO_COL_NAME				+ " integer, " +
				DATA_PAUTA_INSTANCIA_CERT_EQUIPO_ID_CERTIFICADOR_COL_NAME				+ " integer, " + // Id Certificador = ID Usuario;
				DATA_PAUTA_INSTANCIA_CERT_EQUIPO_ID_PROGRAMACION_CERT_COL_NAME			+ " integer, " +
				DATA_PAUTA_INSTANCIA_CERT_EQUIPO_NUMERO_INST_CERTIFICACION_COL_NAME		+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EQUIPO_FECHA_INSTANCIA_COL_NAME				+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EQUIPO_LUGAR_COL_NAME							+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EQUIPO_SITUACION_APROBACION_COL_NAME			+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EQUIPO_OBSERVACION_SITUACION_EQUIPO_COL_NAME	+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EQUIPO_OBSERVACION_FINAL_COL_NAME				+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EQUIPO_RESULTADO_CERTIFICACION_COL_NAME		+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EQUIPO_LATITUD_COL_NAME						+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EQUIPO_LONGITUD_COL_NAME						+ " text " +
				");"; 
				
		static final String CENTER_PAUTA_DATA_EMPLEADO_TABLE_CREATE =
				"create table " + CENTER_PAUTA_DATA_EMPLEADO_TABLE +
				" ("+
				DATA_EMPLEADO_ID_COL_NAME					+ " integer primary key, " +
				DATA_EMPLEADO_RUN_COL_NAME 					+ " text, " +
				DATA_EMPLEADO_DIGITO_VERIFICADOR_COL_NAME	+ " text, " +
				DATA_EMPLEADO_TELEFONO_COL_NAME 			+ " text, " +
				DATA_EMPLEADO_NOMBRES_COL_NAME				+ " text, " +
				DATA_EMPLEADO_PRIMER_APELLIDO_COL_NAME		+ " text, " +
				DATA_EMPLEADO_SEGUNDO_APELLIDO_COL_NAME		+ " text, " +
				DATA_EMPLEADO_CELULAR_COL_NAME				+ " text, " +
				DATA_EMPLEADO_TIPO_ACCION_COL_NAME			+ " text " +
				");";
		
		static final String CENTER_PAUTA_DATA_INSTANCIA_CERTIFICACION_EMPLEADO_TABLE_CREATE = 
				"create table " + CENTER_PAUTA_DATA_INSTANCIA_CERTIFICACION_EMPLEADO_TABLE + 
				" (" +
				DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_ID_INST_CERT_EMPLEADO_COL_NAME			+ " integer primary key, " +
				DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_ID_CERT_EMPLEADO_COL_NAME				+ " integer, " +
				DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_ID_CERTIFICADOR_COL_NAME					+ " integer, " + // Id Certificador = ID Usuario;
				DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_ID_PROGRAMACION_CERT_COL_NAME			+ " integer, " +
				DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_NUMERO_INST_CERTIFICACION_COL_NAME		+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_FECHA_INSTANCIA_COL_NAME					+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_LUGAR_COL_NAME							+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_SITUACION_APROBACION_COL_NAME			+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_OBSERVACION_SITUACION_EQUIPO_COL_NAME	+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_OBSERVACION_FINAL_COL_NAME				+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_RESULTADO_CERTIFICACION_COL_NAME			+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_LATITUD_COL_NAME							+ " text, " +
				DATA_PAUTA_INSTANCIA_CERT_EMPLEADO_LONGITUD_COL_NAME						+ " text " +
				");"; 
		
//		center_pauta_data_certificacion_empleado
		
		static final String CENTER_PAUTA_DATA_CERTIFICACION_EMPLEADO_TABLE_CREATE =
				"create table " + CENTER_PAUTA_DATA_CERTIFICACION_EMPLEADO_TABLE +
				" ("+
				DATA_CERTIFICACION_EMPLEADO_ID_CERTIFICACION_EMPLEADO_COL_NAME		+ " integer primary key," +
				DATA_CERTIFICACION_EMPLEADO_ID_PAUTA_CERTIFICACION_COL_NAME			+ " integer, " +
				DATA_CERTIFICACION_EMPLEADO_ID_EMPLEADO_COL_NAME					+ " integer, " +
				DATA_CERTIFICACION_EMPLEADO_EQUIPO_ASOCIADO_COL_NAME				+ " text, " +
				DATA_CERTIFICACION_EMPLEADO_EQUIPO_ASOCIADO_TONELADA_COL_NAME		+ " text, " +
				DATA_CERTIFICACION_EMPLEADO_LUGAR_EVALUACION_COL_NAME				+ " text, " +
				DATA_CERTIFICACION_EMPLEADO_NUMERO_CONSTANCIA_COL_NAME				+ " text, " +
				DATA_CERTIFICACION_EMPLEADO_NUMERO_NIVEL_APROBACION_COL_NAME		+ " text, " +
				DATA_CERTIFICACION_EMPLEADO_FECHA_COL_NAME							+ " text, " +
				DATA_CERTIFICACION_EMPLEADO_FECHA_CERTIFICACION_COL_NAME			+ " text, " +
				DATA_CERTIFICACION_EMPLEADO_FECHA_VIGENCIA_COL_NAME					+ " text, " +
				DATA_CERTIFICACION_EMPLEADO_TIPO_ACCION_COL_NAME					+ " text " +
				");";
		
		
		@Override
		public void onCreate(SQLiteDatabase db) {
			
			Log.d(LOGTAG, CENTER_OTIC_DATA_TABLE_CREATE);
			db.execSQL(CENTER_OTIC_DATA_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_CENTRO_CERTIF_TABLE_CREATE);
			db.execSQL(CENTER_CENTRO_CERTIF_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_AGENDA_TABLE_CREATE);
			db.execSQL(CENTER_AGENDA_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_PERSONAS_TABLE_CREATE);
			db.execSQL(CENTER_PERSONAS_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_USUARIOS_TABLE_CREATE);
			db.execSQL(CENTER_USUARIOS_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_FECHA_SYNC_TABLE_CREATE);
			db.execSQL(CENTER_FECHA_SYNC_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_PAUTAS_TABLE_CREATE);
			db.execSQL(CENTER_PAUTAS_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_PAUTAS_GRUPO_TABLE_CREATE);
			db.execSQL(CENTER_PAUTAS_GRUPO_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_PAUTAS_ITEM_TABLE_CREATE);
			db.execSQL(CENTER_PAUTAS_ITEM_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_DOCUMENTO_OBLIGATORIO_EQUIPO_TABLE_CREATE);
			db.execSQL(CENTER_DOCUMENTO_OBLIGATORIO_EQUIPO_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_CERT_EXT_EQUIPO_TABLE_CREATE);
			db.execSQL(CENTER_CERT_EXT_EQUIPO_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_PAUTAS_DOC_OBLIGATORIOS_EMPLEADO_TABLE_CREATE);
			db.execSQL(CENTER_PAUTAS_DOC_OBLIGATORIOS_EMPLEADO_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_PAUTA_DATA_ITEM_PAUTA_TABLE_CREATE);
			db.execSQL(CENTER_PAUTA_DATA_ITEM_PAUTA_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_PAUTA_DATA_EQUIPO_TABLE_CREATE);
			db.execSQL(CENTER_PAUTA_DATA_EQUIPO_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_PAUTA_DATA_CERTIFICACION_EQUIPO_TABLE_CREATE);
			db.execSQL(CENTER_PAUTA_DATA_CERTIFICACION_EQUIPO_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_PAUTA_DATA_INSTANCIA_CERTIFICACION_EQUIPO_TABLE_CREATE);
			db.execSQL(CENTER_PAUTA_DATA_INSTANCIA_CERTIFICACION_EQUIPO_TABLE_CREATE);
			
			Log.d(LOGTAG, CENTER_PAUTA_DATA_EMPLEADO_TABLE_CREATE);
			db.execSQL(CENTER_PAUTA_DATA_EMPLEADO_TABLE_CREATE);

			Log.d(LOGTAG, CENTER_PAUTA_DATA_INSTANCIA_CERTIFICACION_EMPLEADO_TABLE_CREATE);
			db.execSQL(CENTER_PAUTA_DATA_INSTANCIA_CERTIFICACION_EMPLEADO_TABLE_CREATE);

			Log.d(LOGTAG, CENTER_PAUTA_DATA_CERTIFICACION_EMPLEADO_TABLE_CREATE);
			db.execSQL(CENTER_PAUTA_DATA_CERTIFICACION_EMPLEADO_TABLE_CREATE);
			
			
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			
			Log.d(LOGTAG, "Actualizando desde la versi�n " +
					oldVersion + " a " +
					newVersion + ", destruir� todos los datos antiguos.");
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_OTIC_DATA_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_CENTRO_CERTIFICADOR_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_AGENDA_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_PERSONAS_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_USUARIOS_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_PAUTAS_CERTIFICACION_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_PAUTAS_GRUPO_CERTIFICACION_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_PAUTAS_ITEM_CERTIFICACION_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_PAUTAS_DOC_OBLIGATORIOS_EQUIPO_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_PAUTAS_DOC_OBLIGATORIOS_EMPLEADO_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_PAUTAS_CERT_EXTERNAS_EQUIPO_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_FECHAS_SINCRONIZACION_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_PAUTA_DATA_ITEM_PAUTA_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_PAUTA_DATA_EQUIPO_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_PAUTA_DATA_INSTANCIA_CERTIFICACION_EMPLEADO_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + CENTER_PAUTA_DATA_CERTIFICACION_EMPLEADO_TABLE);
			
			
			onCreate(db);
			
		}
	} // Final de la clase  centroInfoDBOpenHelper.
	
	public DatabaseHandler(Context context){
		mDbHelper = new centroInfoDBOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	// abrir db para escribir en ella, si no es posible, s�lo leer la db.
		public void open() throws SQLiteException {
			try {
				mDb = mDbHelper.getWritableDatabase();
				Log.d(LOGTAG, "WRITEABLE DB CREATED");
			}
			catch ( SQLiteException ex ) {
				Log.d(LOGTAG, "READABLE DB CREATED");
				mDb = mDbHelper.getReadableDatabase();
			}
		}
		
		public void close() {
			mDb.close();
		}

	/**
	 * (Crear, Leer, Actualizar, Borrar) Operaciones.
	 */
		
		// Insertar datos Otic
		public long insertOticData(OticData oticData) {
			
			ContentValues newOticData = new ContentValues();
			newOticData.put(OTICDATA_ID_COL_NAME, oticData.getmID());
			newOticData.put(OTICDATA_IMG_COL_NAME, oticData.getmImagenLogoOTIC());
			newOticData.put(OTICDATA_NOMBRE_OTIC_COL_NAME, oticData.getmNombreOtic());
			newOticData.put(OTICDATA_DESC_COL_NAME, oticData.getmDescripcionOTIC());
			//long insertedRowIndex = mDb.insert(BOOK_TABLE, null, newBook);
			Cursor rslt = mDb.query(CENTER_OTIC_DATA_TABLE, 
					new String[] { OTICDATA_ID_COL_NAME }, 
					OTICDATA_ID_COL_NAME + "=" + oticData.getmID(), 
					null, null, null, null);
			long insertedRowIndex = -1;
			if ((rslt.getCount() == 0 || !rslt.moveToFirst()) ) {
				insertedRowIndex =  mDb.insertWithOnConflict(CENTER_OTIC_DATA_TABLE, 
						null, 
						newOticData, 
						SQLiteDatabase.CONFLICT_REPLACE);	
			}		
			
			// close the cursor and make it completely invalid
			rslt.close();
			Log.d(LOGTAG, "Inserted OticData " + insertedRowIndex);
			return insertedRowIndex;
		}
		
		//Insertar datos Centro Certificador.

		public long insertCertificadorData(Centro_Certificador certificador) {
			
			ContentValues newBook = new ContentValues();
			newBook.put(CERTIFICADOR_ID_CENTRO_CERT_COL_NAME, certificador.getmIdCentroCertificador());
			newBook.put(CERTIFICADOR_ID_COL_NAME, certificador.getM$ID());
			newBook.put(CERTIFICADOR_IMG_COL_NAME, certificador.getmImgLogo());
			newBook.put(CERTIFICADOR_NOMBRE_COL_NAME, certificador.getmNombreCertificador());
			//long insertedRowIndex = mDb.insert(BOOK_TABLE, null, newBook);
			Cursor rslt = mDb.query(CENTER_CENTRO_CERTIFICADOR_TABLE, 
					new String[] { CERTIFICADOR_ID_CENTRO_CERT_COL_NAME }, 
					CERTIFICADOR_ID_CENTRO_CERT_COL_NAME + "=" + certificador.getmIdCentroCertificador(), 
					null, null, null, null);
			long insertedRowIndex = -1;
			if ((rslt.getCount() == 0 || !rslt.moveToFirst()) ) {
				insertedRowIndex =  mDb.insertWithOnConflict(	CENTER_CENTRO_CERTIFICADOR_TABLE, 
																null, 
																newBook, 
																SQLiteDatabase.CONFLICT_REPLACE);	
			}		
			
			// close the cursor and make it completely invalid
			rslt.close();
			Log.d(LOGTAG, "Inserted Centro Certificador " + insertedRowIndex +", " + certificador.getmNombreCertificador());
			return insertedRowIndex;
			
			
		}
		
		// Insertar datos Usuario.
		public long insertUsuarios(Usuario usuario){
			
			ContentValues newUsuarios = new ContentValues();
			newUsuarios.put(USUARIO_ID_USUARIO_COL_NAME, usuario.getmIdUsuario());
			newUsuarios.put(USUARIO_ID_COL_NAME, usuario.get$ID());
			newUsuarios.put(USUARIO_SALT_COL_NAME, usuario.getmSalt());
			newUsuarios.put(USUARIO_NOMBRE_USUARIO_COL_NAME, usuario.getmNombreUsuario());
			newUsuarios.put(USUARIO_PASS_COL_NAME, usuario.getmPassword());
		
			Cursor rslt = mDb.query(CENTER_USUARIOS_TABLE, 
					new String[]{USUARIO_ID_USUARIO_COL_NAME}, 
					USUARIO_ID_USUARIO_COL_NAME + "=" + usuario.getmIdUsuario(), 
					null, null, null, null);
			long insertedRowIndex = -1;
			if((rslt.getCount() == 0) || !rslt.moveToFirst()){
				insertedRowIndex = mDb.insertWithOnConflict(CENTER_USUARIOS_TABLE, 
														    null, 
														    newUsuarios, 
														    SQLiteDatabase.CONFLICT_REPLACE);
			}
			
			rslt.close();
			if(insertedRowIndex == -1){
				
				Log.d(LOGTAG, "Usuario ya existente, " + usuario.getmNombreUsuario());
				
			}else{
				Log.d(LOGTAG, "Inserted Usuario " + insertedRowIndex + ", " + usuario.getmNombreUsuario());
			}
			
			return 0;
			
		}
		
		//Insertar datos Persona
		public long insertPersona(Persona persona){
			
			/*
			 * 	public static final String PERSONA_ID_PERSONA_COL_NAME			= "ID_PERSONA";
				public static final String PERSONA_ID_CENTRO_CERT_COL_NAME 		= "ID_CENTRO_CERT";
				public static final String PERSONA_ID_USUARIO_COL_NAME			= "ID_USUARIO";
				public static final String PERSONA_TIPOACCION_COL_NAME			= "TipoAccion";
				public static final String PERSONA_ID_COL_NAME					= "ID";
				public static final String PERSONA_RUN_COL_NAME					= "RUN";
				public static final String PERSONA_DIGITO_VERIF_COL_NAME 		= "DigitoVerificador";
				public static final String PERSONA_NOMBRES_COL_NAME				= "Nombres";
				public static final String PERSONA_PRIMER_APELLIDO_COL_NAME		= "PrimerApellido";
				public static final String PERSONA_SEGUNDO_APELLIDO_COL_NAME	= "SegundoApellido";
				public static final String PERSONA_TIPO_ACCION_COL_NAME			= "TipoAccion";
				public static final String PERSONA_TELEFONO_COL_NAME 			= "Telefono";
				public static final String PERSONA_CELULAR_COL_NAME				= "Celular";
			 */
			
			ContentValues newPersona = new ContentValues();
			newPersona.put(PERSONA_ID_PERSONA_COL_NAME, persona.getIdPersona());
			newPersona.put(PERSONA_ID_CENTRO_CERT_COL_NAME, persona.getIdCentroCertificador());
			newPersona.put(PERSONA_ID_USUARIO_COL_NAME, persona.getIdUsuario());
			newPersona.put(PERSONA_TIPO_ACCION_COL_NAME, persona.getTipoAccion());
			newPersona.put(PERSONA_ID_COL_NAME, persona.get$id());
			newPersona.put(PERSONA_RUN_COL_NAME, persona.getRUN());
			newPersona.put(PERSONA_DIGITO_VERIF_COL_NAME, persona.getDigitoVerificador());
			newPersona.put(PERSONA_NOMBRES_COL_NAME, persona.getNombres());
			newPersona.put(PERSONA_PRIMER_APELLIDO_COL_NAME, persona.getPrimerApellido());
			newPersona.put(PERSONA_SEGUNDO_APELLIDO_COL_NAME, persona.getSegundoApellido());
			newPersona.put(PERSONA_TELEFONO_COL_NAME, persona.getTelefono());
			newPersona.put(PERSONA_CELULAR_COL_NAME, persona.getCelular());
		
			Cursor rslt = mDb.query(CENTER_PERSONAS_TABLE, 
					new String[]{PERSONA_ID_PERSONA_COL_NAME}, 
					PERSONA_ID_PERSONA_COL_NAME + "=" + persona.getIdPersona(), 
					null, null, null, null);
			long insertedRowIndex = -1;
//			if((rslt.getCount() == 0) || !rslt.moveToFirst()){
				insertedRowIndex = mDb.insertWithOnConflict(CENTER_PERSONAS_TABLE, 
														    null, 
														    newPersona, 
														    SQLiteDatabase.CONFLICT_REPLACE);
//			}
			
			rslt.close();
			
//			if(insertedRowIndex == -1){
//				Log.d(LOGTAG, "No se ha ingresado el Usuario.");
//			}else{
				Log.d(LOGTAG, "Agregado Usuario " + insertedRowIndex + ", " + persona.getNombres());
//			}
			
			return insertedRowIndex;
			
		}
		
		public Persona getPersona(String user){
			
				Cursor rslt1 = 
						mDb.query(CENTER_USUARIOS_TABLE, 
								null, 
								USUARIO_NOMBRE_USUARIO_COL_NAME + " = '" + user + "'", 
								null, 
								null, 
								null, 
								null);
				
				Persona persona = new Persona();
				
				if ( rslt1.getCount() != 0 ) {
					rslt1.moveToFirst();
					
					int idUsuario = rslt1.getInt(rslt1.getColumnIndex(USUARIO_ID_USUARIO_COL_NAME));
					
					try {
						
						Cursor rslt2 = 
								mDb.query(CENTER_PERSONAS_TABLE, 
										null, 
										PERSONA_ID_USUARIO_COL_NAME + " = " + idUsuario , 
										null, 
										null, 
										null, 
										null);
						if ( rslt2.getCount() != 0 ) {
							rslt2.moveToFirst();
							
								/*
							  	public static final String PERSONA_ID_PERSONA_COL_NAME			= "ID_PERSONA";
								public static final String PERSONA_ID_CENTRO_CERT_COL_NAME 		= "ID_CENTRO_CERT";
								public static final String PERSONA_ID_USUARIO_COL_NAME			= "ID_USUARIO";
								public static final String PERSONA_TIPO_ACCION_COL_NAME			= "TipoAccion";
								public static final String PERSONA_ID_COL_NAME					= "ID";
								public static final String PERSONA_RUN_COL_NAME					= "RUN";
								public static final String PERSONA_DIGITO_VERIF_COL_NAME 		= "DigitoVerificador";
								public static final String PERSONA_NOMBRES_COL_NAME				= "Nombres";
								public static final String PERSONA_PRIMER_APELLIDO_COL_NAME		= "PrimerApellido";
								public static final String PERSONA_SEGUNDO_APELLIDO_COL_NAME	= "SegundoApellido";
								public static final String PERSONA_TELEFONO_COL_NAME 			= "Telefono";
								public static final String PERSONA_CELULAR_COL_NAME				= "Celular";
								
					public Persona(int idPersona, int idCentroCertificador, int idUsuario, int tipoAccion, int _$id, int run,
								   String digitoVerificador, String nombres, String primerApellido, String segundoApellido,
								   String telefono, String celular)
							 */
								
								persona.setIdPersona(rslt2.getInt(rslt2.getColumnIndex(PERSONA_ID_PERSONA_COL_NAME)));
								persona.setIdCentroCertificador(rslt2.getInt(rslt2.getColumnIndex(PERSONA_ID_CENTRO_CERT_COL_NAME)));
								persona.setIdUsuario(rslt2.getInt(rslt2.getColumnIndex(PERSONA_ID_USUARIO_COL_NAME)));
								persona.setTipoAccion(rslt2.getInt(rslt2.getColumnIndex(PERSONA_TIPO_ACCION_COL_NAME)));
								persona.set$id(rslt2.getInt(rslt2.getColumnIndex(PERSONA_ID_COL_NAME)));
								persona.setRUN(rslt2.getInt(rslt2.getColumnIndex(PERSONA_RUN_COL_NAME)));
								persona.setDigitoVerificador(rslt2.getString(rslt2.getColumnIndex(PERSONA_DIGITO_VERIF_COL_NAME)));
								persona.setNombres(rslt2.getString(rslt2.getColumnIndex(PERSONA_NOMBRES_COL_NAME)));
								persona.setPrimerApellido(rslt2.getString(rslt2.getColumnIndex(PERSONA_PRIMER_APELLIDO_COL_NAME)));
								persona.setSegundoApellido(rslt2.getString(rslt2.getColumnIndex(PERSONA_SEGUNDO_APELLIDO_COL_NAME)));
								persona.setTelefono(rslt2.getString(rslt2.getColumnIndex(PERSONA_TELEFONO_COL_NAME)));
								persona.setCelular(rslt2.getString(rslt2.getColumnIndex(PERSONA_CELULAR_COL_NAME)));
								
						}else{
							
							Log.e(LOGTAG, "El usuario no corresponde");
							
						}
						
						rslt2.close();
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				rslt1.close();
				
				return persona;
			
			
			
		}
		
		//Borrar Usuario
		public void deleteUsuario(Usuario usuario) {
		    long id = usuario.getmIdUsuario();
		    System.out.println("Borrando usuario con id: " + id);
		    mDb.delete(CENTER_USUARIOS_TABLE, USUARIO_ID_USUARIO_COL_NAME
		        + " = " + id, null);
		    
		    Log.d(LOGTAG, "Se ha eliminado el Usuario: " + usuario.getmNombreUsuario() + ", idUsuario" + id);
		}
		
		//Borrar Persona
		public void deletePersona(Persona persona) {
			
		    long id = persona.getIdPersona();
		    System.out.println("Borrando usuario con id: " + id);
		    mDb.delete(CENTER_PERSONAS_TABLE, PERSONA_ID_PERSONA_COL_NAME
		        + " = " + id, null);
		    
		    Log.d(LOGTAG, "Se ha eliminado: " + persona.getNombres() + ", idPersona: " + id);
		  }
		
		@SuppressLint("SimpleDateFormat")
		public long insertFecha(String tipoSync){
			
			int _id = 0;
			ContentValues newFecha = new ContentValues();
			
			Calendar c = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			String strDate = sdf.format(c.getTime());
			
			if(tipoSync.equals("Usuario")){
				
				_id = FECHA_ID_SYNC_USUARIOS;
				
			}
			
			newFecha.put(FECHA_ID_COL_NAME, _id);
			newFecha.put(FECHA_DATE_COL_NAME, strDate);
			newFecha.put(FECHA_TIPO_SINC_COL_NAME, tipoSync);
			
			Cursor rslt =
					mDb.query(CENTER_FECHAS_SINCRONIZACION_TABLE, 
							new String[]{FECHA_TIPO_SINC_COL_NAME}, 
							FECHA_TIPO_SINC_COL_NAME + " = '" + tipoSync + "'", 
							null, 
							null, 
							null, 
							null);
			
			long insertedRowIndex = -1;
				insertedRowIndex = mDb.insertWithOnConflict(CENTER_FECHAS_SINCRONIZACION_TABLE, 
					    null, 
					    newFecha, 
					    SQLiteDatabase.CONFLICT_REPLACE);
				
			rslt.close();	
				Log.d(LOGTAG, "Fecha de sincronizacion ingresada para: " + tipoSync + ", Fecha: " + strDate);
			return insertedRowIndex;
			
		}
		
		//Obtiene la fecha, si existe devuelve el valor.
		public String getFecha(String tipoSync){
			
			//int _id = 0;
			String _fecha = null;
			Cursor rslt =
					mDb.query(CENTER_FECHAS_SINCRONIZACION_TABLE, 
							new String[]{FECHA_TIPO_SINC_COL_NAME, FECHA_DATE_COL_NAME, FECHA_ID_COL_NAME}, 
							FECHA_TIPO_SINC_COL_NAME + " = '" + tipoSync + "'", 
							null, 
							null, 
							null, 
							null);
			
			if ( rslt.getCount() != 0 ) {
				rslt.moveToFirst();
					_fecha = rslt.getString(rslt.getColumnIndex(FECHA_DATE_COL_NAME));
					
			}
			return _fecha;

			
		}
		
		//Obtener el Logo del Centro Certificador.
		public Bitmap getImgLogo() {
			
			byte[] image_bytes = null;
			Cursor rslt = 
				mDb.query(CENTER_CENTRO_CERTIFICADOR_TABLE, 
						new String[]{CERTIFICADOR_IMG_COL_NAME}, 
						null, 
						null, 
						null, 
						null, 
						null);
			if ( rslt.getCount() != 0 ) {
				rslt.moveToFirst();
				
				image_bytes = rslt.getBlob(rslt.getColumnIndex(CERTIFICADOR_IMG_COL_NAME));
				Bitmap bmp = BitmapFactory.decodeByteArray(image_bytes, 0, image_bytes.length);
				return bmp;
			}
			else {
				return null;
			}
		}
		
		public byte[] getImgLogoByte() {
			
			byte[] image_bytes = null;
			Cursor rslt = 
				mDb.query(CENTER_CENTRO_CERTIFICADOR_TABLE, 
						new String[]{CERTIFICADOR_IMG_COL_NAME}, 
						null, 
						null, 
						null, 
						null, 
						null);
			if ( rslt.getCount() != 0 ) {
				rslt.moveToFirst();
				
				image_bytes = rslt.getBlob(rslt.getColumnIndex(CERTIFICADOR_IMG_COL_NAME));
				Bitmap bmp = BitmapFactory.decodeByteArray(image_bytes, 0, image_bytes.length);
				return image_bytes;
			}
			else {
				return null;
			}
		}
		
		//Query Sqlite
		public String sample_query_01() {
			Cursor rslt =
				mDb.query(CENTER_AGENDA_TABLE, // table name 
						null, 				// column names 
						null, 			    // where clause
						null, 				// selection args
						null, 			    // group by
						null, 				// having
						null);				// order by
			
			StringBuffer buffer = new StringBuffer("");
			
			if ( rslt.getCount() != 0 ) {
				rslt.moveToFirst();
				while ( rslt.isAfterLast() == false ) {
					buffer.append("QUERY 01\n\n");
					
					/*
					 * 
						public static final String AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NAME 	=	"IdProgramacionCertificacion";
						public static final String AGENDA_ID_PAUTA_CERTIFICACION_COL_NAME			=	"IdPautaCertificacion";
						public static final String AGENDA_ID_CERTIFICADOR_COL_NAME					=	"IdCertificador";
						public static final String AGENDA_TIPO_CERTIFICACION_COL_NAME				=	"TipoCertificacion";
						public static final String AGENDA_FECHA_REQUERIMIENTO_COL_NAME				=	"FechaRequerimento";
						public static final String AGENDA_FECHA_PROGRAMACION_COL_NAME				=	"FechaProgramacion";
						public static final String AGENDA_UBICACION_COL_NAME						=	"Ubicacion";
						public static final String AGENDA_CONTACTO_COL_NAME							=	"Contacto";
						public static final String AGENDA_TIPO_ACCION_COL_NAME						=	"TipoAccion";
					 * 
					 */
					
					int id = rslt.getInt(rslt.getColumnIndex(AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NAME));
					int idPautaCert= rslt.getInt(rslt.getColumnIndex(AGENDA_ID_PAUTA_CERTIFICACION_COL_NAME));
					int idCert= rslt.getInt(rslt.getColumnIndex(AGENDA_ID_PAUTA_CERTIFICACION_COL_NAME));
//					byte[] imgOtic = rslt.getBlob(rslt.getColumnIndex(OTICDATA_IMG_COL_NAME));
//					String descOtic = rslt.getString(rslt.getColumnIndex(OTICDATA_DESC_COL_NAME));
//					
					buffer.append(AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NAME + id + "\n");
					buffer.append(AGENDA_ID_PAUTA_CERTIFICACION_COL_NAME + idPautaCert + "\n");
					buffer.append(AGENDA_ID_CERTIFICADOR_COL_NAME + idCert + "\n");
					buffer.append("=====================\n");
					rslt.moveToNext();
				}
			}else{
				Log.i(LOGTAG, "No se han encontrado Datos ...");
			}
			
			rslt.close();
			
			return buffer.toString();
		}

		public Boolean validarUsuario(String user, String pass) {
			
			boolean resultado = false;
			
			Cursor rslt = 
					mDb.query(CENTER_USUARIOS_TABLE, 
							null, 
							USUARIO_NOMBRE_USUARIO_COL_NAME + " = '" + user + "'", 
							null, 
							null, 
							null, 
							null);
			
			if ( rslt.getCount() != 0 ) {
				rslt.moveToFirst();
				
				String nombre = rslt.getString(rslt.getColumnIndex(USUARIO_NOMBRE_USUARIO_COL_NAME));
				String pass_ = rslt.getString(rslt.getColumnIndex(USUARIO_PASS_COL_NAME));
				String salt_ = rslt.getString(rslt.getColumnIndex(USUARIO_SALT_COL_NAME));
				
				System.out.println("Nombre: " + nombre);
				System.out.println("Password ingresada: " + pass);
				System.out.println("Password encode: " + pass_);
				System.out.println("Salt: " + salt_);
				
				try {
					
					String password = passToSha1(pass, salt_);
					System.out.println("Password con sha1: " + password);
					
					Cursor rslt2 = 
							mDb.query(CENTER_USUARIOS_TABLE, 
									null, 
									USUARIO_NOMBRE_USUARIO_COL_NAME + " = '" + user + "' AND " + 
									USUARIO_PASS_COL_NAME + " = '" + password + "'", 
									null, 
									null, 
									null, 
									null);
					if ( rslt2.getCount() != 0 ) {
						rslt2.moveToFirst();
						
						resultado = true;
						Log.i(LOGTAG, "El usuario es correcto");
						
					}else{
						
						resultado = false;
						Log.e(LOGTAG, "El usuario no corresponde");
						
					}
					
					rslt2.close();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			rslt.close();
			return resultado;
			
		}
		
		public long insertPautasCertificaciones(PautasCertificacion pautasCertificacion){
			
			/*
			    public static final String PAUTAS_CERT_ID_PAUTA_COL_NAME					= "ID_PAUTA_CERTIFICACION";
				public static final String PAUTAS_CERT_ID_CENTRO_CERT_COL_NAME				= "ID_CENTRO_CERTIFICADOR";
				public static final String PAUTAS_NOMBRE_PAUTA_CERT_COL_NAME				= "Nombre";
				public static final String PAUTAS_DESCRIPCION_COL_NAME						= "Descripcion";
				public static final String PAUTAS_TIPOCERTIFICACION_COL_NAME				= "TipoCertificacion";
				public static final String PAUTAS_CANTIDAD_INSTANCIAS_COL_NAME				= "CantidadInstancias";
				public static final String PAUTAS_INCLUYE_OBS_PORPREGUNTA_BOOLEAN_COL_NAME	= "ObservacionPorPreguntaBoolean";
				public static final String PAUTAS_INCLUYE_NO_APLICA_BOOLEAN_COL_NAME		= "IncluyeNoAplicaBoolean";
				public static final String PAUTAS_REVISION_COL_NAME							= "Revision";
				public static final String PAUTAS_IMAGEN_LOGO_CERTIFICACION_COL_NAME		= "ImgLogoCertificacion";
				public static final String PAUTAS_VENCIMIENTO_KM_HM_BOOLEAN_COL_NAME		= "VencimientoKM_HM_Boolean";
				public static final String PAUTAS_TIPO_ACCION_COL_NAME						= "TipoAccion";
			 */
			
			ContentValues newPautaCertificacion = new ContentValues();
			newPautaCertificacion.put(PAUTAS_CERT_ID_PAUTA_COL_NAME, 	   				pautasCertificacion.getIdPautaCertificacion());
			newPautaCertificacion.put(PAUTAS_CERT_ID_CENTRO_CERT_COL_NAME, 				pautasCertificacion.getIdCentroCertificador());
			newPautaCertificacion.put(PAUTAS_NOMBRE_PAUTA_CERT_COL_NAME,   				pautasCertificacion.getNombre());
			newPautaCertificacion.put(PAUTAS_DESCRIPCION_COL_NAME, 		 			    pautasCertificacion.getDescripcion());
			newPautaCertificacion.put(PAUTAS_TIPOCERTIFICACION_COL_NAME,   				pautasCertificacion.getTipoCertificacion());
			newPautaCertificacion.put(PAUTAS_CANTIDAD_INSTANCIAS_COL_NAME, 				pautasCertificacion.getCantidadInstancias());
			newPautaCertificacion.put(PAUTAS_INCLUYE_OBS_PORPREGUNTA_BOOLEAN_COL_NAME, 	pautasCertificacion.isIncluyeObservacionPorPregunta());
			newPautaCertificacion.put(PAUTAS_INCLUYE_NO_APLICA_BOOLEAN_COL_NAME, 		pautasCertificacion.isIncluyeNoAplica());
			newPautaCertificacion.put(PAUTAS_REVISION_COL_NAME, 						pautasCertificacion.getRevision());
			newPautaCertificacion.put(PAUTAS_IMAGEN_LOGO_CERTIFICACION_COL_NAME, 		pautasCertificacion.getImagenLogoCertificacion());
			newPautaCertificacion.put(PAUTAS_VENCIMIENTO_KM_HM_BOOLEAN_COL_NAME, 		pautasCertificacion.getVencimientoKmHm());
			newPautaCertificacion.put(PAUTAS_TIPO_ACCION_COL_NAME, 						pautasCertificacion.getTipoAccion());
			newPautaCertificacion.put(PAUTAS_INCLUYE__NUMERO_NIVEL_APROBACION_COL_NAME, pautasCertificacion.isIncluyeNumeroNivelAprobacion());
		
			Cursor rslt = mDb.query(CENTER_PAUTAS_CERTIFICACION_TABLE, 
					new String[]{PAUTAS_CERT_ID_PAUTA_COL_NAME}, 
					PAUTAS_CERT_ID_PAUTA_COL_NAME + "=" + pautasCertificacion.getIdPautaCertificacion(), 
					null, null, null, null);
			long insertedRowIndex = -1;
				insertedRowIndex = mDb.insertWithOnConflict(CENTER_PAUTAS_CERTIFICACION_TABLE, 
														    null, 
														    newPautaCertificacion, 
														    SQLiteDatabase.CONFLICT_REPLACE);
			
			rslt.close();
			
			/*
				public static final String DOC_EMPLEADO_ID_TIPO_DOC_OBL_EQUIPO_COL_NAME		= "IdTipoDocumentoObligatorioEquipo";
				public static final String DOC_EMPLEADO_ID_PAUTA_CERT_COL_NAME				= "IdPautaCertificacion";
				public static final String DOC_EMPLEADO_NOMBRE_COL_NAME						= "Nombre";
				public static final String DOC_EMPLEADO_ORDEN_COL_NAME						= "Orden";
				public static final String DOC_EMPLEADO_INCLUYE_FECHA_VENCIMIENTO_COL_NAME	= "IncluyeFechaVencimiento";
				public static final String DOC_EMPLEADO_TIPO_ACCION_COL_NAME				= "TipoAccion";
			 */
			
			List<GrupoPauta> grupoPautas = pautasCertificacion.getGruposPauta();
			
//			Log.d(LOGTAG + " GrupoPauta", "Tama�o Lista GrupoPauta a INGRESAR: " + grupoPautas.size());
			
			for(GrupoPauta gp : grupoPautas){
				/*
				 *	public static final String GRUPO_ID_GRUPOPAUTA_COL_NAME	= "ID_GRUPOPAUTA";
					public static final String GRUPO_ID_PAUTA_CERT_COL_NAME	= "ID_PAUTA_CERT";
					public static final String GRUPO_NOMBRE_COL_NAME		= "Nombre";
					public static final String GRUPO_ORDEN_COL_NAME			= "Orden";
					public static final String GRUPO_TIPOACCION_COL_NAME	= "TipoAccion";
				 */
				
				ContentValues newGrupoPauta = new ContentValues();
				newGrupoPauta.put(GRUPO_ID_GRUPOPAUTA_COL_NAME, gp.getIdGrupoPauta());
				newGrupoPauta.put(GRUPO_ID_PAUTA_CERT_COL_NAME, gp.getIdPautaCertificacion());
				newGrupoPauta.put(GRUPO_NOMBRE_COL_NAME, gp.getNombre());
				newGrupoPauta.put(GRUPO_ORDEN_COL_NAME, gp.getOrden());
				newGrupoPauta.put(GRUPO_TIPOACCION_COL_NAME, gp.getTipoAccion());
				
				Cursor cursor_GrupoPauta = mDb.query(CENTER_PAUTAS_GRUPO_CERTIFICACION_TABLE, 
						new String[]{GRUPO_ID_GRUPOPAUTA_COL_NAME}, 
						GRUPO_ID_GRUPOPAUTA_COL_NAME + " = " + gp.getIdGrupoPauta(), 
						null, null, null, null);
				
				long insertedGrupoPauta = -1;
						insertedGrupoPauta = mDb.insertWithOnConflict(CENTER_PAUTAS_GRUPO_CERTIFICACION_TABLE, 
															    null, 
															    newGrupoPauta, 
															    SQLiteDatabase.CONFLICT_REPLACE);
				
					cursor_GrupoPauta.close();
				
					Log.d(LOGTAG, "GrupoPauta " + insertedGrupoPauta + ", " + gp.getNombre());
			}
			
			List<ItemPauta> itemPautasList = pautasCertificacion.getItemsPauta();
			
			Log.d(LOGTAG + " ItemPauta", "Tama�o Lista ItemPauta a INGRESAR: " + grupoPautas.size());
			
			for(ItemPauta ip : itemPautasList){
				/*
				 *	
						public static final String ITEM_ID_PAUTA_COL_NAME		= "ID_ITEM_PAUTA";	
						public static final String ITEM_ID_GRUPO_COL_NAME		= "ID_GRUPO_PAUTA";
						public static final String ITEM_ID_PAUTA_CERT_COL_NAME	= "ID_PAUTA_CERT";
						public static final String ITEM_NOMBRE_COL_NAME			= "Nombre";
						public static final String ITEM_ORDEN_COL_NAME			= "Orden";
						public static final String ITEM_TIPO_ACCION_COL_NAME	= "TipoAccion";
				 */
				
				ContentValues newItemPauta = new ContentValues();
				newItemPauta.put(ITEM_ID_PAUTA_COL_NAME, ip.getIdItemPauta());
				newItemPauta.put(ITEM_ID_GRUPO_COL_NAME, ip.getIdGrupoPauta());
				newItemPauta.put(ITEM_ID_PAUTA_CERT_COL_NAME, ip.getIdPautaCertificacion());
				newItemPauta.put(ITEM_NOMBRE_COL_NAME, ip.getNombre());
				newItemPauta.put(ITEM_ORDEN_COL_NAME, ip.getOrden());
				newItemPauta.put(ITEM_TIPO_ACCION_COL_NAME, ip.getTipoAccion());
				
				Cursor cursor_GrupoPauta = mDb.query(CENTER_PAUTAS_ITEM_CERTIFICACION_TABLE, 
						new String[]{ITEM_ID_PAUTA_COL_NAME}, 
						ITEM_ID_PAUTA_COL_NAME + " = " + ip.getIdItemPauta(), 
						null, null, null, null);
				
				long insertedItemPauta = -1;
				insertedItemPauta = mDb.insertWithOnConflict(CENTER_PAUTAS_ITEM_CERTIFICACION_TABLE, 
															    null, 
															    newItemPauta, 
															    SQLiteDatabase.CONFLICT_REPLACE);
				
					cursor_GrupoPauta.close();
				
					Log.d(LOGTAG, "ItemPauta " + insertedItemPauta + ", " + ip.getNombre());
			}
		
			List<TipoDocumentoObligatorioEmpleado> documentoObligatorioEmpleados = pautasCertificacion.getDocumentosObligatorioEmpleado();
			
//			Log.d(LOGTAG + " TipoDocumentoObligatorioEmpleado", "Tama�o Lista TipoDocumentoObligatorioEmpleado a INGRESAR: " + documentoObligatorioEmpleados.size());
			
			for(TipoDocumentoObligatorioEmpleado td : documentoObligatorioEmpleados){
				
				ContentValues newItemDocOblEmpleado = new ContentValues();
				newItemDocOblEmpleado.put(DOC_EMPLEADO_ID_TIPO_DOC_OBL_EMPLEADO_COL_NAME, td.getIdTipoDocumentoObligatorioEmpleado());
				newItemDocOblEmpleado.put(DOC_EMPLEADO_ID_PAUTA_CERT_COL_NAME, td.getIdPautaCertificacion());
				newItemDocOblEmpleado.put(DOC_EMPLEADO_NOMBRE_COL_NAME, td.getNombre());
				newItemDocOblEmpleado.put(DOC_EMPLEADO_ORDEN_COL_NAME, td.getOrden());
				newItemDocOblEmpleado.put(DOC_EMPLEADO_INCLUYE_FECHA_VENCIMIENTO_COL_NAME, td.isIncluyeFechaVencimiento());
				newItemDocOblEmpleado.put(DOC_EMPLEADO_TIPO_ACCION_COL_NAME, td.getTipoAccion());
				
				Cursor cursor_documentoObligatorioEmpleados = mDb.query(CENTER_PAUTAS_DOC_OBLIGATORIOS_EMPLEADO_TABLE, 
						new String[]{DOC_EMPLEADO_ID_TIPO_DOC_OBL_EMPLEADO_COL_NAME}, 
						DOC_EMPLEADO_ID_TIPO_DOC_OBL_EMPLEADO_COL_NAME + " = " + td.getIdTipoDocumentoObligatorioEmpleado(), 
						null, null, null, null);
				
				long insertedDocumentoObligatorioEmpleados = -1;
						insertedDocumentoObligatorioEmpleados = mDb.insertWithOnConflict(CENTER_PAUTAS_DOC_OBLIGATORIOS_EMPLEADO_TABLE, 
															    null, 
															    newItemDocOblEmpleado, 
															    SQLiteDatabase.CONFLICT_REPLACE);
				
					cursor_documentoObligatorioEmpleados.close();
					
					
				
					Log.d(LOGTAG, "DocumentosObligatorioEmpleado " + insertedDocumentoObligatorioEmpleados + ", " + td.getNombre());
			}
			
			List<TipoDocumentoObligatorioEquipo> documentoObligatorioEquipos = pautasCertificacion.getDocumentosObligatorioEquipo();
			
//			Log.d(LOGTAG + " TipoDocumentoObligatorioEquipos", "Tama�o Lista TipoDocumentoObligatorioEquipos a INGRESAR: " + documentoObligatorioEquipos.size());
			
			for(TipoDocumentoObligatorioEquipo tde : documentoObligatorioEquipos){
				
				/*
					public static final String DOC_EQUIPO_ID_TIPO_DOC_OBL_EQUIPO_COL_NAME		= "IdTipoDocumentoObligatorioEquipo";
					public static final String DOC_EQUIPO_ID_PAUTA_CERT_COL_NAME				= "IdPautaCertificacion";
					public static final String DOC_EQUIPO_NOMBRE_COL_NAME						= "Nombre";
					public static final String DOC_EQUIPO_ORDEN_COL_NAME						= "Orden";
					public static final String DOC_EQUIPO_INCLUYE_FECHA_VENCIMIENTO_COL_NAME	= "IncluyeFechaVencimiento";
					public static final String DOC_EQUIPO_TIPO_ACCION_COL_NAME					= "TipoAccion";
				 */
				
				ContentValues newItemDocOblEquipo = new ContentValues();
				newItemDocOblEquipo.put(DOC_EQUIPO_ID_TIPO_DOC_OBL_EQUIPO_COL_NAME, tde.getIdTipoDocumentoObligatorioEquipo());
				
//				Log.d(LOGTAG, "DocumentosObligatorioEquipos Id Pauta certificacion: "  + tde.getIdPautaCertificacion());
				
				newItemDocOblEquipo.put(DOC_EQUIPO_ID_PAUTA_CERT_COL_NAME, tde.getIdPautaCertificacion());
				newItemDocOblEquipo.put(DOC_EQUIPO_NOMBRE_COL_NAME, tde.getNombre());
				newItemDocOblEquipo.put(DOC_EQUIPO_ORDEN_COL_NAME, tde.getOrden());
				newItemDocOblEquipo.put(DOC_EQUIPO_INCLUYE_FECHA_VENCIMIENTO_COL_NAME, tde.isIncluyeFechaVencimiento());
				newItemDocOblEquipo.put(DOC_EQUIPO_TIPO_ACCION_COL_NAME, tde.getTipoAccion());
				
				Cursor cursor_documentoObligatorioEquipos = mDb.query(CENTER_PAUTAS_DOC_OBLIGATORIOS_EQUIPO_TABLE, 
						new String[]{DOC_EQUIPO_ID_TIPO_DOC_OBL_EQUIPO_COL_NAME}, 
						DOC_EQUIPO_ID_TIPO_DOC_OBL_EQUIPO_COL_NAME + " = " + tde.getIdTipoDocumentoObligatorioEquipo(), 
						null, null, null, null);
				
				long insertedDocumentoObligatorioEquipos = -1;
				insertedDocumentoObligatorioEquipos = mDb.insertWithOnConflict(CENTER_PAUTAS_DOC_OBLIGATORIOS_EQUIPO_TABLE, 
															    null, 
															    newItemDocOblEquipo, 
															    SQLiteDatabase.CONFLICT_REPLACE);
				
				cursor_documentoObligatorioEquipos.close();
					
					
				
					Log.d(LOGTAG, "DocumentosObligatorioEquipos " + insertedDocumentoObligatorioEquipos + ", " + tde.getNombre());
			}
			
			List<TipoCertificacionExternaEquipo> certificacionExternaEquiposList = pautasCertificacion.getCertificacionesExternasEquipo();
			
//			Log.d(LOGTAG + " TipoCertificacionExternaEquipo", "Tama�o Lista TipoCertificacionExternaEquipo a INGRESAR: " + certificacionExternaEquiposList.size());
			
			for(TipoCertificacionExternaEquipo tcee : certificacionExternaEquiposList){
				
				/*
					public static final String CERT_EXT_EQUIPO_ID_TIPO_CERT_COL_NAME			= "IdTipoCertificacionExternaEquipo";
					public static final String CERT_EXT_EQUIPO_ID_PAUTA_CERT_COL_NAME			= "IdPautaCertificacion";
					public static final String CERT_EXT_EQUIPO_NOMBRE_COL_NAME					= "Nombre";
					public static final String CERT_EXT_EQUIPO_ORDEN_COL_NAME					= "Orden";
					public static final String CERT_EXT_EQUIPO_TIPO_ACCION_COL_NAME				= "TipoAccion";
					public static final String CERT_EXT_EQUIPO_TIPO_VENCIMIENTO_COL_NAME		= "TipoVencimiento";
				 */
				
				ContentValues newItemCertExtEquipo = new ContentValues();
				newItemCertExtEquipo.put(CERT_EXT_EQUIPO_ID_TIPO_CERT_COL_NAME, tcee.getIdTipoCertificacionExternaEquipo());
				
//				Log.d(LOGTAG, "TipoCertificacionExternaEquipo Id Pauta certificacion: "  + tcee.getIdPautaCertificacion());
//				Log.d(LOGTAG, "TipoCertificacionExternaEquipo Nombre: "  + tcee.getNombre());
				
				newItemCertExtEquipo.put(CERT_EXT_EQUIPO_ID_PAUTA_CERT_COL_NAME, tcee.getIdPautaCertificacion());
				newItemCertExtEquipo.put(CERT_EXT_EQUIPO_NOMBRE_COL_NAME, tcee.getNombre());
				newItemCertExtEquipo.put(CERT_EXT_EQUIPO_ORDEN_COL_NAME, tcee.getOrden());
				newItemCertExtEquipo.put(CERT_EXT_EQUIPO_TIPO_ACCION_COL_NAME, tcee.getTipoAccion());
				newItemCertExtEquipo.put(CERT_EXT_EQUIPO_TIPO_VENCIMIENTO_COL_NAME, tcee.getTipoVencimiento());
				
				Cursor cursor_certificacionExternaEquipos = mDb.query(CENTER_PAUTAS_CERT_EXTERNAS_EQUIPO_TABLE, 
						new String[]{CERT_EXT_EQUIPO_ID_TIPO_CERT_COL_NAME}, 
						CERT_EXT_EQUIPO_ID_TIPO_CERT_COL_NAME + " = " + tcee.getIdTipoCertificacionExternaEquipo(), 
						null, null, null, null);
				
				long insertedCertificacionExternaEquipos = -1;
				insertedCertificacionExternaEquipos = mDb.insertWithOnConflict(CENTER_PAUTAS_CERT_EXTERNAS_EQUIPO_TABLE, 
															    null, 
															    newItemCertExtEquipo, 
															    SQLiteDatabase.CONFLICT_REPLACE);
				
				cursor_certificacionExternaEquipos.close();
					
					
				
					Log.d(LOGTAG, "TipoCertificacionExternaEquipo " + insertedCertificacionExternaEquipos + ", " + tcee.getNombre());
			}
			
			
			
				Log.d(LOGTAG, "Agregado Pauta N� " + insertedRowIndex + ", " + pautasCertificacion.getNombre());
			
			return insertedRowIndex;
			
		}
		
		public String getNombrePauta(int idPauta){
			
			PautasCertificacion pautasCertificacion = new PautasCertificacion();
			String nombrePautaCertificacion = null;
			Cursor rslt = 
					mDb.query(CENTER_PAUTAS_CERTIFICACION_TABLE, 
							null, 
							PAUTAS_CERT_ID_PAUTA_COL_NAME + " = '" + idPauta + "'", 
							null, 
							null, 
							null, 
							null);
			
			if ( rslt.getCount() != 0 ) {
				rslt.moveToFirst();
				/*
				 * 	public static final String PAUTAS_CERT_ID_PAUTA_COL_NAME					= "ID_PAUTA_CERTIFICACION";
					public static final String PAUTAS_CERT_ID_CENTRO_CERT_COL_NAME				= "ID_CENTRO_CERTIFICADOR";
					public static final String PAUTAS_NOMBRE_PAUTA_CERT_COL_NAME				= "Nombre";
					public static final String PAUTAS_DESCRIPCION_COL_NAME						= "Descripcion";
					public static final String PAUTAS_TIPOCERTIFICACION_COL_NAME				= "TipoCertificacion";
					public static final String PAUTAS_CANTIDAD_INSTANCIAS_COL_NAME				= "CantidadInstancias";
					public static final String PAUTAS_INCLUYE_OBS_PORPREGUNTA_BOOLEAN_COL_NAME	= "ObservacionPorPreguntaBoolean";
					public static final String PAUTAS_INCLUYE_NO_APLICA_BOOLEAN_COL_NAME		= "IncluyeNoAplicaBoolean";
					public static final String PAUTAS_REVISION_COL_NAME							= "Revision";
					public static final String PAUTAS_IMAGEN_LOGO_CERTIFICACION_COL_NAME		= "ImgLogoCertificacion";
					public static final String PAUTAS_VENCIMIENTO_KM_HM_BOOLEAN_COL_NAME		= "VencimientoKM_HM_Boolean";
					public static final String PAUTAS_TIPO_ACCION_COL_NAME						= "TipoAccion";
					public static final String PAUTAS_INCLUYE__NUMERO_NIVEL_APROBACION_COL_NAME = "IncluyeNumeroNivelAprobacion";
				 * 
				 */
				
				nombrePautaCertificacion = rslt.getString(rslt.getColumnIndex(PAUTAS_NOMBRE_PAUTA_CERT_COL_NAME));
				pautasCertificacion.setIdPautaCertificacion(rslt.getInt(rslt.getColumnIndex(PAUTAS_CERT_ID_PAUTA_COL_NAME)));
				pautasCertificacion.setIdCentroCertificador(rslt.getInt(rslt.getColumnIndex(PAUTAS_CERT_ID_CENTRO_CERT_COL_NAME)));
				pautasCertificacion.setNombre(rslt.getString(rslt.getColumnIndex(PAUTAS_NOMBRE_PAUTA_CERT_COL_NAME)));
				pautasCertificacion.setDescripcion(rslt.getString(rslt.getColumnIndex(PAUTAS_DESCRIPCION_COL_NAME)));
				pautasCertificacion.setTipoCertificacion(rslt.getInt(rslt.getColumnIndex(PAUTAS_TIPOCERTIFICACION_COL_NAME)));
				pautasCertificacion.setCantidadInstancias(rslt.getInt(rslt.getColumnIndex(PAUTAS_CANTIDAD_INSTANCIAS_COL_NAME)));
				pautasCertificacion.setIncluyeObservacionPorPregunta(Boolean.getBoolean((rslt.getString(rslt.getColumnIndex(PAUTAS_INCLUYE_OBS_PORPREGUNTA_BOOLEAN_COL_NAME)))));
				pautasCertificacion.setIncluyeNoAplica(Boolean.getBoolean((rslt.getString(rslt.getColumnIndex(PAUTAS_INCLUYE_NO_APLICA_BOOLEAN_COL_NAME)))));
				pautasCertificacion.setRevision(rslt.getString(rslt.getColumnIndex(PAUTAS_REVISION_COL_NAME)));
				pautasCertificacion.setImagenLogoCertificacion(rslt.getBlob(rslt.getColumnIndex(PAUTAS_IMAGEN_LOGO_CERTIFICACION_COL_NAME)));
				pautasCertificacion.setVencimientoKmHm(rslt.getString(rslt.getColumnIndex(PAUTAS_VENCIMIENTO_KM_HM_BOOLEAN_COL_NAME)));
				pautasCertificacion.setTipoAccion(rslt.getInt(rslt.getColumnIndex(PAUTAS_TIPO_ACCION_COL_NAME)));
				pautasCertificacion.setIncluyeNumeroNivelAprobacion(Boolean.getBoolean((rslt.getString(rslt.getColumnIndex(PAUTAS_INCLUYE__NUMERO_NIVEL_APROBACION_COL_NAME)))));
				
				pautasCertificacion.setGruposPauta(getGrupoPauta(idPauta));
				pautasCertificacion.setItemsPauta(getItemPauta(idPauta));
				pautasCertificacion.setCertificacionesExternasEquipo(getCertificacionExternaEquipo(idPauta));
				pautasCertificacion.setDocumentosObligatorioEmpleado(getDocumentoObligatorioEmpleado(idPauta));
				pautasCertificacion.setDocumentosObligatorioEquipo(getDocumentoObligatorioEquipo(idPauta));
				
				
				/*
				 * public PautasCertificacion(int _idPautaCertificacion, int _idCentroCertificador, String _Nombre, String _Descripcion, int _tipoCertificacion,
								int _cantidadInstancias, boolean _incluyeObservacionPorPregunta, boolean incluyeNoAplica,
								String _Revision, byte[] _ImagenLogoCertificacion, String _vencimientoKmHm, int _tipoAccion, boolean incluyeNumeroNivelAprobacion,
								List<GrupoPauta> _gruposPauta, List<ItemPauta> _itemsPauta, 
								List<TipoDocumentoObligatorioEquipo> _documentosObligatorioEquipo,
								List<TipoDocumentoObligatorioEmpleado> _documentosObligatorioEmpleado,
								List<TipoCertificacionExternaEquipo> _certificacionesExternasEquipo)
				 */
					
			}else{
				
				Log.e(LOGTAG, "No se ha encontrado la Pauta con el ID indicado; ID: " + idPauta);
				
			}
			
			rslt.close();
			
			
			return nombrePautaCertificacion;
			
			
		}
		
		public List<PautasCertificacion> getPautas(int idPauta){
			List<PautasCertificacion> pautasList = new ArrayList<PautasCertificacion>();
			
			Cursor rslt = 
					mDb.query(CENTER_PAUTAS_CERTIFICACION_TABLE, 
							null, 
							PAUTAS_CERT_ID_PAUTA_COL_NAME + " = '" + idPauta + "'", 
							null, 
							null, 
							null, 
							null);
			
			if ( rslt.getCount() != 0 ) {
				rslt.moveToFirst();
				/*
				 * 	public static final String PAUTAS_CERT_ID_PAUTA_COL_NAME					= "ID_PAUTA_CERTIFICACION";
					public static final String PAUTAS_CERT_ID_CENTRO_CERT_COL_NAME				= "ID_CENTRO_CERTIFICADOR";
					public static final String PAUTAS_NOMBRE_PAUTA_CERT_COL_NAME				= "Nombre";
					public static final String PAUTAS_DESCRIPCION_COL_NAME						= "Descripcion";
					public static final String PAUTAS_TIPOCERTIFICACION_COL_NAME				= "TipoCertificacion";
					public static final String PAUTAS_CANTIDAD_INSTANCIAS_COL_NAME				= "CantidadInstancias";
					public static final String PAUTAS_INCLUYE_OBS_PORPREGUNTA_BOOLEAN_COL_NAME	= "ObservacionPorPreguntaBoolean";
					public static final String PAUTAS_INCLUYE_NO_APLICA_BOOLEAN_COL_NAME		= "IncluyeNoAplicaBoolean";
					public static final String PAUTAS_REVISION_COL_NAME							= "Revision";
					public static final String PAUTAS_IMAGEN_LOGO_CERTIFICACION_COL_NAME		= "ImgLogoCertificacion";
					public static final String PAUTAS_VENCIMIENTO_KM_HM_BOOLEAN_COL_NAME		= "VencimientoKM_HM_Boolean";
					public static final String PAUTAS_TIPO_ACCION_COL_NAME						= "TipoAccion";
					public static final String PAUTAS_INCLUYE__NUMERO_NIVEL_APROBACION_COL_NAME = "IncluyeNumeroNivelAprobacion";
				 * 
				 */

//				String count = String.valueOf(rslt.getCount());
//				Log.d(LOGTAG + " cursor count Agenda ", count);
				
				PautasCertificacion pautasCertificacion = new PautasCertificacion();
				pautasCertificacion.setIdPautaCertificacion(rslt.getInt(rslt.getColumnIndex(PAUTAS_CERT_ID_PAUTA_COL_NAME)));
				pautasCertificacion.setIdCentroCertificador(rslt.getInt(rslt.getColumnIndex(PAUTAS_CERT_ID_CENTRO_CERT_COL_NAME)));
				pautasCertificacion.setNombre(rslt.getString(rslt.getColumnIndex(PAUTAS_NOMBRE_PAUTA_CERT_COL_NAME)));
				
//				Log.d(LOGTAG, "Nombre Pauta: " + rslt.getString(rslt.getColumnIndex(PAUTAS_NOMBRE_PAUTA_CERT_COL_NAME)));
				
				pautasCertificacion.setDescripcion(rslt.getString(rslt.getColumnIndex(PAUTAS_DESCRIPCION_COL_NAME)));
				pautasCertificacion.setTipoCertificacion(rslt.getInt(rslt.getColumnIndex(PAUTAS_TIPOCERTIFICACION_COL_NAME)));
				pautasCertificacion.setCantidadInstancias(rslt.getInt(rslt.getColumnIndex(PAUTAS_CANTIDAD_INSTANCIAS_COL_NAME)));
				pautasCertificacion.setIncluyeObservacionPorPregunta(Boolean.getBoolean((rslt.getString(rslt.getColumnIndex(PAUTAS_INCLUYE_OBS_PORPREGUNTA_BOOLEAN_COL_NAME)))));
				pautasCertificacion.setIncluyeNoAplica(Boolean.getBoolean((rslt.getString(rslt.getColumnIndex(PAUTAS_INCLUYE_NO_APLICA_BOOLEAN_COL_NAME)))));
				pautasCertificacion.setRevision(rslt.getString(rslt.getColumnIndex(PAUTAS_REVISION_COL_NAME)));
				pautasCertificacion.setImagenLogoCertificacion(rslt.getBlob(rslt.getColumnIndex(PAUTAS_IMAGEN_LOGO_CERTIFICACION_COL_NAME)));
				pautasCertificacion.setVencimientoKmHm(rslt.getString(rslt.getColumnIndex(PAUTAS_VENCIMIENTO_KM_HM_BOOLEAN_COL_NAME)));
				pautasCertificacion.setTipoAccion(rslt.getInt(rslt.getColumnIndex(PAUTAS_TIPO_ACCION_COL_NAME)));
				pautasCertificacion.setIncluyeNumeroNivelAprobacion(Boolean.getBoolean((rslt.getString(rslt.getColumnIndex(PAUTAS_INCLUYE__NUMERO_NIVEL_APROBACION_COL_NAME)))));
				
				pautasCertificacion.setGruposPauta(getGrupoPauta(idPauta));
				pautasCertificacion.setItemsPauta(getItemPauta(idPauta));
				pautasCertificacion.setCertificacionesExternasEquipo(getCertificacionExternaEquipo(idPauta));
				pautasCertificacion.setDocumentosObligatorioEmpleado(getDocumentoObligatorioEmpleado(idPauta));
				pautasCertificacion.setDocumentosObligatorioEquipo(getDocumentoObligatorioEquipo(idPauta));
				
				
				pautasList.add(pautasCertificacion);
				/*
				 * public PautasCertificacion(int _idPautaCertificacion, int _idCentroCertificador, String _Nombre, String _Descripcion, int _tipoCertificacion,
								int _cantidadInstancias, boolean _incluyeObservacionPorPregunta, boolean incluyeNoAplica,
								String _Revision, byte[] _ImagenLogoCertificacion, String _vencimientoKmHm, int _tipoAccion, boolean incluyeNumeroNivelAprobacion,
								List<GrupoPauta> _gruposPauta, List<ItemPauta> _itemsPauta, 
								List<TipoDocumentoObligatorioEquipo> _documentosObligatorioEquipo,
								List<TipoDocumentoObligatorioEmpleado> _documentosObligatorioEmpleado,
								List<TipoCertificacionExternaEquipo> _certificacionesExternasEquipo)
				 */
					
			}else{
				
				Log.e(LOGTAG, "No se han encontrado pautas con el ID indicado; ID: " + idPauta);
				
			}
			
			rslt.close();
			
			
			return pautasList;
			
			
			
		}
		
		public List<GrupoPauta> getGrupoPauta(int idPauta){
			
			List<GrupoPauta> grupoPautasList = new ArrayList<GrupoPauta>();
			Cursor rslt = 
					mDb.query(CENTER_PAUTAS_GRUPO_CERTIFICACION_TABLE, 
							null, 
							GRUPO_ID_PAUTA_CERT_COL_NAME + " = '" + idPauta + "'", 
							null, 
							null, 
							null, 
							GRUPO_ORDEN_COL_NAME);
			
			if ( rslt.getCount() != 0 ) {
				if(rslt.moveToFirst()){
					
					do{
						
						String count = String.valueOf(rslt.getCount());
						
//						Log.d(LOGTAG + " cursor count GrupoPauta ", count);
						
						/*
							public static final String GRUPO_ID_GRUPOPAUTA_COL_NAME	= "ID_GRUPOPAUTA";
							public static final String GRUPO_ID_PAUTA_CERT_COL_NAME	= "ID_PAUTA_CERT";
							public static final String GRUPO_NOMBRE_COL_NAME		= "Nombre";
							public static final String GRUPO_ORDEN_COL_NAME			= "Orden";
							public static final String GRUPO_TIPOACCION_COL_NAME	= "TipoAccion";
							
								
						Cursor cursor_GrupoPauta = mDb.query(CENTER_PAUTAS_GRUPO_CERTIFICACION_TABLE, 
						new String[]{GRUPO_ID_GRUPOPAUTA_COL_NAME}, 
						GRUPO_ID_GRUPOPAUTA_COL_NAME + " = " + gp.getIdGrupoPauta(), 
						null, null, null, null);
						 * 
						 */
						
						GrupoPauta grupoPauta = new GrupoPauta();
						
						grupoPauta.setIdGrupoPauta(rslt.getInt(rslt.getColumnIndex(GRUPO_ID_GRUPOPAUTA_COL_NAME)));
						grupoPauta.setIdPautaCertificacion(rslt.getInt(rslt.getColumnIndex(GRUPO_ID_PAUTA_CERT_COL_NAME)));
						grupoPauta.setNombre(rslt.getString(rslt.getColumnIndex(GRUPO_NOMBRE_COL_NAME)));
						grupoPauta.setOrden(rslt.getInt(rslt.getColumnIndex(GRUPO_ORDEN_COL_NAME)));
						grupoPauta.setTipoAccion(rslt.getInt(rslt.getColumnIndex(GRUPO_TIPOACCION_COL_NAME)));
						
						grupoPautasList.add(grupoPauta);
						
					}while(rslt.moveToNext());
					
					
				}
				
				
			}else{
				
				Log.e(LOGTAG, "No se han encontrado GrupoPauta con el ID indicado; ID: " + idPauta);
				
			}
			
			rslt.close();
			
			
			return grupoPautasList;
			
			
		}
		
		public List<ItemPauta> getItemPauta(int idPauta){
			
			List<ItemPauta> itemPautasList = new ArrayList<ItemPauta>();
			Cursor rslt = 
					mDb.query(CENTER_PAUTAS_ITEM_CERTIFICACION_TABLE, 
							null, 
							ITEM_ID_PAUTA_CERT_COL_NAME + " = '" + idPauta + "'", 
							null, 
							null, 
							null, 
							ITEM_ORDEN_COL_NAME);
			
			if ( rslt.getCount() != 0 ) {
				if(rslt.moveToFirst()){
					
					do{
						
						String count = String.valueOf(rslt.getCount());
						
//						Log.d(LOGTAG + " cursor count ItemPauta ", count);
						
						/*
							public static final String ITEM_ID_PAUTA_COL_NAME		= "ID_ITEM_PAUTA";	
							public static final String ITEM_ID_GRUPO_COL_NAME		= "ID_GRUPO_PAUTA";
							public static final String ITEM_ID_PAUTA_CERT_COL_NAME	= "ID_PAUTA_CERT";
							public static final String ITEM_NOMBRE_COL_NAME			= "Nombre";
							public static final String ITEM_ORDEN_COL_NAME			= "Orden";
							public static final String ITEM_TIPO_ACCION_COL_NAME	= "TipoAccion";
						 * 
						 */
						
						ItemPauta itemPauta = new ItemPauta();
						
						itemPauta.setIdItemPauta(rslt.getInt(rslt.getColumnIndex(ITEM_ID_PAUTA_COL_NAME)));
						itemPauta.setIdGrupoPauta(rslt.getInt(rslt.getColumnIndex(ITEM_ID_GRUPO_COL_NAME)));
						itemPauta.setIdPautaCertificacion(rslt.getInt(rslt.getColumnIndex(ITEM_ID_PAUTA_CERT_COL_NAME)));
						itemPauta.setNombre(rslt.getString(rslt.getColumnIndex(ITEM_NOMBRE_COL_NAME)));
						itemPauta.setOrden(rslt.getInt(rslt.getColumnIndex(ITEM_ORDEN_COL_NAME)));
						itemPauta.setTipoAccion(rslt.getInt(rslt.getColumnIndex(ITEM_TIPO_ACCION_COL_NAME)));
						
						itemPautasList.add(itemPauta);
						
					}while(rslt.moveToNext());
					
					
				}
				
				
			}else{
				
				Log.e(LOGTAG, "No se han encontrado ItemPauta con el ID indicado; ID: " + idPauta);
				
			}
			
			rslt.close();
			
			
			return itemPautasList;
			
			
		}
		
		public List<TipoDocumentoObligatorioEmpleado> getDocumentoObligatorioEmpleado(int idPauta){
			
			List<TipoDocumentoObligatorioEmpleado> documentoObligatorioEmpleadosList = new ArrayList<TipoDocumentoObligatorioEmpleado>();
			
			Cursor rslt = 
					mDb.query(CENTER_PAUTAS_DOC_OBLIGATORIOS_EMPLEADO_TABLE, 
							null, 
							DOC_EMPLEADO_ID_PAUTA_CERT_COL_NAME + " = '" + idPauta + "'", 
							null, 
							null, 
							null, 
							null);
			
			if ( rslt.getCount() != 0 ) {
				if(rslt.moveToFirst()){
					
					do{
						
						String count = String.valueOf(rslt.getCount());
						
//						Log.d(LOGTAG + " cursor count DOC_OBLIGATORIOS_EMPLEADO ", count);
						
						/*
						 * 
			
			public static final String DOC_EMPLEADO_ID_TIPO_DOC_OBL_EQUIPO_COL_NAME		= "IdTipoDocumentoObligatorioEquipo";
			public static final String DOC_EMPLEADO_ID_PAUTA_CERT_COL_NAME				= "IdPautaCertificacion";
			public static final String DOC_EMPLEADO_NOMBRE_COL_NAME						= "Nombre";
			public static final String DOC_EMPLEADO_ORDEN_COL_NAME						= "Orden";
			public static final String DOC_EMPLEADO_INCLUYE_FECHA_VENCIMIENTO_COL_NAME	= "IncluyeFechaVencimiento";
			public static final String DOC_EMPLEADO_TIPO_ACCION_COL_NAME				= "TipoAccion";
						 * 
						 */
						
						TipoDocumentoObligatorioEmpleado documentoObligatorioEmpleado = new TipoDocumentoObligatorioEmpleado();
						documentoObligatorioEmpleado.setIdTipoDocumentoObligatorioEmpleado(rslt.getInt(rslt.getColumnIndex(DOC_EMPLEADO_ID_TIPO_DOC_OBL_EMPLEADO_COL_NAME)));
						documentoObligatorioEmpleado.setIdPautaCertificacion(rslt.getInt(rslt.getColumnIndex(DOC_EMPLEADO_ID_PAUTA_CERT_COL_NAME)));
						documentoObligatorioEmpleado.setNombre(rslt.getString(rslt.getColumnIndex(DOC_EMPLEADO_NOMBRE_COL_NAME)));
						
						//Log.i("Nombre doc obl empleado", rslt.getString(rslt.getColumnIndex(DOC_EMPLEADO_NOMBRE_COL_NAME)));
						
						documentoObligatorioEmpleado.setIncluyeFechaVencimiento(Boolean.getBoolean(rslt.getString(rslt.getColumnIndex(DOC_EMPLEADO_INCLUYE_FECHA_VENCIMIENTO_COL_NAME))));
						documentoObligatorioEmpleado.setOrden(rslt.getInt(rslt.getColumnIndex(DOC_EMPLEADO_ORDEN_COL_NAME)));
						documentoObligatorioEmpleado.setTipoAccion(rslt.getInt(rslt.getColumnIndex(DOC_EMPLEADO_TIPO_ACCION_COL_NAME)));
						/*
						 *  public TipoDocumentoObligatorioEmpleado(int _idTipoDocumentoObligatorioEmpleado, int _idPautaCertificacion, String _Nombre,
		    		 								boolean _incluyeFechaVencimiento, int _Orden, int _TipoAccion){
		    	
						 */
						documentoObligatorioEmpleadosList.add(documentoObligatorioEmpleado);
						
					}while(rslt.moveToNext());
					
					
				}
				
				
			}else{
				
				Log.e(LOGTAG, "No se han encontrado Documento obligatorio para Empleado con el ID indicado; ID: " + idPauta);
				
			}
			
			rslt.close();
			
			
			return documentoObligatorioEmpleadosList;
			
			
			
			
		}
		
		public List<TipoDocumentoObligatorioEquipo> getDocumentoObligatorioEquipo(int idPauta){
			
			List<TipoDocumentoObligatorioEquipo> documentoObligatorioEquiposList = new ArrayList<TipoDocumentoObligatorioEquipo>();
			Cursor rslt = 
					mDb.query(CENTER_PAUTAS_DOC_OBLIGATORIOS_EQUIPO_TABLE, 
							null, 
							DOC_EQUIPO_ID_PAUTA_CERT_COL_NAME + " = '" + idPauta + "'", 
							null, 
							null, 
							null, 
							null);
			
			if ( rslt.getCount() != 0 ) {
				if(rslt.moveToFirst()){
					
					do{
						
						String count = String.valueOf(rslt.getCount());
						
//						Log.d(LOGTAG + " cursor count DOC_OBLIGATORIOS_EQUIPO ", count);
						
						/*
						 * 
							public static final String DOC_EQUIPO_ID_TIPO_DOC_OBL_EQUIPO_COL_NAME		= "IdTipoDocumentoObligatorioEquipo";
							public static final String DOC_EQUIPO_ID_PAUTA_CERT_COL_NAME				= "IdPautaCertificacion";
							public static final String DOC_EQUIPO_NOMBRE_COL_NAME						= "Nombre";
							public static final String DOC_EQUIPO_ORDEN_COL_NAME						= "Orden";
							public static final String DOC_EQUIPO_INCLUYE_FECHA_VENCIMIENTO_COL_NAME	= "IncluyeFechaVencimiento";
							public static final String DOC_EQUIPO_TIPO_ACCION_COL_NAME					= "TipoAccion";
						 * 
						 */
						
						TipoDocumentoObligatorioEquipo documentoObligatorioEquipo = new TipoDocumentoObligatorioEquipo();
						
						documentoObligatorioEquipo.setIdTipoDocumentoObligatorioEquipo(rslt.getInt(rslt.getColumnIndex(DOC_EQUIPO_ID_TIPO_DOC_OBL_EQUIPO_COL_NAME)));
						documentoObligatorioEquipo.setIdPautaCertificacion(rslt.getInt(rslt.getColumnIndex(DOC_EQUIPO_ID_PAUTA_CERT_COL_NAME)));
						documentoObligatorioEquipo.setNombre(rslt.getString(rslt.getColumnIndex(DOC_EQUIPO_NOMBRE_COL_NAME)));
						
						//Log.i("Nombre doc obl empleado", rslt.getString(rslt.getColumnIndex(DOC_EMPLEADO_NOMBRE_COL_NAME)));
						
						documentoObligatorioEquipo.setIncluyeFechaVencimiento(Boolean.getBoolean(rslt.getString(rslt.getColumnIndex(DOC_EQUIPO_INCLUYE_FECHA_VENCIMIENTO_COL_NAME))));
						documentoObligatorioEquipo.setOrden(rslt.getInt(rslt.getColumnIndex(DOC_EQUIPO_ORDEN_COL_NAME)));
						documentoObligatorioEquipo.setTipoAccion(rslt.getInt(rslt.getColumnIndex(DOC_EQUIPO_TIPO_ACCION_COL_NAME)));
						/*
						 *  public TipoDocumentoObligatorioEmpleado(int _idTipoDocumentoObligatorioEmpleado, int _idPautaCertificacion, String _Nombre,
		    		 								boolean _incluyeFechaVencimiento, int _Orden, int _TipoAccion){
		    	
						 */
						documentoObligatorioEquiposList.add(documentoObligatorioEquipo);
						
					}while(rslt.moveToNext());
					
					
				}
				
				
			}else{
				
				Log.e(LOGTAG, "No se han encontrado documentos obligatorios para Equipo con el ID indicado; ID: " + idPauta);
				
			}
			
			rslt.close();
			
			
			return documentoObligatorioEquiposList;
			
			
			
			
		}
		
		public List<TipoCertificacionExternaEquipo> getCertificacionExternaEquipo(int idPauta){
			
			List<TipoCertificacionExternaEquipo> certificacionExternaEquiposList = new ArrayList<TipoCertificacionExternaEquipo>();
			Cursor rslt = 
					mDb.query(CENTER_PAUTAS_CERT_EXTERNAS_EQUIPO_TABLE, 
							null, 
							CERT_EXT_EQUIPO_ID_PAUTA_CERT_COL_NAME + " = '" + idPauta + "'", 
							null, 
							null, 
							null, 
							CERT_EXT_EQUIPO_ORDEN_COL_NAME);
			
			if ( rslt.getCount() != 0 ) {
				if(rslt.moveToFirst()){
					
					do{
						
						String count = String.valueOf(rslt.getCount());
						
//						Log.d(LOGTAG + " cursor count CERT_EXT_EQUIPO ", count);
						
						/*
							public static final String CERT_EXT_EQUIPO_ID_TIPO_CERT_COL_NAME			= "IdTipoCertificacionExternaEquipo";
							public static final String CERT_EXT_EQUIPO_ID_PAUTA_CERT_COL_NAME			= "IdPautaCertificacion";
							public static final String CERT_EXT_EQUIPO_NOMBRE_COL_NAME					= "Nombre";
							public static final String CERT_EXT_EQUIPO_ORDEN_COL_NAME					= "Orden";
							public static final String CERT_EXT_EQUIPO_TIPO_ACCION_COL_NAME				= "TipoAccion";
							public static final String CERT_EXT_EQUIPO_TIPO_VENCIMIENTO_COL_NAME		= "TipoVencimiento";
						 * 
						 */
						
						TipoCertificacionExternaEquipo certificacionExternaEquipo = new TipoCertificacionExternaEquipo();
						
						certificacionExternaEquipo.setIdTipoCertificacionExternaEquipo(rslt.getInt(rslt.getColumnIndex(CERT_EXT_EQUIPO_ID_TIPO_CERT_COL_NAME)));
						certificacionExternaEquipo.setIdPautaCertificacion(rslt.getInt(rslt.getColumnIndex(CERT_EXT_EQUIPO_ID_PAUTA_CERT_COL_NAME)));
						certificacionExternaEquipo.setNombre(rslt.getString(rslt.getColumnIndex(CERT_EXT_EQUIPO_NOMBRE_COL_NAME)));
						certificacionExternaEquipo.setOrden(rslt.getInt(rslt.getColumnIndex(CERT_EXT_EQUIPO_ORDEN_COL_NAME)));
						certificacionExternaEquipo.setTipoAccion(rslt.getInt(rslt.getColumnIndex(CERT_EXT_EQUIPO_TIPO_ACCION_COL_NAME)));
						certificacionExternaEquipo.setTipoVencimiento(rslt.getInt(rslt.getColumnIndex(CERT_EXT_EQUIPO_TIPO_VENCIMIENTO_COL_NAME)));
						
						certificacionExternaEquiposList.add(certificacionExternaEquipo);
						
					}while(rslt.moveToNext());
					
					
				}
				
				
			}else{
				
				Log.e(LOGTAG, "No se han encontrado CertificacionExternaEquipo con el ID indicado; ID: " + idPauta);
				
			}
			
			rslt.close();
			
			
			return certificacionExternaEquiposList;
			
			
		}
		
		public List<AgendaData> getAgenda() {
			
			//Comparar Id Persona con ID Certificador para obtener los datos de la agenda.
			
			List<AgendaData> agendaList = new ArrayList<AgendaData>();
			Cursor rslt = 
					mDb.query(CENTER_AGENDA_TABLE, 
							null, 
							null,
							null, 
							null, 
							null, 
							null);
			
			if ( rslt.getCount() != 0 ) {
				if(rslt.moveToFirst()){
					
					do{
						
//						String count = String.valueOf(rslt.getCount());
//						Log.d(LOGTAG + " cursor count Agenda ", count);
						
						/*
						 *	 private int  ;
							private int idPautaCertificacion;
							private int idCertificador;
							private int tipoCertificacion;
							private String fechaRequerimento;
							private String fechaProgramacion;
							private String ubicacion;
							private String contacto;
							private int tipoAccion;
						 */
						
						AgendaData agendaData = new AgendaData();
						agendaData.setIdProgramacionCertificacion(rslt.getInt(AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NUM));
						agendaData.setIdPautaCertificacion(rslt.getInt(AGENDA_ID_PAUTA_CERTIFICACION_COL_NUM));
						agendaData.setIdEquipo(rslt.getInt(AGENDA_ID_EQUIPO_COL_NUM));
						agendaData.setIdEmpleado(rslt.getInt(AGENDA_ID_EMPLEADO_COL_NUM));
						agendaData.setIdCertificador(rslt.getInt(AGENDA_ID_CERTIFICADOR_COL_NUM));
						agendaData.setTipoCertificacion(rslt.getInt(AGENDA_TIPO_CERTIFICACION_COL_NUM));
						
//						Log.d(LOGTAG, AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NAME + ":  " + String.valueOf(rslt.getInt(AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NUM)));
//						Log.d(LOGTAG, AGENDA_TIPO_CERTIFICACION_COL_NAME + ":  " + String.valueOf(rslt.getInt(AGENDA_TIPO_CERTIFICACION_COL_NUM)));
						
						agendaData.setFechaRequerimento(rslt.getString(AGENDA_FECHA_REQUERIMIENTO_COL_NUM));
						agendaData.setFechaProgramacion(rslt.getString(AGENDA_FECHA_PROGRAMACION_COL_NUM));
						agendaData.setUbicacion(rslt.getString(AGENDA_UBICACION_COL_NUM));
						agendaData.setContacto(rslt.getString(AGENDA_CONTACTO_COL_NUM));
						agendaData.setTipoAccion(rslt.getInt(AGENDA_TIPO_ACCION_COL_NUM));
						agendaData.setJsonEnviar(rslt.getString(AGENDA_JSON_ENVIAR_COL_NUM));
						agendaData.setJsonRellenar(rslt.getString(AGENDA_JSON_RELLENAR_COL_NUM));
						agendaData.setFinalizado(rslt.getString(AGENDA_BO_FINALIZADO_COL_NUM));
						
						Log.d(LOGTAG, AGENDA_JSON_RELLENAR_COL_NAME + ":  " + rslt.getString(AGENDA_JSON_RELLENAR_COL_NUM));
						
						agendaList.add(agendaData);
						
					}while(rslt.moveToNext());
					
					
				}
				
				
			}else{
				
				Log.e(LOGTAG, "No se han encontrado Datos en la Agenda");
				
			}
			
			rslt.close();
			return agendaList;
		}
		
		public EmpleadoData getEmpleado(int idEmpleado) {
			
			EmpleadoData empleadoData = new EmpleadoData();
			Cursor rslt = 
					mDb.query(CENTER_PAUTA_DATA_EMPLEADO_TABLE, 
							null, 
							DATA_EMPLEADO_ID_COL_NAME + " = '" + idEmpleado + "'",
							null, 
							null, 
							null, 
							null);
			
			if ( rslt.getCount() != 0 ) {
				if(rslt.moveToFirst()){
					
					do{
						
						empleadoData.setIdEmpleado(rslt.getInt(rslt.getColumnIndex(DATA_EMPLEADO_ID_COL_NAME)));
						empleadoData.setRUN(rslt.getInt(rslt.getColumnIndex(DATA_EMPLEADO_RUN_COL_NAME)));
						empleadoData.setDigitoVerificador(rslt.getString(rslt.getColumnIndex(DATA_EMPLEADO_DIGITO_VERIFICADOR_COL_NAME)));
						empleadoData.setTelefono(rslt.getString(rslt.getColumnIndex(DATA_EMPLEADO_TELEFONO_COL_NAME)));
						empleadoData.setNombres(rslt.getString(rslt.getColumnIndex(DATA_EMPLEADO_NOMBRES_COL_NAME)));
						empleadoData.setPrimerApellido(rslt.getString(rslt.getColumnIndex(DATA_EMPLEADO_PRIMER_APELLIDO_COL_NAME)));
						empleadoData.setSegundoApellido(rslt.getString(rslt.getColumnIndex(DATA_EMPLEADO_SEGUNDO_APELLIDO_COL_NAME)));
						empleadoData.setCelular(rslt.getString(rslt.getColumnIndex(DATA_EMPLEADO_CELULAR_COL_NAME)));
						empleadoData.setTipoAccion(rslt.getInt(rslt.getColumnIndex(DATA_EMPLEADO_TIPO_ACCION_COL_NAME)));
						
					}while(rslt.moveToNext());
					
					
				}
				
				
			}else{
				
				Log.e(LOGTAG, "No se han encontrado Datos del Empleado");
				
			}
			
			rslt.close();
			return empleadoData;
		}
		
		public EquipoData getEquipo(int idEquipo) {
			
			EquipoData equipoData = new EquipoData();
			
			Cursor rslt = 
					mDb.query(CENTER_PAUTA_DATA_EQUIPO_TABLE, 
							null, 
							DATA_EQUIPO_ID_EQUIPO_COL_NAME + " = '" + idEquipo + "'",
							null, 
							null, 
							null, 
							null);
			
			if ( rslt.getCount() != 0 ) {
				if(rslt.moveToFirst()){
					
					do{
						
						equipoData.setIdEquipo(rslt.getInt(rslt.getColumnIndex(DATA_EQUIPO_ID_EQUIPO_COL_NAME)));
						equipoData.setMarca(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_MARCA_COL_NAME)));
						equipoData.setModelo(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_MODELO_COL_NAME)));
						equipoData.setAno(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_AÑO_COL_NAME)));
						equipoData.setPlacaPatente(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_PLACA_PATENTE_COL_NAME)));
						equipoData.setCodigoInterno(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_CODIGO_INTERNO_COL_NAME)));
						equipoData.setColor(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_COLOR_COL_NAME)));
						equipoData.setNumeroMotor(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_NUMERO_SERIE_COL_NAME)));
						equipoData.setNumeroSerie(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_NUMERO_SERIE_COL_NAME)));
						equipoData.setNumeroFactura(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_NUMERO_FACTURA_COL_NAME)));
						equipoData.setNumeroGuiaDespacho(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_NUMERO_GUIA_DESPACHO_COL_NAME)));
						equipoData.setCapacidad(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_CAPACIDAD_COL_NAME)));
						equipoData.setPotencia(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_POTENCIA_COL_NAME)));
						equipoData.setFechaCertificacion(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_POTENCIA_COL_NAME)));
						equipoData.setTipoAccion(rslt.getString(rslt.getColumnIndex(DATA_EQUIPO_FECHA_CERTIFICACION_COL_NAME)));
						
					}while(rslt.moveToNext());
					
					
				}
				
				
			}else{
				
				Log.e(LOGTAG, "No se han encontrado Datos del Equipo");
				
			}
			
			rslt.close();
			return equipoData;
		}
		
		public String passToSha1(String password, String salt) throws Exception {
			 
			 StringBuilder sb = new StringBuilder();
			 
				try{
					
					byte[] passwordByte = password.getBytes("UTF-8");
					byte[] saltByte = salt.getBytes("UTF-8");
							
				    Mac localMac =  Mac.getInstance("HmacSHA1");
			
			        localMac.init(new SecretKeySpec(saltByte, localMac.getAlgorithm()));        
			        byte[] sha1hash = localMac.doFinal(passwordByte);
				    	        
					for (byte b : sha1hash) {
						sb.append(String.format("%02x", b));
					}		    
				    
			    } catch (Exception e) {
			        e.printStackTrace();
			    }		
				return sb.toString();
				 
		        
		    }

		public long insertAgenda(AgendaData agendaData) {
			
			/*
			 * 	private int idProgramacionCertificacion;
				private int idPautaCertificacion;
				private int idEquipo;
				private int idEmpleado;
				private int idCertificador;
				private int tipoCertificacion;
				private String fechaRequerimento;
				private String fechaProgramacion;
				private String ubicacion;
				private String contacto;
				private int tipoAccion;
			 */
			
			ContentValues newAgenda = new ContentValues();
			newAgenda.put(AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NAME, agendaData.getIdProgramacionCertificacion());
			newAgenda.put(AGENDA_ID_PAUTA_CERTIFICACION_COL_NAME, agendaData.getIdPautaCertificacion());
			newAgenda.put(AGENDA_ID_EQUIPO_COL_NAME, agendaData.getIdEquipo());
			newAgenda.put(AGENDA_ID_EMPLEADO_COL_NAME, agendaData.getIdEmpleado());
			newAgenda.put(AGENDA_ID_CERTIFICADOR_COL_NAME, agendaData.getIdCertificador());
			newAgenda.put(AGENDA_TIPO_CERTIFICACION_COL_NAME, agendaData.getTipoCertificacion());
			
//			Log.d(LOGTAG, "===============================================================================");
//			Log.d(LOGTAG, AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NAME + ": " + agendaData.getIdProgramacionCertificacion());
//			Log.d(LOGTAG, AGENDA_ID_PAUTA_CERTIFICACION_COL_NAME + ": " + agendaData.getIdPautaCertificacion());
//			Log.d(LOGTAG, AGENDA_ID_CERTIFICADOR_COL_NAME + ": " + agendaData.getIdCertificador());
//			Log.d(LOGTAG, AGENDA_TIPO_CERTIFICACION_COL_NAME + ": " + agendaData.getTipoCertificacion());
//			Log.d(LOGTAG, AGENDA_FECHA_REQUERIMIENTO_COL_NAME + ": " + agendaData.getFechaRequerimento());
//			Log.d(LOGTAG, AGENDA_FECHA_PROGRAMACION_COL_NAME + ": " + agendaData.getFechaProgramacion());
//			Log.d(LOGTAG, "===============================================================================");
			
			newAgenda.put(AGENDA_FECHA_REQUERIMIENTO_COL_NAME, agendaData.getFechaRequerimento());
			newAgenda.put(AGENDA_FECHA_PROGRAMACION_COL_NAME, agendaData.getFechaProgramacion());
			newAgenda.put(AGENDA_UBICACION_COL_NAME, agendaData.getUbicacion());
			newAgenda.put(AGENDA_CONTACTO_COL_NAME, agendaData.getContacto());
			newAgenda.put(AGENDA_TIPO_ACCION_COL_NAME, agendaData.getTipoAccion());
		
			Cursor rslt = mDb.query(CENTER_AGENDA_TABLE, 
					new String[]{AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NAME}, 
					AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NAME + "=" + agendaData.getIdProgramacionCertificacion(), 
					null, null, null, null);
			
			long insertedRowIndex = -1;
				insertedRowIndex = mDb.insertWithOnConflict(CENTER_AGENDA_TABLE, 
																    null, 
																    newAgenda, 
																    SQLiteDatabase.CONFLICT_REPLACE);
			
			rslt.close();
			
				Log.d(LOGTAG, "Agregado Agenda N� " + insertedRowIndex + ", " + agendaData.getIdPautaCertificacion());
			
			return insertedRowIndex;
			
		}
		
		public void updateAgenda(AgendaData agendaData) {
//			AGENDA_ID_PROGRAMACION_CERTIFICACION_COL_NAME
			
		    long id = agendaData.getIdPautaCertificacion();
		    
//		    String[] whereClauseArgument = new String[3];
//			whereClauseArgument[0] = String.valueOf(agendaData.getIdProgramacionCertificacion());

		    ContentValues contentValues = new ContentValues();
		    
		    Log.d("jsonEnviar", agendaData.getJsonEnviar());
		    Log.d("jsonRelleno", agendaData.getJsonRellenar());
		    
			contentValues.put(AGENDA_JSON_ENVIAR_COL_NAME, agendaData.getJsonEnviar());
			contentValues.put(AGENDA_JSON_RELLENAR_COL_NAME, agendaData.getJsonRellenar());
			contentValues.put(AGENDA_BO_FINALIZADO_COL_NAME, agendaData.getFinalizado());

//			Cursor rslt = mDb.query(CENTER_AGENDA_TABLE, 
//					new String[]{AGENDA_ID_PAUTA_CERTIFICACION_COL_NAME, AGENDA_JSON_ENVIAR_COL_NAME, AGENDA_JSON_RELLENAR_COL_NAME, AGENDA_BO_FINALIZADO_COL_NAME}, 
//					AGENDA_ID_PAUTA_CERTIFICACION_COL_NAME + "=" + agendaData.getIdPautaCertificacion(), 
//					null, null, null, null);
//			
			long insertedRowIndex = -1;
				insertedRowIndex = mDb.update(CENTER_AGENDA_TABLE, contentValues, AGENDA_ID_PAUTA_CERTIFICACION_COL_NAME + " = " + id, null);
			
				Log.d(LOGTAG, "Valor index: " + insertedRowIndex);
				
				if(insertedRowIndex != -1){
					 Log.d(LOGTAG, "Se ha Actualziado la Agenda con el ID: " + id );
				}else{
					
					Log.d(LOGTAG, "La Agenda NO  se ha actualizado...");
					
				}
				
//			rslt.close();
		    
		   
		}
		
		public long insertEmpleado(EmpleadoData empleadoData) {
			
			ContentValues newEmpleado = new ContentValues();
			newEmpleado.put(DATA_EMPLEADO_ID_COL_NAME, empleadoData.getIdEmpleado());
			newEmpleado.put(DATA_EMPLEADO_RUN_COL_NAME, empleadoData.getRUN());
			newEmpleado.put(DATA_EMPLEADO_DIGITO_VERIFICADOR_COL_NAME, empleadoData.getDigitoVerificador());
			newEmpleado.put(DATA_EMPLEADO_TELEFONO_COL_NAME, empleadoData.getTelefono());
			newEmpleado.put(DATA_EMPLEADO_NOMBRES_COL_NAME, empleadoData.getNombres());
			newEmpleado.put(DATA_EMPLEADO_PRIMER_APELLIDO_COL_NAME, empleadoData.getPrimerApellido());
			newEmpleado.put(DATA_EMPLEADO_SEGUNDO_APELLIDO_COL_NAME, empleadoData.getSegundoApellido());
			newEmpleado.put(DATA_EMPLEADO_CELULAR_COL_NAME, empleadoData.getCelular());
			newEmpleado.put(DATA_EMPLEADO_TIPO_ACCION_COL_NAME, empleadoData.getTipoAccion());
			
			Log.d(LOGTAG, "===============================================================================");
			Log.d(LOGTAG, DATA_EMPLEADO_ID_COL_NAME + ": " + empleadoData.getIdEmpleado());
			Log.d(LOGTAG, DATA_EMPLEADO_RUN_COL_NAME + ": " + empleadoData.getRUN());
			Log.d(LOGTAG, DATA_EMPLEADO_NOMBRES_COL_NAME + ": " + empleadoData.getNombres());
			Log.d(LOGTAG, DATA_EMPLEADO_PRIMER_APELLIDO_COL_NAME + ": " + empleadoData.getPrimerApellido());
			Log.d(LOGTAG, DATA_EMPLEADO_SEGUNDO_APELLIDO_COL_NAME + ": " + empleadoData.getSegundoApellido());
			Log.d(LOGTAG, "===============================================================================");
		
			Cursor rslt = mDb.query(CENTER_PAUTA_DATA_EMPLEADO_TABLE, 
					new String[]{DATA_EMPLEADO_ID_COL_NAME}, 
					DATA_EMPLEADO_ID_COL_NAME + "=" + empleadoData.getIdEmpleado(), 
					null, null, null, null);
			
			long insertedEmpleadoIndex = -1;
			insertedEmpleadoIndex = mDb.insertWithOnConflict(CENTER_PAUTA_DATA_EMPLEADO_TABLE, 
																    null, 
																    newEmpleado, 
																    SQLiteDatabase.CONFLICT_REPLACE);
			
			rslt.close();
			
				Log.d(LOGTAG, "Agregado Empleado N� " + insertedEmpleadoIndex + ", " + empleadoData.getIdEmpleado());
			
			return insertedEmpleadoIndex;
			
		}
		
		public long insertEquipo(EquipoData equipoData){
			
			Log.d("Data Equipo Log", "Ingresando Datos Equipos");
			
			ContentValues newEquipo = new ContentValues();
			
			newEquipo.put(DATA_EQUIPO_ID_EQUIPO_COL_NAME, equipoData.getIdEquipo());
			newEquipo.put(DATA_EQUIPO_MARCA_COL_NAME, equipoData.getMarca());
			newEquipo.put(DATA_EQUIPO_AÑO_COL_NAME, equipoData.getAno());
			newEquipo.put(DATA_EQUIPO_PLACA_PATENTE_COL_NAME, equipoData.getPlacaPatente());
			newEquipo.put(DATA_EQUIPO_CODIGO_INTERNO_COL_NAME, equipoData.getCodigoInterno());
			newEquipo.put(DATA_EQUIPO_COLOR_COL_NAME, equipoData.getColor());
			newEquipo.put(DATA_EQUIPO_NUMERO_MOTOR_COL_NAME, equipoData.getNumeroMotor());
			newEquipo.put(DATA_EQUIPO_NUMERO_SERIE_COL_NAME, equipoData.getNumeroSerie());
			newEquipo.put(DATA_EQUIPO_NUMERO_FACTURA_COL_NAME, equipoData.getNumeroFactura());
			newEquipo.put(DATA_EQUIPO_NUMERO_GUIA_DESPACHO_COL_NAME, equipoData.getNumeroGuiaDespacho());
			newEquipo.put(DATA_EQUIPO_CAPACIDAD_COL_NAME, equipoData.getCapacidad());
			newEquipo.put(DATA_EQUIPO_POTENCIA_COL_NAME, equipoData.getPotencia());
			newEquipo.put(DATA_EQUIPO_FECHA_CERTIFICACION_COL_NAME, equipoData.getFechaCertificacion());
			
			Log.d(LOGTAG, "===============================================================================");
			Log.d(LOGTAG, DATA_EQUIPO_ID_EQUIPO_COL_NAME + ": " + equipoData.getIdEquipo());
			Log.d(LOGTAG, DATA_EQUIPO_MARCA_COL_NAME + ": " + equipoData.getMarca());
			Log.d(LOGTAG, DATA_EQUIPO_AÑO_COL_NAME + ": " + equipoData.getAno());
			Log.d(LOGTAG, DATA_EQUIPO_PLACA_PATENTE_COL_NAME + ": " + equipoData.getPlacaPatente());
			Log.d(LOGTAG, DATA_EQUIPO_CODIGO_INTERNO_COL_NAME + ": " + equipoData.getCodigoInterno());
			Log.d(LOGTAG, "===============================================================================");
			
			
			Cursor rslt = mDb.query(CENTER_PAUTA_DATA_EQUIPO_TABLE, 
					new String[]{DATA_EQUIPO_ID_EQUIPO_COL_NAME}, 
					DATA_EQUIPO_ID_EQUIPO_COL_NAME + "=" + equipoData.getIdEquipo(), 
					null, null, null, null);
			
			long insertedEquipoIndex = -1;
			insertedEquipoIndex = mDb.insertWithOnConflict(CENTER_PAUTA_DATA_EQUIPO_TABLE, 
																    null, 
																    newEquipo, 
																    SQLiteDatabase.CONFLICT_REPLACE);
			
			rslt.close();
			
				Log.d(LOGTAG, "Agregado Equipo N� " + insertedEquipoIndex + ", " + equipoData.getIdEquipo());
			
			return insertedEquipoIndex;
			
		}
		
		public long insertCertificacionEquipoData(CertificacionEquipoData certificacionEquipoData){
			
			ContentValues newCertificacionEquipoData = new ContentValues();
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_ID_CERTIFICACION_EQUIPO_COL_NAME, certificacionEquipoData.getIdCertificacionEquipo());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_ID_EQUIPO_CERTIFICAR_COL_NAME, certificacionEquipoData.getIdEquipoCertificar());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_ID_PAUTA_CERTIFICACION_COL_NAME, certificacionEquipoData.getIdPautaCertificacion());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_EMPRESA_RESPONSABLE_COL_NAME, certificacionEquipoData.getEmpresaResponsable());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_LUGAR_INSPECCION_COL_NAME, certificacionEquipoData.getLugarInspeccion());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_NPPA_COL_NAME, certificacionEquipoData.getNppa());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_COTIZACION_COL_NAME, certificacionEquipoData.getCotizacion());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_ITEM_COL_NAME, certificacionEquipoData.getItem());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_HOROMETRO_COL_NAME, certificacionEquipoData.getHorometro());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_KILOMETRAJE_COL_NAME, certificacionEquipoData.getKilometraje());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_TIPO_CERTIFICACION_COL_NAME, certificacionEquipoData.getTipoCertificacion());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_FECHA_ENTREGA_DOCUMENTACION_COL_NAME, certificacionEquipoData.getFechaEntregaDocumentacion());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_FECHA_APROBACION_COL_NAME, certificacionEquipoData.getFechaAprobacion());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_OPERA_COL_NAME, certificacionEquipoData.getEquipoOpera());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_FECHA_VIGENCIA_COL_NAME, certificacionEquipoData.getFechaVigencia());
			newCertificacionEquipoData.put(DATA_CERTIFICACION_EQUIPO_TIPO_ACCION_COL_NAME, certificacionEquipoData.getTipoAccion());
			
			Cursor rslt = mDb.query(CENTER_PAUTA_DATA_CERTIFICACION_EQUIPO_TABLE, 
					new String[]{DATA_CERTIFICACION_EQUIPO_ID_CERTIFICACION_EQUIPO_COL_NAME}, 
					DATA_CERTIFICACION_EQUIPO_ID_CERTIFICACION_EQUIPO_COL_NAME + "=" + certificacionEquipoData.getIdCertificacionEquipo(), 
					null, null, null, null);
			
			long insertedCertificacionEquipoDataIndex = -1;
			insertedCertificacionEquipoDataIndex = mDb.insertWithOnConflict(CENTER_PAUTA_DATA_CERTIFICACION_EQUIPO_TABLE, 
																    null, 
																    newCertificacionEquipoData, 
																    SQLiteDatabase.CONFLICT_REPLACE);
			
			rslt.close();
			
				Log.d(LOGTAG, "Agregado CertificacionEquipoData N� " + insertedCertificacionEquipoDataIndex + ", " + certificacionEquipoData.getIdCertificacionEquipo());
			
			return insertedCertificacionEquipoDataIndex;
			
		}
		
		public long insertCertificacionEmpleadoData(CertificacionEmpleadoData certificacionEmpleadoData){
			
			
			ContentValues newCertificacionEmpleadoData = new ContentValues();
			newCertificacionEmpleadoData.put(DATA_CERTIFICACION_EMPLEADO_ID_CERTIFICACION_EMPLEADO_COL_NAME, 	certificacionEmpleadoData.getIdCertificacionEmpleado());
			newCertificacionEmpleadoData.put(DATA_CERTIFICACION_EMPLEADO_ID_PAUTA_CERTIFICACION_COL_NAME, 		certificacionEmpleadoData.getIdPautaCertificacion());
			newCertificacionEmpleadoData.put(DATA_CERTIFICACION_EMPLEADO_ID_EMPLEADO_COL_NAME, 					certificacionEmpleadoData.getIdEmpleado());
			newCertificacionEmpleadoData.put(DATA_CERTIFICACION_EMPLEADO_EQUIPO_ASOCIADO_COL_NAME,				certificacionEmpleadoData.getEquipoAsociado());
			newCertificacionEmpleadoData.put(DATA_CERTIFICACION_EMPLEADO_EQUIPO_ASOCIADO_TONELADA_COL_NAME, 	certificacionEmpleadoData.getEquipoAsociadoTonelada());
			newCertificacionEmpleadoData.put(DATA_CERTIFICACION_EMPLEADO_LUGAR_EVALUACION_COL_NAME, 			certificacionEmpleadoData.getLugarEvaluacion());
			newCertificacionEmpleadoData.put(DATA_CERTIFICACION_EMPLEADO_NUMERO_CONSTANCIA_COL_NAME, 			certificacionEmpleadoData.getNumeroConstancia());
			newCertificacionEmpleadoData.put(DATA_CERTIFICACION_EMPLEADO_NUMERO_NIVEL_APROBACION_COL_NAME, 		certificacionEmpleadoData.getNumeroNivelAprobacion());
			newCertificacionEmpleadoData.put(DATA_CERTIFICACION_EMPLEADO_FECHA_COL_NAME, 						certificacionEmpleadoData.getFecha());
			newCertificacionEmpleadoData.put(DATA_CERTIFICACION_EMPLEADO_FECHA_CERTIFICACION_COL_NAME, 			certificacionEmpleadoData.getFechaCertificacion());
			newCertificacionEmpleadoData.put(DATA_CERTIFICACION_EMPLEADO_FECHA_VIGENCIA_COL_NAME, 				certificacionEmpleadoData.getFechaVigencia());
			newCertificacionEmpleadoData.put(DATA_CERTIFICACION_EMPLEADO_TIPO_ACCION_COL_NAME, 					certificacionEmpleadoData.getTipoAccion());
			
			
			Cursor rslt = mDb.query(CENTER_PAUTA_DATA_CERTIFICACION_EMPLEADO_TABLE, 
					new String[]{DATA_CERTIFICACION_EMPLEADO_ID_CERTIFICACION_EMPLEADO_COL_NAME}, 
					DATA_CERTIFICACION_EMPLEADO_ID_CERTIFICACION_EMPLEADO_COL_NAME + "=" + certificacionEmpleadoData.getIdCertificacionEmpleado(), 
					null, null, null, null);
			
			long insertedCertificacionEmpleadoDataIndex = -1;
			insertedCertificacionEmpleadoDataIndex = mDb.insertWithOnConflict(CENTER_PAUTA_DATA_CERTIFICACION_EMPLEADO_TABLE, 
																    null, 
																    newCertificacionEmpleadoData, 
																    SQLiteDatabase.CONFLICT_REPLACE);
			
			rslt.close();
			
				Log.d(LOGTAG, "Agregado CertificacionEquipoData N� " + insertedCertificacionEmpleadoDataIndex + ", " + certificacionEmpleadoData.getIdCertificacionEmpleado());
			
			return insertedCertificacionEmpleadoDataIndex;
			
		}

}
