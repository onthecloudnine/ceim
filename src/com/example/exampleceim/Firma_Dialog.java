package com.example.exampleceim;

import java.io.ByteArrayOutputStream;

import com.example.exampleceim.pautas_certificacion.Pauta_Empleado;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class Firma_Dialog extends Activity {

	public int tipo = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_firma__dialog);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.firma__dialog, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		static DrawingView dv;	
		Bitmap b;
		
		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			final View rootView = inflater.inflate(R.layout.fragment_firma__dialog,
					container, false);
			
			dv = new DrawingView(getActivity());
			dv.setDrawingCacheEnabled(true);
				
			Button btnGuardarFirma = (Button) rootView.findViewById(R.id.btnGuardar);
			btnGuardarFirma.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
				
					try{
					
						b = loadBitmapFromView(dv);
						
						Pauta_Empleado.bFirmaOperador = b;
//						Pauta_Empleado.imgFirmaOperador.setImageBitmap(b);
						
//						Log.d("", String.valueOf(b.getWidth()));
						Log.d("Guardando firma como Bitmap", b.toString());
						
					}catch(Exception e){
						e.printStackTrace();
					}finally{
						
						Intent resultIntent = new Intent();
						
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						b.compress(Bitmap.CompressFormat.PNG, 100, stream);
						byte[] byteArray = stream.toByteArray();
						
						resultIntent.putExtra("ComingFrom", byteArray);
						getActivity().setResult(Activity.RESULT_OK, resultIntent);
						getActivity().finish();
						
//					    Log.d("", String.valueOf(b.getWidth()));
						
					}
					
				}
			});
			
			final RelativeLayout ll = (RelativeLayout) rootView.findViewById(R.id.layout_firmaCertificador);
			
			 dv.mPaint = new Paint();
	            
			 dv.mPaint.setAntiAlias(true);
			 dv.mPaint.setDither(true);
			 dv.mPaint.setColor(Color.BLACK);
			 dv.mPaint.setStyle(Paint.Style.STROKE);
			 dv.mPaint.setStrokeJoin(Paint.Join.ROUND);
			 dv.mPaint.setStrokeCap(Paint.Cap.ROUND);
			 dv.mPaint.setStrokeWidth(6);
			 
           ll.addView(dv);
           
			
			Button btnLimpiarFirma = (Button) rootView.findViewById(R.id.btnLimpiarFirma);
			btnLimpiarFirma.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					Log.d("####", "Limpiando Firma ...");
					
					 dv.clearCanvas();
				}
			}); 
			
			
            
			return rootView;
		}
		
		@Override
		public void onStart() {
			super.onStart();
			
		}
		
		public static Bitmap loadBitmapFromView(View v) {
			
			Log.d("", String.valueOf(v.getMeasuredWidth()));
			Log.d("", String.valueOf(v.getMeasuredHeight()));
			Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth() , v.getMeasuredHeight(), Bitmap.Config.ARGB_8888); 
			
			Canvas c = new Canvas(b);
			
			v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
			
			v.draw(c);
			
			return b;
		}
		
	}

}
