package com.example.exampleceim.usuarios;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.example.exampleceim.BaseParser;
import com.example.exampleceim.DatabaseHandler;

public class UsuariosParser extends BaseParser{
	
	public static final String LOGTAG = UsuariosParser.class.getName() + " LOG_TAG";
	
	/***  Json keys ***/
	private final static String JSON_KEY_USUARIO="Usuario";
	
	private final static String JSON_KEY_IDUSUARIO="IdUsuario";
	private final static String JSON_KEY_NOMBREUSUARIO="NombreUsuario";
	private final static String JSON_KEY_PASSWORD="Password";
	private final static String JSON_KEY_SALT="Salt";

	private final static String JSON_KEY_IDPERSONA="IdPersona";
	private final static String JSON_KEY_IDCENTROCERTIFICADOR="IdCentroCertificador";
	private final static String JSON_KEY_TIPOACCION = "TipoAccion";
	private final static String JSON_KEY_RUN="RUN";
	private final static String JSON_KEY_DIGITOVERIFICADOR="DigitoVerificador";
	private final static String JSON_KEY_NOMBRES="Nombres";
	private final static String JSON_KEY_PRIMERAPELLIDO="PrimerApellido";
	private final static String JSON_KEY_SEGUNDOAPELLIDO="SegundoApellido";
	private final static String JSON_KEY_TELEFONO="Telefono";
	private final static String JSON_KEY_CELULAR = "Celular";
	
	private final static String JSON_KEY_$ID="$id";
	
	/*
	 * 	 private int IdPersona;  
		 private int IdCentroCertificador;
		 private int IdUsuario;
		 private int TipoAccion;
		 private int $id;
	     private int RUN;
	     private String DigitoVerificador;
	     private String Nombres;
	     private String PrimerApellido;
	     private String SegundoApellido;
	     private String Telefono;
	     private String Celular;
	 */
	
	
	public void guardarDatosUsuario(JSONObject responseJson, Context context, DatabaseHandler db){
		
		try{
			db.open();
			
			JSONArray jValues = getJSONArray(responseJson, "$values");
			if( jValues.length() <= 0 ){
				
				Log.e( LOGTAG, " No se ha encontrado información ");
				
			}else{
				for(int i = 0; i < jValues.length(); i++){
					
					JSONObject jItems = jValues.getJSONObject(i);
					
					/*****   Datos Usuario  *****/
					JSONObject jUsuario = getJSONObject(jItems, JSON_KEY_USUARIO);
					Usuario usuario;
					usuario = new Usuario(getInt(jUsuario, JSON_KEY_IDUSUARIO), getString(jUsuario, JSON_KEY_NOMBREUSUARIO),
							getString(jUsuario, JSON_KEY_PASSWORD), getString(jUsuario, JSON_KEY_SALT), getInt(jUsuario, JSON_KEY_$ID));
					
					db.insertUsuarios(usuario);
					
					/*****  Datos Persona *****/
					Persona persona;
					
					persona = new Persona(getInt(jItems, JSON_KEY_IDPERSONA), 
										  getInt(jItems, JSON_KEY_IDCENTROCERTIFICADOR), 
										  getInt(jItems, JSON_KEY_IDUSUARIO), 
										  getInt(jItems, JSON_KEY_TIPOACCION), 
										  getInt(jItems, JSON_KEY_$ID), 
										  getInt(jItems, JSON_KEY_RUN), 
										  getString(jItems, JSON_KEY_DIGITOVERIFICADOR), 
										  getString(jItems, JSON_KEY_NOMBRES), 
										  getString(jItems, JSON_KEY_PRIMERAPELLIDO), 
										  getString(jItems, JSON_KEY_SEGUNDOAPELLIDO), 
										  getString(jItems, JSON_KEY_TELEFONO), 
										  getString(jItems, JSON_KEY_CELULAR));
					
					db.insertPersona(persona);
				}
				
				db.insertFecha("Usuario");
			}
			db.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sincronizarUsuarios(JSONObject responseJson, Context context, DatabaseHandler db){
		
		try{
			db.open();
			
			JSONArray jValues = getJSONArray(responseJson, "$values");
			if( jValues.length() <= 0 ){
				
				Log.e( LOGTAG, " No se ha encontrado información ");
				
			}else{
				for(int i = 0; i < jValues.length(); i++){
					
					JSONObject jItems = jValues.getJSONObject(i);
					
					int tipoAccion = getInt(jItems, JSON_KEY_TIPOACCION);
					Usuario usuario;
					Persona persona;
					JSONObject jUsuario = getJSONObject(jItems, JSON_KEY_USUARIO);
					
					if(tipoAccion == 4){
						
						
					
						usuario = new Usuario(getInt(jUsuario, JSON_KEY_IDUSUARIO), getString(jUsuario, JSON_KEY_NOMBREUSUARIO),
								getString(jUsuario, JSON_KEY_PASSWORD), getString(jUsuario, JSON_KEY_SALT), getInt(jUsuario, JSON_KEY_$ID));
						
						db.insertUsuarios(usuario);
						
						persona = new Persona(getInt(jItems, JSON_KEY_IDPERSONA), 
											  getInt(jItems, JSON_KEY_IDCENTROCERTIFICADOR), 
											  getInt(jItems, JSON_KEY_IDUSUARIO), 
											  getInt(jItems, JSON_KEY_TIPOACCION), 
											  getInt(jItems, JSON_KEY_$ID), 
											  getInt(jItems, JSON_KEY_RUN), 
											  getString(jItems, JSON_KEY_DIGITOVERIFICADOR), 
											  getString(jItems, JSON_KEY_NOMBRES), 
											  getString(jItems, JSON_KEY_PRIMERAPELLIDO), 
											  getString(jItems, JSON_KEY_SEGUNDOAPELLIDO), 
											  getString(jItems, JSON_KEY_TELEFONO), 
											  getString(jItems, JSON_KEY_CELULAR));
						db.insertPersona(persona);
						
					}else if(tipoAccion == 3){
						
						usuario = new Usuario(getInt(jUsuario, JSON_KEY_IDUSUARIO), getString(jUsuario, JSON_KEY_NOMBREUSUARIO),
								getString(jUsuario, JSON_KEY_PASSWORD), getString(jUsuario, JSON_KEY_SALT), getInt(jUsuario, JSON_KEY_$ID));
						
						
						persona = new Persona(getInt(jItems, JSON_KEY_IDPERSONA), 
								  getInt(jItems, JSON_KEY_IDCENTROCERTIFICADOR), 
								  getInt(jItems, JSON_KEY_IDUSUARIO), 
								  getInt(jItems, JSON_KEY_TIPOACCION), 
								  getInt(jItems, JSON_KEY_$ID), 
								  getInt(jItems, JSON_KEY_RUN), 
								  getString(jItems, JSON_KEY_DIGITOVERIFICADOR), 
								  getString(jItems, JSON_KEY_NOMBRES), 
								  getString(jItems, JSON_KEY_PRIMERAPELLIDO), 
								  getString(jItems, JSON_KEY_SEGUNDOAPELLIDO), 
								  getString(jItems, JSON_KEY_TELEFONO), 
								  getString(jItems, JSON_KEY_CELULAR));
						
						db.deleteUsuario(usuario);
						db.deletePersona(persona);
					}
					
					
				}
			}
			db.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
}