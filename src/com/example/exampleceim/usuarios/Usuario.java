package com.example.exampleceim.usuarios;

public class Usuario extends UsuariosParser{
	
	 private int mIdUsuario;
     private String mNombreUsuario;
     private String mPassword;
     private String mSalt;
     private int $ID;
     
     
     public Usuario(int idUsuario, String nombreUsuario, String password, String salt, int _$id) {
 		
    	 setmIdUsuario(idUsuario);
    	 setmNombreUsuario(nombreUsuario);
    	 setmPassword(password);
    	 setmSalt(salt);
    	 set$ID(_$id);
 		
 	}
     
	public int getmIdUsuario() {
		return mIdUsuario;
	}
	public void setmIdUsuario(int mIdUsuario) {
		this.mIdUsuario = mIdUsuario;
	}



	public String getmNombreUsuario() {
		return mNombreUsuario;
	}



	public void setmNombreUsuario(String mNombreUsuario) {
		this.mNombreUsuario = mNombreUsuario;
	}


	public String getmPassword() {
		return mPassword;
	}



	public void setmPassword(String mPassword) {
		this.mPassword = mPassword;
	}



	public String getmSalt() {
		return mSalt;
	}



	public void setmSalt(String mSalt) {
		this.mSalt = mSalt;
	}

	public int get$ID() {
		return $ID;
	}

	public void set$ID(int $ID) {
		this.$ID = $ID;
	}
     
	
	
}