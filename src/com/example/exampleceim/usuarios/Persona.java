package com.example.exampleceim.usuarios;

public class Persona {
	
	 private int IdPersona;  
	 private int IdCentroCertificador;
	 private int IdUsuario;
	 private int TipoAccion;
	 private int $id;
     private int RUN;
     private String DigitoVerificador;
     private String Nombres;
     private String PrimerApellido;
     private String SegundoApellido;
     private String Telefono;
     private String Celular;
     
     /*
      * 	"RUN": 11328029,
            "IdCentroCertificador": 1,
            "Telefono": "5555",
            "DigitoVerificador": "8",
            "IdPersona": 5,
            "Nombres": "Jos� Juan",
            "PrimerApellido": "Perez",
            "TipoAccion": 4,
            "$id": "2",
            "Celular": "4444",
            "IdUsuario": 4,
            "SegundoApellido": "Soto"
      * 
      */

	public Persona(int idPersona, int idCentroCertificador, int idUsuario, int tipoAccion, int _$id, int run,
				   String digitoVerificador, String nombres, String primerApellido, String segundoApellido,
				   String telefono, String celular) {
		
		setIdPersona(idPersona);
		setIdCentroCertificador(idCentroCertificador);
		setIdUsuario(idUsuario);
		setTipoAccion(tipoAccion);
		set$id(_$id);
		setRUN(run);
		setDigitoVerificador(digitoVerificador);
		setNombres(nombres);
		setPrimerApellido(primerApellido);
		setSegundoApellido(segundoApellido);
		setTelefono(telefono);
		setCelular(celular);
		
	}

	public Persona() {
		// TODO Auto-generated constructor stub
	}

	public int getIdPersona() {
		return IdPersona;
	}

	public void setIdPersona(int idPersona) {
		IdPersona = idPersona;
	}

	public int getRUN() {
		return RUN;
	}

	public void setRUN(int rUN) {
		RUN = rUN;
	}

	public String getDigitoVerificador() {
		return DigitoVerificador;
	}

	public void setDigitoVerificador(String digitoVerificador) {
		DigitoVerificador = digitoVerificador;
	}

	public String getNombres() {
		return Nombres;
	}

	public void setNombres(String nombres) {
		Nombres = nombres;
	}

	public String getPrimerApellido() {
		return PrimerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		PrimerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return SegundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		SegundoApellido = segundoApellido;
	}

	public String getTelefono() {
		return Telefono;
	}

	public void setTelefono(String telefono) {
		Telefono = telefono;
	}

	public String getCelular() {
		return Celular;
	}

	public void setCelular(String celular) {
		Celular = celular;
	}

	public int getIdCentroCertificador() {
		return IdCentroCertificador;
	}

	public void setIdCentroCertificador(int idCentroCertificador) {
		IdCentroCertificador = idCentroCertificador;
	}

	public int getIdUsuario() {
		return IdUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		IdUsuario = idUsuario;
	}

	public int getTipoAccion() {
		return TipoAccion;
	}

	public void setTipoAccion(int tipoAccion) {
		TipoAccion = tipoAccion;
	}

	public int get$id() {
		return $id;
	}

	public void set$id(int $id) {
		this.$id = $id;
	}

}
