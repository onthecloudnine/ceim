package com.example.exampleceim.ItemsPauta;

import com.example.exampleceim.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Item_GruposPauta extends RelativeLayout{
	
	public TextView titulo;
	public LinearLayout content_ItemGrupoPauta;
	
	public Item_GruposPauta(Context context) {
		super(context);
		
		LayoutInflater inflater = (LayoutInflater)  this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 
		inflater.inflate(R.layout.item_grupospauta_view, this, true);
		
		titulo = (TextView) this.findViewById(R.id.txtNombreGrupoPauta);
		content_ItemGrupoPauta = (LinearLayout) this.findViewById(R.id.content_ItemGrupoPauta);
	}


}
