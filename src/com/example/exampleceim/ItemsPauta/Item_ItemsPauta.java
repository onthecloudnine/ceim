package com.example.exampleceim.ItemsPauta;

import com.example.exampleceim.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Item_ItemsPauta extends RelativeLayout{
	
	public TextView titulo;
	public RadioGroup radioGroup;
	public Item_ItemsPauta(Context context) {
		super(context);
		
		LayoutInflater inflater = (LayoutInflater)  this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 
		inflater.inflate(R.layout.item_itemspauta_view, this, true);
		
		titulo = (TextView) this.findViewById(R.id.txtItemPauta);
		radioGroup = (RadioGroup) this.findViewById(R.id.radioGroup1);
	}


}
