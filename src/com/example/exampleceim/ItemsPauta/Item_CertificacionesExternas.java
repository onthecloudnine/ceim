package com.example.exampleceim.ItemsPauta;

import com.example.exampleceim.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Item_CertificacionesExternas extends RelativeLayout{
	
	public TextView titulo;
	public EditText empresaCertificadora, numeroCertificado, fechaVencimiento, vencimientoKM;
	
	
	public Item_CertificacionesExternas(Context context) {
		super(context);
		
		LayoutInflater inflater = (LayoutInflater)  this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 
		inflater.inflate(R.layout.item_certificacionesextermas_view, this, true);
		
		titulo = (TextView) this.findViewById(R.id.txt_ItemDocumentoObligatorios);
		empresaCertificadora = (EditText) this.findViewById(R.id.eTxtEmpresaCertificadora);
		numeroCertificado = (EditText) this.findViewById(R.id.eTxt_numeroCertificado);
		fechaVencimiento = (EditText) this.findViewById(R.id.eTxt_fechaVencimiento);
		vencimientoKM = (EditText) this.findViewById(R.id.eTxtVencimientoKM);
	}


}
