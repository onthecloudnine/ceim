package com.example.exampleceim.ItemsPauta;

import com.example.exampleceim.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Item_DocumentosObligatorios extends RelativeLayout{
	
	public TextView titulo;
	public RadioButton rSi, rNo;
	public EditText fechaVencimiento;
	public RadioGroup radioGroup;
	
	public Item_DocumentosObligatorios(Context context) {
		super(context);
		
		LayoutInflater inflater = (LayoutInflater)  this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 
		inflater.inflate(R.layout.item_documentosobligatorios_view, this, true);
		
		titulo = (TextView) this.findViewById(R.id.txt_ItemDocumentoObligatorios);
		rSi = (RadioButton) this.findViewById(R.id.rSi);
		rNo = (RadioButton) this.findViewById(R.id.rNo);
		radioGroup = (RadioGroup) this.findViewById(R.id.radioGroup1);
		
		fechaVencimiento = (EditText) this.findViewById(R.id.eTxt_FechaVencimiento);
	}


}
