package com.example.exampleceim.pautas_certificacion;

public class TipoDocumentoObligatorioEquipo {
	
	 private int idTipoDocumentoObligatorioEquipo;
     private int idPautaCertificacion;
     private String nombre;
     private boolean incluyeFechaVencimiento;
     private int orden;
     private int tipoAccion;

     public TipoDocumentoObligatorioEquipo(int _idTipoDocumentoObligatorioEquipo, int _idPautaCertificacion, String _Nombre,
    		 							   boolean _incluyeFechaVencimiento, int _orden, int _tipoAccion){
    	 
    	 setIdTipoDocumentoObligatorioEquipo(_idTipoDocumentoObligatorioEquipo);
    	 setIdPautaCertificacion(_idPautaCertificacion);
    	 setNombre(_Nombre);
    	 setIncluyeFechaVencimiento(_incluyeFechaVencimiento);
    	 setOrden(_orden);
    	 setTipoAccion(_tipoAccion);
    	 
     }
     
	public TipoDocumentoObligatorioEquipo() {
		// TODO Auto-generated constructor stub
	}

	public int getIdTipoDocumentoObligatorioEquipo() {
		return idTipoDocumentoObligatorioEquipo;
	}
	public void setIdTipoDocumentoObligatorioEquipo(
			int idTipoDocumentoObligatorioEquipo) {
		this.idTipoDocumentoObligatorioEquipo = idTipoDocumentoObligatorioEquipo;
	}
	public int getIdPautaCertificacion() {
		return idPautaCertificacion;
	}
	public void setIdPautaCertificacion(int idPautaCertificacion) {
		this.idPautaCertificacion = idPautaCertificacion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isIncluyeFechaVencimiento() {
		return incluyeFechaVencimiento;
	}
	public void setIncluyeFechaVencimiento(boolean incluyeFechaVencimiento) {
		this.incluyeFechaVencimiento = incluyeFechaVencimiento;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	public int getTipoAccion() {
		return tipoAccion;
	}
	public void setTipoAccion(int tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

}
