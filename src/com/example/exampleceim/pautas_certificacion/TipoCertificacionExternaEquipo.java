package com.example.exampleceim.pautas_certificacion;

public class TipoCertificacionExternaEquipo {
	
	  private int idTipoCertificacionExternaEquipo;
      private int idPautaCertificacion;
      private String nombre;
      private int orden;
      private int tipoAccion;
      private int tipoVencimiento;
      
    public TipoCertificacionExternaEquipo(int _idTipoCertificacionExternaEquipo, int _idPautaCertificacion, String _Nombre, int _Orden, int _TipoAccion, int _TipoVencimiento){
    	
    	setIdTipoCertificacionExternaEquipo(_idTipoCertificacionExternaEquipo);
    	setIdPautaCertificacion(_idPautaCertificacion);
    	setNombre(_Nombre);
    	setOrden(_Orden);
    	setTipoAccion(_TipoAccion);
    	setTipoVencimiento(_TipoVencimiento);
    	
    }
      
      
	public TipoCertificacionExternaEquipo() {
		// TODO Auto-generated constructor stub
	}


	public int getIdTipoCertificacionExternaEquipo() {
		return idTipoCertificacionExternaEquipo;
	}
	public void setIdTipoCertificacionExternaEquipo(
			int idTipoCertificacionExternaEquipo) {
		this.idTipoCertificacionExternaEquipo = idTipoCertificacionExternaEquipo;
	}
	public int getIdPautaCertificacion() {
		return idPautaCertificacion;
	}
	public void setIdPautaCertificacion(int idPautaCertificacion) {
		this.idPautaCertificacion = idPautaCertificacion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	public int getTipoAccion() {
		return tipoAccion;
	}
	public void setTipoAccion(int tipoAccion) {
		this.tipoAccion = tipoAccion;
	}
	public int getTipoVencimiento() {
		return tipoVencimiento;
	}
	public void setTipoVencimiento(int tipoVencimiento) {
		this.tipoVencimiento = tipoVencimiento;
	}

}
