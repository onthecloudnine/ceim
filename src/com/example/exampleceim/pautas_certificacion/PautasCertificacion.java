package com.example.exampleceim.pautas_certificacion;

import java.util.List;

public class PautasCertificacion {
	
	private int idPautaCertificacion;
    private int idCentroCertificador;
    private String nombre;
    private String descripcion;
    private int tipoCertificacion;
    private int cantidadInstancias;
    private boolean incluyeObservacionPorPregunta;
    private boolean incluyeNoAplica;
    private String revision;
    private byte[] ImagenLogoCertificacion;
    private String vencimientoKmHm;
    private int tipoAccion;
    private boolean incluyeNumeroNivelAprobacion;
    private List<GrupoPauta> gruposPauta;
    private List<ItemPauta> itemsPauta;

    private List<TipoDocumentoObligatorioEquipo> documentosObligatorioEquipo;
    private List<TipoDocumentoObligatorioEmpleado> documentosObligatorioEmpleado;
    private List<TipoCertificacionExternaEquipo> certificacionesExternasEquipo;

	public PautasCertificacion(int _idPautaCertificacion, int _idCentroCertificador, String _Nombre, String _Descripcion, int _tipoCertificacion,
								int _cantidadInstancias, boolean _incluyeObservacionPorPregunta, boolean incluyeNoAplica,
								String _Revision, byte[] _ImagenLogoCertificacion, String _vencimientoKmHm, int _tipoAccion, boolean incluyeNumeroNivelAprobacion,
								List<GrupoPauta> _gruposPauta, List<ItemPauta> _itemsPauta, 
								List<TipoDocumentoObligatorioEquipo> _documentosObligatorioEquipo,
								List<TipoDocumentoObligatorioEmpleado> _documentosObligatorioEmpleado,
								List<TipoCertificacionExternaEquipo> _certificacionesExternasEquipo) {
		
		setIdPautaCertificacion(_idPautaCertificacion);
		setIdCentroCertificador(_idCentroCertificador);
		setNombre(_Nombre);
		setDescripcion(_Descripcion);
		setTipoCertificacion(_tipoCertificacion);
		setCantidadInstancias(_cantidadInstancias);
		setIncluyeObservacionPorPregunta(_incluyeObservacionPorPregunta);
		setIncluyeNoAplica(incluyeNoAplica);
		setRevision(_Revision);
		setImagenLogoCertificacion(_ImagenLogoCertificacion);
		setVencimientoKmHm(_vencimientoKmHm);
		setTipoAccion(_tipoAccion);
		setIncluyeNumeroNivelAprobacion(incluyeNumeroNivelAprobacion);
		setGruposPauta(_gruposPauta);
		setItemsPauta(_itemsPauta);
		setDocumentosObligatorioEquipo(_documentosObligatorioEquipo);
		setDocumentosObligatorioEmpleado(_documentosObligatorioEmpleado);
		setCertificacionesExternasEquipo(_certificacionesExternasEquipo);
		
		
	}

	public PautasCertificacion(int int1, int int2, String string,
			String string2, int int3, int int4, Boolean boolean1,
			Boolean boolean2, String string3, String string4, int int5,
			int int6, List<GrupoPauta> listGrupoPauta,
			List<ItemPauta> listItemPauta,
			List<TipoDocumentoObligatorioEquipo> listDocOblEquipo,
			List<TipoDocumentoObligatorioEmpleado> listDocOblEmpleado,
			List<TipoCertificacionExternaEquipo> listCertExternaEquipo) {
		// TODO Auto-generated constructor stub
	}

	public PautasCertificacion() {
		// TODO Auto-generated constructor stub
	}

	public int getIdPautaCertificacion() {
		return idPautaCertificacion;
	}

	public void setIdPautaCertificacion(int idPautaCertificacion) {
		this.idPautaCertificacion = idPautaCertificacion;
	}

	public int getIdCentroCertificador() {
		return idCentroCertificador;
	}

	public void setIdCentroCertificador(int idCentroCertificador) {
		this.idCentroCertificador = idCentroCertificador;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getCantidadInstancias() {
		return cantidadInstancias;
	}

	public void setCantidadInstancias(int cantidadInstancias) {
		this.cantidadInstancias = cantidadInstancias;
	}

	public boolean isIncluyeObservacionPorPregunta() {
		return incluyeObservacionPorPregunta;
	}

	public void setIncluyeObservacionPorPregunta(
			boolean incluyeObservacionPorPregunta) {
		this.incluyeObservacionPorPregunta = incluyeObservacionPorPregunta;
	}

	public boolean isIncluyeNoAplica() {
		return incluyeNoAplica;
	}

	public void setIncluyeNoAplica(boolean incluyeNoAplica) {
		this.incluyeNoAplica = incluyeNoAplica;
	}

	public String getRevision() {
		return revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public byte[] getImagenLogoCertificacion() {
		return ImagenLogoCertificacion;
	}

	public void setImagenLogoCertificacion(byte[] imagenLogoCertificacion) {
		ImagenLogoCertificacion = imagenLogoCertificacion;
	}

	public String getVencimientoKmHm() {
		return vencimientoKmHm;
	}

	public void setVencimientoKmHm(String _vencimientoKmHm) {
		this.vencimientoKmHm = _vencimientoKmHm;
	}

	public int getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(int tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

	public List<GrupoPauta> getGruposPauta() {
		return gruposPauta;
	}

	public void setGruposPauta(List<GrupoPauta> gruposPauta) {
		this.gruposPauta = gruposPauta;
	}

	public List<ItemPauta> getItemsPauta() {
		return itemsPauta;
	}

	public void setItemsPauta(List<ItemPauta> itemsPauta) {
		this.itemsPauta = itemsPauta;
	}

	public List<TipoDocumentoObligatorioEquipo> getDocumentosObligatorioEquipo() {
		return documentosObligatorioEquipo;
	}

	public void setDocumentosObligatorioEquipo(
			List<TipoDocumentoObligatorioEquipo> documentosObligatorioEquipo) {
		this.documentosObligatorioEquipo = documentosObligatorioEquipo;
	}

	public List<TipoDocumentoObligatorioEmpleado> getDocumentosObligatorioEmpleado() {
		return documentosObligatorioEmpleado;
	}

	public void setDocumentosObligatorioEmpleado(
			List<TipoDocumentoObligatorioEmpleado> documentosObligatorioEmpleado) {
		this.documentosObligatorioEmpleado = documentosObligatorioEmpleado;
	}

	public List<TipoCertificacionExternaEquipo> getCertificacionesExternasEquipo() {
		return certificacionesExternasEquipo;
	}

	public void setCertificacionesExternasEquipo(
			List<TipoCertificacionExternaEquipo> certificacionesExternasEquipo) {
		this.certificacionesExternasEquipo = certificacionesExternasEquipo;
	}

	public int getTipoCertificacion() {
		return tipoCertificacion;
	}

	public void setTipoCertificacion(int tipoCertificacion) {
		this.tipoCertificacion = tipoCertificacion;
	}

	public boolean isIncluyeNumeroNivelAprobacion() {
		return incluyeNumeroNivelAprobacion;
	}

	public void setIncluyeNumeroNivelAprobacion(boolean incluyeNumeroNivelAprobacion) {
		this.incluyeNumeroNivelAprobacion = incluyeNumeroNivelAprobacion;
	}

}
