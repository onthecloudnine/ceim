package com.example.exampleceim.pautas_certificacion;

public class TipoDocumentoObligatorioEmpleado {
	
	 private int idTipoDocumentoObligatorioEmpleado;
     private int idPautaCertificacion;
     private String nombre;
     private boolean incluyeFechaVencimiento;
     private int orden;
     private int tipoAccion;
     
     public TipoDocumentoObligatorioEmpleado(int _idTipoDocumentoObligatorioEmpleado, int _idPautaCertificacion, String _Nombre,
    		 								boolean _incluyeFechaVencimiento, int _Orden, int _TipoAccion){
    	 
    	 setIdTipoDocumentoObligatorioEmpleado(_idTipoDocumentoObligatorioEmpleado);
    	 setIdPautaCertificacion(_idPautaCertificacion);
    	 setNombre(_Nombre);
    	 setIncluyeFechaVencimiento(_incluyeFechaVencimiento);
    	 setOrden(_Orden);
    	 setTipoAccion(_TipoAccion);
    	 
     }
     
     
	public TipoDocumentoObligatorioEmpleado() {
		// TODO Auto-generated constructor stub
	}


	public int getIdTipoDocumentoObligatorioEmpleado() {
		return idTipoDocumentoObligatorioEmpleado;
	}
	public void setIdTipoDocumentoObligatorioEmpleado(
			int idTipoDocumentoObligatorioEmpleado) {
		this.idTipoDocumentoObligatorioEmpleado = idTipoDocumentoObligatorioEmpleado;
	}
	public int getIdPautaCertificacion() {
		return idPautaCertificacion;
	}
	public void setIdPautaCertificacion(int idPautaCertificacion) {
		this.idPautaCertificacion = idPautaCertificacion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public boolean isIncluyeFechaVencimiento() {
		return incluyeFechaVencimiento;
	}
	public void setIncluyeFechaVencimiento(boolean incluyeFechaVencimiento) {
		this.incluyeFechaVencimiento = incluyeFechaVencimiento;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	public int getTipoAccion() {
		return tipoAccion;
	}
	public void setTipoAccion(int tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

}
