package com.example.exampleceim.pautas_certificacion;

public class ItemPauta {
	
	 private int idItemPauta;
     private int idGrupoPauta;
     private int idPautaCertificacion;
     private String nombre;
     private int orden;
     private int tipoAccion;
     
     public ItemPauta(int _idItemPauta, int _idGrupoPauta, int _idPautaCertificacion, String _nombre, int _orden, int _tipoAccion){
		
    	 setIdItemPauta(_idItemPauta);
		setIdGrupoPauta(_idGrupoPauta);
		setIdPautaCertificacion(_idPautaCertificacion);
		setNombre(_nombre);
		setOrden(_orden);
		setTipoAccion(_tipoAccion);
    	 
     }
     
	public ItemPauta() {
		// TODO Auto-generated constructor stub
	}

	public int getIdItemPauta() {
		return idItemPauta;
	}
	public void setIdItemPauta(int idItemPauta) {
		this.idItemPauta = idItemPauta;
	}
	public int getIdGrupoPauta() {
		return idGrupoPauta;
	}
	public void setIdGrupoPauta(int idGrupoPauta) {
		this.idGrupoPauta = idGrupoPauta;
	}
	public int getIdPautaCertificacion() {
		return idPautaCertificacion;
	}
	public void setIdPautaCertificacion(int idPautaCertificacion) {
		this.idPautaCertificacion = idPautaCertificacion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	public int getTipoAccion() {
		return tipoAccion;
	}
	public void setTipoAccion(int tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

}
