package com.example.exampleceim.pautas_certificacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.exampleceim.DatabaseHandler;
import com.example.exampleceim.DrawingView;
import com.example.exampleceim.Firma_Dialog;
import com.example.exampleceim.FormUtility;
import com.example.exampleceim.R;
import com.example.exampleceim.Setup_Inicializacion;
import com.example.exampleceim.Agenda.AgendaData;
import com.example.exampleceim.ItemsPauta.Item_CertificacionesExternas;
import com.example.exampleceim.ItemsPauta.Item_DocumentosObligatorios;
import com.example.exampleceim.ItemsPauta.Item_GruposPauta;
import com.example.exampleceim.ItemsPauta.Item_ItemsPauta;

public class Pauta_Equipo extends Activity {
	
	DatabaseHandler db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pautas);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
			
			cargarLogo(getApplicationContext());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pautas, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	public void cargarLogo(Context context){
		
		db = new DatabaseHandler(context);
		db.open();
		
		Resources res = getResources();
		BitmapDrawable icon = new BitmapDrawable(res, db.getImgLogo());
		getActionBar().setIcon(icon);
		
		db.close();
		
	}
	
	@Override
	public void onBackPressed() {
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setTitle("Atenci�n");
	    builder.setIcon(R.drawable.fail);
	    builder.setMessage("�Desea salir sin guardar la Informaci�n?")
	           .setCancelable(false)
	           .setPositiveButton("Si", new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	            	   finish();
	               }
	           })
	           .setNegativeButton("No", new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	                    dialog.cancel();
	               }
	           });
	    AlertDialog alert = builder.create();
	    alert.show();

	}
	
	public static class PlaceholderFragment extends Fragment implements OnClickListener{
		
		
		
		DrawingView dv;	
		DatabaseHandler db;
		static final String KEY_TAG_RADIO_BUTTON = "TAG";
		ArrayList<HashMap<String, String>> radioButtonsList = new ArrayList<HashMap<String, String>>();
		
		View rootView;
		
		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			
			rootView = inflater.inflate(R.layout.fragment_pauta_equipo, container, false);
			
			Button btnFirma_PersonaResponsableEmpresa = (Button) rootView.findViewById(R.id.btnFirma_PersonaResponsableEmpresa);
			Button btnFirmaCertificador = (Button) rootView.findViewById(R.id.btnFirmaCertificador);
			
			btnFirma_PersonaResponsableEmpresa.setOnClickListener(this);
			btnFirmaCertificador.setOnClickListener(this);
			
			Button btnGuardar = (Button) rootView.findViewById(R.id.btnGuardar);
			btnGuardar.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					Setup_Inicializacion.showAlertDialogGuardar(getActivity(), "Guardar ...",
		                    "Guardar Informaci�n de la Pauta");
					
					FormUtility obtenerDatos = new FormUtility();
					ScrollView rootScroll = (ScrollView) rootView; 

					RelativeLayout layout = (RelativeLayout) rootScroll.getChildAt(0);
					
					//obtenerDatos.obtenerValores(layout);
					
					//recuperarDatos( ViewGroup grupo, JSONArray jsonArray )
					
					
					try {
							
					obtenerDatos.obtenerValores(layout); // primero
					
					
					obtenerDatos.getJSONEquipo(); //para obtener el form a enviar
					
					obtenerDatos.jsonFill.toString(); //para guardar el form a mostrar despues
					
					//obtenerDatos.recuperarDatos(layout, "json guardado en la tabla");
					
					String jsonEquipo = obtenerDatos.jsonFill.toString();
					String jsonFill = obtenerDatos.jsonFill.toString();
					
//					Log.d("jsonEquipo", jsonEquipo);
//					Log.d("jsonFill", jsonFill);
					
					db.open();
					
					Intent intent = getActivity().getIntent();
					int idPautaCertificacion = Integer.valueOf(intent.getStringExtra("IdPautaCertificacion"));
					
					AgendaData agendaData = new AgendaData();
					agendaData = new AgendaData(idPautaCertificacion, 
							jsonEquipo,
							jsonFill,
							"false");
					
					
					db.updateAgenda(agendaData);
					
//					db.close();
					
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			});
			
			Button btnFinalizar = (Button) rootView.findViewById(R.id.btnFinalizar);
			btnFinalizar.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					 builder.setTitle("Atenci�n");
					 builder.setIcon(R.drawable.fail);
					 builder.setMessage("Al finalizar la Pauta no podr� editar los datos...")
					           .setCancelable(false)
					           .setPositiveButton("Si", new DialogInterface.OnClickListener() {
					               public void onClick(DialogInterface dialog, int id) {

					            	   ScrollView rootScroll = (ScrollView) rootView; 

										RelativeLayout layout = (RelativeLayout) rootScroll.getChildAt(0);

										FormUtility validador = new FormUtility();
										
										if (!validador.validar(layout)) {

											Setup_Inicializacion.showAlertDialog(getActivity(), "Atenci�n", "Faltan datos por Completar ...", false);

										return;
										
										}else{
											
											validador.setEnabledAllFields(layout, false);
											Log.d("Validar", "Datos Validados");
										}
					               }
					           })
					           .setNegativeButton("No", new DialogInterface.OnClickListener() {
					               public void onClick(DialogInterface dialog, int id) {
					                    dialog.cancel();
					               }
					           });
					    AlertDialog alert = builder.create();
					    alert.show();

					
				}
			});
			
			return rootView;
		}
		
		
		@Override
		public void onStart() {
			super.onStart();
			
			try {
				obtenerDocumentosObligatorios();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		public void obtenerDocumentosObligatorios() throws JSONException{
			
			db = new DatabaseHandler(getActivity());
			db.open();
			
			Intent intent = getActivity().getIntent();

			
			int idPautaCertificacion = Integer.valueOf(intent.getStringExtra("IdPautaCertificacion"));
			String datos = intent.getStringExtra("recuperarDatos");
			Log.d("Log Agenda", "IdPautaCertificacion: " + idPautaCertificacion);
			
			ImageView imgLogoCertificacion = (ImageView) getActivity().findViewById(R.id.imgLogoCertificacion);
		
			List<PautasCertificacion> pautasCertificacions = db.getPautas(idPautaCertificacion);
			
			LinearLayout Layout_contentDocumentosObligatorios = (LinearLayout) getActivity().findViewById(R.id.contentDocumentosObligatorios);
			LinearLayout Layout_contentCertificacionesExternasObligatorios = (LinearLayout) getActivity().findViewById(R.id.contentCertificacionesExternasObligatorios);
			LinearLayout Layout_contentDynamic = (LinearLayout) getActivity().findViewById(R.id.contentDynamic);
			
			for(final PautasCertificacion pc : pautasCertificacions){
				
				TextView txtNombrePauta = (TextView) getActivity().findViewById(R.id.txtNombrePauta);
				txtNombrePauta.setText(pc.getNombre());
				
				getActivity().getActionBar().setTitle(pc.getNombre());
				
				TextView txtDescripcion = (TextView) getActivity().findViewById(R.id.txtDescripcion);
				txtDescripcion.setText(pc.getDescripcion());
				
				TextView txtRevision = (TextView) getActivity().findViewById(R.id.txtRevision);
				txtRevision.setText(pc.getRevision());
				
				//Carga logo certificacion
				byte[] imgLogoCertificacionByte = pc.getImagenLogoCertificacion();
				
				Bitmap bmpLogoCertificacionByte = BitmapFactory.decodeByteArray(imgLogoCertificacionByte, 0, imgLogoCertificacionByte.length);
				
				imgLogoCertificacion.setImageBitmap(bmpLogoCertificacionByte);
				
				List<TipoDocumentoObligatorioEquipo> list_documentoObligatorioEquipos = pc.getDocumentosObligatorioEquipo();
				
				for(TipoDocumentoObligatorioEquipo documentoObligatorioEquipo : list_documentoObligatorioEquipos){
					
					Log.d("log", documentoObligatorioEquipo.getNombre());
					
					HashMap<String, String> mapDocOblEquipo = new HashMap<String, String>();
					
					Item_DocumentosObligatorios documentosObligatorios_View = new Item_DocumentosObligatorios(getActivity());
					
					documentosObligatorios_View.titulo.setText(documentoObligatorioEquipo.getNombre());
					documentosObligatorios_View.radioGroup.setTag(documentoObligatorioEquipo.getIdTipoDocumentoObligatorioEquipo());
					boolean incluyeFechaVencimiento = documentoObligatorioEquipo.isIncluyeFechaVencimiento();
					
					if(incluyeFechaVencimiento == false){
					
						documentosObligatorios_View.fechaVencimiento.setVisibility(View.GONE);
						documentosObligatorios_View.fechaVencimiento.setTag(documentoObligatorioEquipo.getIdTipoDocumentoObligatorioEquipo());
						
					}
					
//					mapDocOblEquipo.put(key, value);
					
					radioButtonsList.add(mapDocOblEquipo);
					
					LinearLayout noticiasLayout = new LinearLayout(getActivity());
			 		noticiasLayout.setPadding(10, 0, 10, 10);
			 		
			 		Layout_contentDocumentosObligatorios.addView(noticiasLayout);
			 		
					noticiasLayout.addView(documentosObligatorios_View);
				}
				
				
				List<TipoCertificacionExternaEquipo> certificacionExternaEquiposList = pc.getCertificacionesExternasEquipo();
				
				for(TipoCertificacionExternaEquipo cee : certificacionExternaEquiposList){
					
					Log.d("TipoCertificacionExternaEquipo Nombre: ", cee.getNombre());
					
					Item_CertificacionesExternas certificacionesExternas_Items = new Item_CertificacionesExternas(getActivity());
					
//					empresaCertificadora = (EditText) this.findViewById(R.id.eTxtEmpresaCertificadora);
//					numeroCertificado = (EditText) this.findViewById(R.id.eTxt_numeroCertificado);
//					fechaVencimiento = (EditText) this.findViewById(R.id.eTxt_fechaVencimiento);
//					vencimientoKM = (EditText) this.findViewById(R.id.eTxtVencimientoKM);
					
					certificacionesExternas_Items.empresaCertificadora.setTag("Empresa Certificadora, id ItemPauta: "+cee.getIdTipoCertificacionExternaEquipo());
					certificacionesExternas_Items.numeroCertificado.setTag("Numero Certificado, id ItemPauta: "+cee.getIdTipoCertificacionExternaEquipo());
					certificacionesExternas_Items.fechaVencimiento.setTag("Fecha Vencimiento, id ItemPauta: "+cee.getIdTipoCertificacionExternaEquipo());
					certificacionesExternas_Items.vencimientoKM.setTag("Vencimiento (KM), id ItemPauta: "+cee.getIdTipoCertificacionExternaEquipo());
					certificacionesExternas_Items.titulo.setText(cee.getNombre());
					
					int tipoVencimiento = cee.getTipoVencimiento();
					
					if(tipoVencimiento == 1){
						
						
					}else{
						
						
					}
					
					LinearLayout certificacionesExternasLayoutItem = new LinearLayout(getActivity());
					certificacionesExternasLayoutItem.setPadding(10, 0, 10, 10);
			 		
			 		Layout_contentCertificacionesExternasObligatorios.addView(certificacionesExternasLayoutItem);
			 		
			 		certificacionesExternasLayoutItem.addView(certificacionesExternas_Items);
					
				}
				
				List<GrupoPauta> grupoPautasList = pc.getGruposPauta();
				
				for(GrupoPauta gp : grupoPautasList){
					
					Log.d("GrupoPauta Nombre: ", gp.getNombre());
					
					Item_GruposPauta gruposPauta_item = new Item_GruposPauta(getActivity());
					
					gruposPauta_item.titulo.setText(gp.getNombre());
					
					int idGrupoPauta = gp.getIdGrupoPauta();
					
					LinearLayout gruposPautaLayoutItem = new LinearLayout(getActivity());
					gruposPautaLayoutItem.setPadding(10, 0, 10, 10);
			 		
			 		List<ItemPauta> itemPautasList = pc.getItemsPauta();
			 		for(ItemPauta ip : itemPautasList){
			 			
			 			if(idGrupoPauta == ip.getIdGrupoPauta()){
			 				
			 				Item_ItemsPauta itemsPauta = new Item_ItemsPauta(getActivity());
			 				
			 				itemsPauta.radioGroup.setTag("itemPauta*"+ip.getIdItemPauta());
			 				itemsPauta.titulo.setText(ip.getNombre());
			 				
			 				LinearLayout itemPautaLayoutItem = new LinearLayout(getActivity());
			 				itemPautaLayoutItem.setPadding(10, 0, 10, 10);
			 				
			 				gruposPauta_item.content_ItemGrupoPauta.addView(itemsPauta);
			 				
			 			}
			 			
			 		}
			 		
			 		
			 		Layout_contentDynamic.addView(gruposPautaLayoutItem);
			 		
			 		gruposPautaLayoutItem.addView(gruposPauta_item);
					
				}
				
			}
		
			FormUtility obtenerDatos = new FormUtility();
			ScrollView rootScroll = (ScrollView) rootView; 
			RelativeLayout layout = (RelativeLayout) rootScroll.getChildAt(0);
			obtenerDatos.obtenerValores(layout);
			
			if(datos != null){
				
				Log.d("json a rellenar", datos);
				JSONArray json = new JSONArray(datos);
				obtenerDatos.recuperarDatos(layout, json);
				
			}
			
			
			
			db.close();
		}
		
//		public void cargarLogo(Context context){
//			
//			db = new DatabaseHandler(context);
//			db.open();
//			
//			ImageView imgLogo = (ImageView) getActivity().findViewById(R.id.imgLogo);
//			
//			Bitmap imgLogoBitmap = db.getImgLogo();
//			
//			imgLogo.setImageBitmap(imgLogoBitmap);
//			
//			db.close();
//			
//		}

		@Override
		public void onClick(View v) {
			
			Intent intent = new Intent(getActivity(), Firma_Dialog.class);
			intent.putExtra("Tag", v.getTag().toString());
			startActivity(intent);

			
		}
		
	}

}
