package com.example.exampleceim.pautas_certificacion;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

import com.example.exampleceim.DatabaseHandler;
import com.example.exampleceim.DrawingView;
import com.example.exampleceim.Firma_Dialog;
import com.example.exampleceim.FormUtility;
import com.example.exampleceim.Login_Usuario;
import com.example.exampleceim.R;
import com.example.exampleceim.Setup_Inicializacion;
import com.example.exampleceim.Agenda.Agenda;
import com.example.exampleceim.ItemsPauta.Item_DocumentosObligatorios;
import com.example.exampleceim.ItemsPauta.Item_GruposPauta;
import com.example.exampleceim.ItemsPauta.Item_ItemsPauta;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

public class Pauta_Empleado extends Activity {
	DatabaseHandler db;
	public static ImageView imgFirmaOperador;
	public static Bitmap bFirmaOperador;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pauta__empleado);
		
		
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pauta__empleado, menu);
		return true;
	}
	
	@Override
	public void onBackPressed() {
		
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setTitle("Atenci�n");
	    builder.setIcon(R.drawable.fail);
	    builder.setMessage("�Desea salir sin guardar la Informaci�n?")
	           .setCancelable(false)
	           .setPositiveButton("Si", new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	            	   finish();
	               }
	           })
	           .setNegativeButton("No", new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) {
	                    dialog.cancel();
	               }
	           });
	    AlertDialog alert = builder.create();
	    alert.show();

	}
	
	@Override
	protected void onStart() {
		super.onStart();
		Log.d("", "onStart");
		cargarLogo(getApplicationContext());
	}
	
	public void cargarLogo(Context context){
		
		db = new DatabaseHandler(context);
		db.open();
		
		Resources res = getResources();
		BitmapDrawable icon = new BitmapDrawable(res, db.getImgLogo());
		getActionBar().setIcon(icon);
		
		db.close();
		
	}
	
	public void onClickEditText(View v){
		Log.d("Pauta Empleado Textos", v.getTag().toString());
	} 
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment implements OnClickListener{

		DrawingView dv;	
		DatabaseHandler db;
		AsyncHttpClient client = new AsyncHttpClient();
		private static final int CAMERA_REQUEST = 1888; 
	    private ImageView imgFirmaOperador;
	    
	    public static Bitmap bFirmaOperador;
	    
	    FormUtility formUtility;
	    View rootView;
	    
		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			
			rootView = inflater.inflate(R.layout.fragment_pauta__empleado,
					container, false);
			
			Button btnFirmaOperador = (Button) rootView.findViewById(R.id.btnFirmaOperador);
			Button btnFirmaSupervisor = (Button) rootView.findViewById(R.id.btnFirmaSupervisor);
			Button btnFirmaCertificador = (Button) rootView.findViewById(R.id.btnFirmaCertificador);
			imgFirmaOperador = (ImageView) rootView.findViewById(R.id.imgFirmaOperador);
			
			btnFirmaOperador.setOnClickListener(this);
			btnFirmaSupervisor.setOnClickListener(this);
			btnFirmaCertificador.setOnClickListener(this);
			
			Button btnGuardar = (Button) rootView.findViewById(R.id.btnGuardar);
			btnGuardar.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {

					Setup_Inicializacion.showAlertDialogGuardar(getActivity(), "Guardar ...",
		                    "Guardar Informaci�n de la Pauta");
					
					FormUtility obtenerDatos = new FormUtility();
					ScrollView rootScroll = (ScrollView) rootView; 

					RelativeLayout layout = (RelativeLayout) rootScroll.getChildAt(0);
					//obtenerDatos.obtenerValores(layout);
					
					
				}
			});
			
			Button btnFinalizar = (Button) rootView.findViewById(R.id.btnFinalizar);
			btnFinalizar.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					 builder.setTitle("Atenci�n");
					 builder.setIcon(R.drawable.fail);
					 builder.setMessage("Al finalizar la Pauta no podr� editar los datos...")
					           .setCancelable(false)
					           .setPositiveButton("Si", new DialogInterface.OnClickListener() {
					               public void onClick(DialogInterface dialog, int id) {

					            	   ScrollView rootScroll = (ScrollView) rootView; 

										RelativeLayout layout = (RelativeLayout) rootScroll.getChildAt(0);

										FormUtility validador = new FormUtility();
										
										if (!validador.validar(layout)) {

											Setup_Inicializacion.showAlertDialog(getActivity(), "Atenci�n", "Faltan datos por Completar ...", false);

										return;
										
										}else{
											
											validador.setEnabledAllFields(layout, false);
											Log.d("Validar", "Datos Validados");
										}
					               }
					           })
					           .setNegativeButton("No", new DialogInterface.OnClickListener() {
					               public void onClick(DialogInterface dialog, int id) {
					                    dialog.cancel();
					               }
					           });
					    AlertDialog alert = builder.create();
					    alert.show();
					
				}
			});
			
//			this.imageView = (ImageView)rootView.findViewById(R.id.imageView1);
//	        Button photoButton = (Button) rootView.findViewById(R.id.btnFinalizar);
//	        photoButton.setOnClickListener(new View.OnClickListener() {
//
//	            @Override
//	            public void onClick(View v) {
//	                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
//	                startActivityForResult(cameraIntent, CAMERA_REQUEST); 
//	            }
//	        });

			
			
			return rootView;
		}
		
		@Override
		public void onStart() {
			super.onStart();
			
			obtenerDocumentosObligatorios();
//			Log.d("img Firma", bFirmaOperador.toString() );
//			cargarLogo(getActivity().getApplicationContext());
//			cargarDatos();
		}
		
		@Override
	    public void onActivityResult(int requestCode,int resultCode,Intent data){
	     super.onActivityResult(requestCode, resultCode, data);

	     if (requestCode == 1 && resultCode == RESULT_OK) {
	          
	        	 byte[] image_bytes = null;
	 		     image_bytes = data.getByteArrayExtra("ComingFrom");
	 		     Bitmap bmp = BitmapFactory.decodeByteArray(image_bytes, 0, image_bytes.length);
	 		     imgFirmaOperador.setScaleType(ScaleType.CENTER_CROP);
	 		     imgFirmaOperador.setImageBitmap(bmp);
	 		     
	     }
		     
	    }
		
//	    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//	    	switch(requestCode) {
//		    case (MY_CHILD_ACTIVITY) : {	
//		      if (resultCode == Activity.RESULT_OK && requestCode == Firma_Dialog) {
//		        // TODO Extract the data returned from the child Activity.
//		      }
//		      break;
//		      if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {  
//		            Bitmap photo = (Bitmap) data.getExtras().get("data"); 
//		            imageView.setImageBitmap(photo);
//		      }
//		      break;
//		    } 
//		  }
//	        
//	    }
		
		
		public void obtenerDocumentosObligatorios(){
			
			db = new DatabaseHandler(getActivity());
			db.open();
			
			Intent intent = getActivity().getIntent();

			int idPautaCertificacion = Integer.valueOf(intent.getStringExtra("IdPautaCertificacion"));
			//Log.d("Log Agenda", "IdPautaCertificacion: " + idPautaCertificacion);
			
			ImageView imgLogoCertificacion = (ImageView) getActivity().findViewById(R.id.imgLogoCertificacion);
		
			List<PautasCertificacion> pautasCertificacions = db.getPautas(idPautaCertificacion);
			
			LinearLayout Layout_contentDynamic = (LinearLayout) getActivity().findViewById(R.id.contentDynamic);
			
			for(final PautasCertificacion pc : pautasCertificacions){
				
				
				TextView txtNombrePauta = (TextView) getActivity().findViewById(R.id.txtNombrePauta);
				txtNombrePauta.setText(pc.getNombre());
				
				getActivity().getActionBar().setTitle(pc.getNombre());
				
				TextView txtDescripcion = (TextView) getActivity().findViewById(R.id.txtDescripcion);
				txtDescripcion.setText(pc.getDescripcion());
				
				TextView txtRevision = (TextView) getActivity().findViewById(R.id.txtRevision);
				txtRevision.setText(pc.getRevision());
				
				//Carga logo certificacion
				byte[] imgLogoCertificacionByte = pc.getImagenLogoCertificacion();
				
				Bitmap bmpLogoCertificacionByte = BitmapFactory.decodeByteArray(imgLogoCertificacionByte, 0, imgLogoCertificacionByte.length);
				
				imgLogoCertificacion.setImageBitmap(bmpLogoCertificacionByte);
				
				
				List<GrupoPauta> grupoPautasList = pc.getGruposPauta();
				
				for(GrupoPauta gp : grupoPautasList){
					
					Log.d("GrupoPauta Nombre: ", gp.getNombre());
					
					Item_GruposPauta gruposPauta_item = new Item_GruposPauta(getActivity());
					
					gruposPauta_item.titulo.setText(gp.getNombre());
					
					int idGrupoPauta = gp.getIdGrupoPauta();
					
					LinearLayout gruposPautaLayoutItem = new LinearLayout(getActivity());
					gruposPautaLayoutItem.setPadding(10, 0, 10, 10);
			 		
			 		List<ItemPauta> itemPautasList = pc.getItemsPauta();
			 		for(ItemPauta ip : itemPautasList){
			 			
			 			if(idGrupoPauta == ip.getIdGrupoPauta()){
			 				
			 				Item_ItemsPauta itemsPauta = new Item_ItemsPauta(getActivity());
			 				
			 				itemsPauta.titulo.setText(ip.getNombre());
			 				itemsPauta.radioGroup.setTag(ip.getIdItemPauta());
			 				LinearLayout itemPautaLayoutItem = new LinearLayout(getActivity());
			 				itemPautaLayoutItem.setPadding(10, 0, 10, 10);
			 				
			 				gruposPauta_item.content_ItemGrupoPauta.addView(itemsPauta);
			 				
			 			}
			 			
			 		}
			 		
			 		
			 		Layout_contentDynamic.addView(gruposPautaLayoutItem);
			 		
			 		gruposPautaLayoutItem.addView(gruposPauta_item);
					
				}
				
				
			}

			LinearLayout Layout_contentDocumentosObligatorios = (LinearLayout) getActivity().findViewById(R.id.contentDocumentosObligatorios);
			
			List<TipoDocumentoObligatorioEmpleado> documentoObligatorioEmpleados = db.getDocumentoObligatorioEmpleado(idPautaCertificacion);
			
			Log.i("Log Pauta Empleado", "Tama�o List TipoDocumentoObligatorioEmpleado: " + documentoObligatorioEmpleados.size());
			
			for(TipoDocumentoObligatorioEmpleado td : documentoObligatorioEmpleados){
			
				Log.d("log", td.getNombre());
				
				Item_DocumentosObligatorios documentosObligatorios_View = new Item_DocumentosObligatorios(getActivity());
				
				documentosObligatorios_View.titulo.setText(td.getNombre());
				documentosObligatorios_View.radioGroup.setTag(td.getIdTipoDocumentoObligatorioEmpleado());
				
				if(td.isIncluyeFechaVencimiento() == false){
					documentosObligatorios_View.fechaVencimiento.setVisibility(View.GONE);
					documentosObligatorios_View.fechaVencimiento.setTag(td.getIdTipoDocumentoObligatorioEmpleado());
				}
				
				LinearLayout noticiasLayout = new LinearLayout(getActivity());
		 		noticiasLayout.setPadding(10, 0, 10, 10);
		 		
		 		Layout_contentDocumentosObligatorios.addView(noticiasLayout);
		 		
				noticiasLayout.addView(documentosObligatorios_View);
				
			}
			
			
			db.close();
		}
		
		void cargarDatos(){
			//Converting to dip unit
	        final int dip = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
	                (float) 1, getResources().getDisplayMetrics());
	        
	        Intent intent = getActivity().getIntent();
			String user = intent.getStringExtra("user");
			String pass = intent.getStringExtra("pass");
	        client.setBasicAuth(user, pass);
			//Para distinguir el centro certificador, se utiliza: CodeCertificatorCenter / 5HwMQ0O4�c6UzrwAlqYgYLSz=
			client.addHeader("CodeCertificatorCenter", "5HwMQ0O4�c6UzrwAlqYgYLSz=");
			client.get(getActivity().getApplicationContext(), Login_Usuario.urlServicioAgenda, new JsonHttpResponseHandler(){
				@Override
				public void onSuccess(int statusCode, JSONObject response) {
					super.onSuccess(statusCode, response);
					
					//Log.i("Json response", response.toString());
					
					 try {
						 
						 JSONArray jAgendaArray = response.getJSONArray("$values");
							
							for (int i = 0; i < jAgendaArray.length(); i++){
								
								
								JSONObject jItemAgenda = jAgendaArray.getJSONObject(i);
								
								
								String fecha = Agenda.stringFrom("T", jItemAgenda.getString("FechaProgramacion"));

//								map.put(KEY_FECHA, fecha);
//								map.put(KEY_TIPO_CERTIFICACION, jItemAgenda.getString("TipoCertificacion"));
//		        				map.put(KEY_EMPRESA, "Empresa");
//		        				map.put(KEY_PAUTA, jItemAgenda.getString("IdPautaCertificacion"));
//		        				
		        				if(jItemAgenda.getString("DataCertificacionEmpleado").equals("") || jItemAgenda.getString("DataCertificacionEmpleado").equals("null") ){
		        					
		        					//Log.e("Log Agenda", "No hay Datos DataCertificacionEmpleado");
		        					
		        				}else{
		        					
		        					JSONObject jObjectDataCertEmpleado = jItemAgenda.getJSONObject("DataCertificacionEmpleado");
			        				
		        					JSONObject jObjectEmpleado = jObjectDataCertEmpleado.getJSONObject("Empleado");
		        					
		        					String Nombre = jObjectEmpleado.getString("Nombres");
		        					String Apellido = jObjectEmpleado.getString("PrimerApellido");
		        					String Apellido2 = jObjectEmpleado.getString("SegundoApellido");
		        					
		        					String Nombres = Nombre + " " + Apellido + " " + Apellido2;
		        					
		        						TextView txtNombre = (TextView) getActivity().findViewById(R.id.txtNombre);
		        						txtNombre.setText(Nombres);
		        					
		        					String RUN = jObjectEmpleado.getString("RUN");
		        					String digitoVerificador =  jObjectEmpleado.getString("DigitoVerificador");
		        						
				        			String Rut = RUN + "-" + digitoVerificador;	
				        				
				        				TextView txtRUN = (TextView) getActivity().findViewById(R.id.txtRut);
				        				txtRUN.setText(Rut);
				        			
				        			String celular = jObjectEmpleado.getString("Celular");
				        			
				        				TextView txtCelular = (TextView) getActivity().findViewById(R.id.txtCelular);
				        				txtCelular.setText(celular);
//				        				map.put(KEY_EQUIPO, Empleado);
		        				}
		        				
		        				
		        				//JSONObject jItemsEmpleado = jObjectDataCertEmpleado.get
		        				
//		        				DataCertificacionEmpleado": {
//		        			      "Empleado": {
//		        			        "IdEmpleado": 1,
//		        			        "RUN": 16239404,
//		        			        "DigitoVerificador": "5",
//		        			        "Nombres": "ALEXIS",
//		        			        "PrimerApellido": "CEA",
//		        			        "SegundoApellido": "ROJAS",
//		        			        "Celular": "5555632",
//		        			        "Telefono": null,
//		        			        "FechaCertificacion": null,
//		        			        "TipoAccion": 4
//		        			      },
//		        			      "CertificacionEmpleado": {
//		        			        "IdCertificacionEmpleado": 0,
//		        			        "IdPautaCertificacion": 0,
//		        			        "IdEmpleado": 0,
//		        			        "EquipoAsociado": null,
//		        			        "EquipoAsociadoTonelada": 0,
//		        			        "LugarEvaluacion": null,
//		        			        "NumeroConstancia": 0,
//		        			        "Fecha": "0001-01-01T00:00:00",
//		        			        "NumeroNivelAprobacion": null,
//		        			        "FechaVigencia": null,
//		        			        "TipoAccion": 0
//		        			      },
//		        			      "NumeroInstancia": 0,
//		        			      "InstanciaCertificacionEmpleado": []
//		        			    }
		        				
		        				
		        				
							}
							
							
//					            File myFile = new File("/sdcard/AgendaJson.txt");
//					            myFile.createNewFile();
//					            FileOutputStream fOut = new FileOutputStream(myFile);
//					            OutputStreamWriter myOutWriter =new OutputStreamWriter(fOut);
//					            myOutWriter.append(response.toString());
//					            myOutWriter.close();
//					            fOut.close();
				            
//					            Log.i(TAG_APP, "Done writing SD");
						 }
				        catch (Exception e) 
				        {
				        	
				        	e.printStackTrace();
				           // Toast.makeText(getApplicationContext(), e.getMessage(),Toast.LENGTH_SHORT).show();
				        }
					
					
				}
				
				@Override
				public void onProgress(final int bytesWritten, final int totalSize) {
					super.onProgress(bytesWritten, totalSize);
					
					
				}
				
				@Override
				public void onFailure(Throwable e, JSONObject errorResponse) {
					super.onFailure(e, errorResponse);
					
					Log.e("Log Pauta Empleado " , "Error en la respuesta del servicio desde el servidor.");
					e.printStackTrace();
					
					//Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
				}
				
			});
			
			
		}
	
		@Override
		public void onClick(View v) {
			
			Intent intent = new Intent(getActivity(), Firma_Dialog.class);
			intent.putExtra("Tag", v.getTag().toString());
			startActivityForResult(intent, 1);

			
		}
		
	}

}
