package com.example.exampleceim.pautas_certificacion;

public class GrupoPauta {
	
	private int idGrupoPauta;
    private int idPautaCertificacion;
    private String nombre;
    private int orden;
    private int tipoAccion;
    
    public GrupoPauta(int _idGrupoPauta, int _idPautaCertificacion, String _nombre, int _orden, int _tipoAccion){
    	
    	setIdGrupoPauta(_idGrupoPauta);
    	setIdPautaCertificacion(_idPautaCertificacion);
    	setNombre(_nombre);
    	setOrden(_orden);
    	setTipoAccion(_tipoAccion);
    	
    }

	public GrupoPauta() {
		// TODO Auto-generated constructor stub
	}

	public int getIdGrupoPauta() {
		return idGrupoPauta;
	}

	public void setIdGrupoPauta(int idGrupoPauta) {
		this.idGrupoPauta = idGrupoPauta;
	}

	public int getIdPautaCertificacion() {
		return idPautaCertificacion;
	}

	public void setIdPautaCertificacion(int idPautaCertificacion) {
		this.idPautaCertificacion = idPautaCertificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	public int getTipoAccion() {
		return tipoAccion;
	}

	public void setTipoAccion(int tipoAccion) {
		this.tipoAccion = tipoAccion;
	}

}
