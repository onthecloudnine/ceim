package com.example.exampleceim.pautas_certificacion;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.example.exampleceim.BaseParser;
import com.example.exampleceim.DatabaseHandler;
import com.example.exampleceim.initializer.DataInitializer_Parser;

public class PautasParser extends BaseParser{
	
	public static final String LOGTAG = PautasParser.class.getName() + " LOG_TAG";
	
	/***  Json keys DATOS CENTRO CERTIFICADOR***/
	private final static String JSON_KEY_ID_PAUTACERTIFICACION				= "IdPautaCertificacion";
	private final static String JSON_KEY_ID_CENTROCERTIFICADOR 				= "IdCentroCertificador";
	private final static String JSON_KEY_NOMBRE 							= "Nombre";
	private final static String JSON_KEY_DESCRIPCION 						= "Descripcion";
	private final static String JSON_KEY_TIPOCERTIFICACION 					= "TipoCertificacion";
	private final static String JSON_KEY_CANTIDAD_INSTANCIAS 				= "CantidadInstancias";
	private final static String JSON_KEY_INCLUYE_OBSERVACION_POR_PREGUNTA 	= "IncluyeObservacionPorPregunta";
	private final static String JSON_KEY_INCLUYE_NO_APLICA 					= "IncluyeNoAplica";
	private final static String JSON_KEY_REVISION							= "Revision";
	private final static String JSON_KEY_IMG_LOGO_CERTIFICACION				= "ImagenLogoCertificacion";
	private final static String JSON_KEY_VENCIMIENTO_KMHM 					= "VencimientoKmHm";
	private final static String JSON_KEY_TIPO_ACCION	 					= "TipoAccion";
	private final static String JSON_KEY_IncluyeNumeroNivelAprobacion 		= "IncluyeNumeroNivelAprobacion";
	
	private final static String JSON_KEY_ID_GRUPO_PAUTA	 					= "IdGrupoPauta";
	private final static String JSON_KEY_ORDEN	 							= "Orden";
	
	private final static String JSON_KEY_ID_ITEM_PAUTA 						= "IdItemPauta";
	
	private final static String JSON_KEY_ID_TIPO_DOC_OBL_EQUIPO				= "IdTipoDocumentoObligatorioEquipo";
	private final static String JSON_KEY_INCLUYE_FECHA_VENCIMIENTO			= "IncluyeFechaVencimiento";
			
	private final static String JSON_KEY_ID_TIPO_CERT_EXT_EQUIPO			= "IdTipoCertificacionExternaEquipo";
	private final static String JSON_KEY_TIPO_VENCIMIENTO					= "TipoVencimiento";
	
	private final static String JSON_KEY_ID_TIPO_DOC_OBL_EMPLEADO			= "IdTipoDocumentoObligatorioEmpleado";
			
	
	
	public void guardarPautaCertificacion(JSONObject responseJson, Context context, DatabaseHandler db){
		
		try{
			db.open();
			
			JSONObject jObjetPautasCertificacion = responseJson.getJSONObject("PautasCertificacion");
			JSONArray jPautasCertificacion = getJSONArray(jObjetPautasCertificacion, "$values");
			if( jPautasCertificacion.length() <= 0 ){
				
				Log.e( LOGTAG, " No se ha encontrado información ");
				
			}else{
				for(int i = 0; i < jPautasCertificacion.length(); i++){
					
					PautasCertificacion pautasCertificacion;
					
					JSONObject jItems = jPautasCertificacion.getJSONObject(i);
					
//					{
//				          "IdGrupoPauta": 1,
//				          "IdPautaCertificacion": 2,
//				          "Nombre": "REQUISITOS BASICOS",
//				          "Orden": 1,
//				          "TipoAccion": 4
//				     }
					
					JSONObject jObjectGruposPauta = jItems.getJSONObject("GruposPauta");
					JSONArray jArrayGruposPauta = jObjectGruposPauta.getJSONArray("$values");
					
					List<GrupoPauta> listGrupoPauta = new ArrayList<GrupoPauta>();
					for(int g = 0; g < jArrayGruposPauta.length(); g++){
						
						JSONObject jGrupoPauta_Items = jArrayGruposPauta.getJSONObject(g);
						
						GrupoPauta grupoPauta = new GrupoPauta();
						grupoPauta.setIdGrupoPauta(getInt(jGrupoPauta_Items, JSON_KEY_ID_GRUPO_PAUTA));
						grupoPauta.setIdPautaCertificacion(getInt(jGrupoPauta_Items, JSON_KEY_ID_PAUTACERTIFICACION));
						grupoPauta.setNombre(getString(jGrupoPauta_Items, JSON_KEY_NOMBRE));
						grupoPauta.setOrden(getInt(jGrupoPauta_Items, JSON_KEY_ORDEN));
						grupoPauta.setTipoAccion(getInt(jGrupoPauta_Items, JSON_KEY_TIPO_ACCION));
						
						listGrupoPauta.add(grupoPauta);
						
					}
					
					JSONObject jObjectItemsPauta = jItems.getJSONObject("ItemsPauta");
					JSONArray jArrayItemsPauta = jObjectItemsPauta.getJSONArray("$values");
					
					List<ItemPauta> listItemPauta = new ArrayList<ItemPauta>();
					for(int g = 0; g < jArrayItemsPauta.length(); g++){
						
						JSONObject jItemsPauta = jArrayItemsPauta.getJSONObject(g);
						
						ItemPauta itemPauta = new ItemPauta();
						itemPauta.setIdItemPauta(getInt(jItemsPauta, JSON_KEY_ID_ITEM_PAUTA));
						itemPauta.setIdGrupoPauta(getInt(jItemsPauta, JSON_KEY_ID_GRUPO_PAUTA));
						itemPauta.setIdPautaCertificacion(getInt(jItemsPauta, JSON_KEY_ID_PAUTACERTIFICACION));
						itemPauta.setNombre(getString(jItemsPauta, JSON_KEY_NOMBRE));
						itemPauta.setOrden(getInt(jItemsPauta, JSON_KEY_ORDEN));
						itemPauta.setTipoAccion(getInt(jItemsPauta, JSON_KEY_TIPO_ACCION));
						
						
						listItemPauta.add(itemPauta);
						
					}
					
					List<TipoDocumentoObligatorioEquipo> listDocOblEquipo = new ArrayList<TipoDocumentoObligatorioEquipo>();
					
					if( jItems.getString("DocumentosObligatorioEquipo").equals("null")){
						
						Log.d(LOGTAG, "No hay datos en DocumentosObligatorioEquipo");
						
					}else{
						JSONObject jObjectDocumentosObligatorioEquipo = jItems.getJSONObject("DocumentosObligatorioEquipo");
						JSONArray jArrayDocOblEquipo = jObjectDocumentosObligatorioEquipo.getJSONArray("$values");
						for(int d = 0; d < jArrayDocOblEquipo.length(); d++){
							
							/*}
							 * {
						          "IdTipoDocumentoObligatorioEquipo": 1,
						          "IdPautaCertificacion": 2,
						          "Nombre": "Inscripción propietario",
						          "Orden": 1,
						          "IncluyeFechaVencimiento": false,
						          "TipoAccion": 4
						        }
							 *
							 */
							
							JSONObject jItemsDocOblEquipo = jArrayDocOblEquipo.getJSONObject(d);
							
							TipoDocumentoObligatorioEquipo documentoObligatorioEquipo = new TipoDocumentoObligatorioEquipo();

							documentoObligatorioEquipo.setIdTipoDocumentoObligatorioEquipo(getInt(jItemsDocOblEquipo, JSON_KEY_ID_TIPO_DOC_OBL_EQUIPO));
							documentoObligatorioEquipo.setIdPautaCertificacion(getInt(jItemsDocOblEquipo, JSON_KEY_ID_PAUTACERTIFICACION));
							documentoObligatorioEquipo.setNombre(getString(jItemsDocOblEquipo, JSON_KEY_NOMBRE));
							documentoObligatorioEquipo.setOrden(getInt(jItemsDocOblEquipo, JSON_KEY_ORDEN));
							documentoObligatorioEquipo.setIncluyeFechaVencimiento(getBoolean(jItemsDocOblEquipo, JSON_KEY_INCLUYE_FECHA_VENCIMIENTO));
							documentoObligatorioEquipo.setTipoAccion(getInt(jItemsDocOblEquipo, JSON_KEY_TIPO_ACCION));
							
							
							listDocOblEquipo.add(documentoObligatorioEquipo);
							
							
						}
					}
					
					List<TipoDocumentoObligatorioEmpleado> listDocOblEmpleado = new ArrayList<TipoDocumentoObligatorioEmpleado>();
					if( jItems.getString("DocumentosObligatorioEmpleado").equals("null")){
						
						Log.d(LOGTAG, "No hay datos en DocumentosObligatorioEmpleado");
						
					}else{
						
						Log.d(LOGTAG, "Ingresando DocumentosObligatorioEmpleado");
						
						JSONObject jsonDocumentosOblEmpleado = jItems.getJSONObject("DocumentosObligatorioEmpleado");
						JSONArray jArrayDocumentosOblEmpleado = jsonDocumentosOblEmpleado.getJSONArray("$values");
						for(int c = 0; c < jArrayDocumentosOblEmpleado.length(); c++){
							
							JSONObject jItemsDocOblEmpleado = jArrayDocumentosOblEmpleado.getJSONObject(c);
							
							TipoDocumentoObligatorioEmpleado documentoObligatorioEmpleado = new TipoDocumentoObligatorioEmpleado();
							
							documentoObligatorioEmpleado.setIdTipoDocumentoObligatorioEmpleado(getInt(jItemsDocOblEmpleado, JSON_KEY_ID_TIPO_DOC_OBL_EMPLEADO));
							documentoObligatorioEmpleado.setIdPautaCertificacion(getInt(jItemsDocOblEmpleado, JSON_KEY_ID_PAUTACERTIFICACION));
							documentoObligatorioEmpleado.setNombre(getString(jItemsDocOblEmpleado, JSON_KEY_NOMBRE));
							documentoObligatorioEmpleado.setIncluyeFechaVencimiento(getBoolean(jItemsDocOblEmpleado, JSON_KEY_INCLUYE_FECHA_VENCIMIENTO));
							documentoObligatorioEmpleado.setOrden(getInt(jItemsDocOblEmpleado, JSON_KEY_ORDEN));
							documentoObligatorioEmpleado.setTipoAccion(getInt(jItemsDocOblEmpleado, JSON_KEY_TIPO_ACCION));		
										
							
							listDocOblEmpleado.add(documentoObligatorioEmpleado);
						}
						
					}
					
					List<TipoCertificacionExternaEquipo> listCertExternaEquipo = new ArrayList<TipoCertificacionExternaEquipo>();
					if( jItems.getString("CertificacionesExternasEquipo").equals("null")){
						
						Log.d(LOGTAG, "No hay datos en CertificacionesExternasEquipo");
						
					}else{
	
						Log.d(LOGTAG, "Ingresando CertificacionesExternasEquipo");
						
						JSONObject jObjectCertExternasEquipo = jItems.getJSONObject("CertificacionesExternasEquipo");
						JSONArray jArrayCertExternasEquipo =  jObjectCertExternasEquipo.getJSONArray("$values");
						
						for(int c = 0; c < jArrayCertExternasEquipo.length(); c++){
							
							JSONObject jItemsCertExternaEquipo = jArrayCertExternasEquipo.getJSONObject(c);
							
							TipoCertificacionExternaEquipo certificacionExternaEquipo = new TipoCertificacionExternaEquipo();
							
//							Log.e(LOGTAG, "Insertando TipoCertificacionExternaEquipo ID Tipo Cert: " + getInt(jItemsCertExternaEquipo, JSON_KEY_ID_TIPO_CERT_EXT_EQUIPO));
//							Log.e(LOGTAG, "Insertando TipoCertificacionExternaEquipo ID Pauta: " + getInt(jItemsCertExternaEquipo, JSON_KEY_ID_PAUTACERTIFICACION));
//							Log.e(LOGTAG, "Insertando TipoCertificacionExternaEquipo Nombre: " + getString(jItemsCertExternaEquipo, JSON_KEY_NOMBRE));
							
							certificacionExternaEquipo.setIdTipoCertificacionExternaEquipo(getInt(jItemsCertExternaEquipo, JSON_KEY_ID_TIPO_CERT_EXT_EQUIPO));
							certificacionExternaEquipo.setIdPautaCertificacion(getInt(jItemsCertExternaEquipo, JSON_KEY_ID_PAUTACERTIFICACION));
							certificacionExternaEquipo.setNombre(getString(jItemsCertExternaEquipo, JSON_KEY_NOMBRE));
							certificacionExternaEquipo.setOrden(getInt(jItemsCertExternaEquipo, JSON_KEY_ORDEN));
							certificacionExternaEquipo.setTipoAccion(getInt(jItemsCertExternaEquipo, JSON_KEY_TIPO_ACCION));
							certificacionExternaEquipo.setTipoVencimiento(getInt(jItemsCertExternaEquipo, JSON_KEY_TIPO_VENCIMIENTO));
																							
							listCertExternaEquipo.add(certificacionExternaEquipo);
						}
					}
					
					pautasCertificacion = new PautasCertificacion(getInt(jItems, JSON_KEY_ID_PAUTACERTIFICACION),
																  getInt(jItems, JSON_KEY_ID_CENTROCERTIFICADOR), 
																  getString(jItems, JSON_KEY_NOMBRE),
																  getString(jItems, JSON_KEY_DESCRIPCION), 
																  getInt(jItems, JSON_KEY_TIPOCERTIFICACION),
																  getInt(jItems, JSON_KEY_CANTIDAD_INSTANCIAS), 
																  getBoolean(jItems, JSON_KEY_INCLUYE_OBSERVACION_POR_PREGUNTA), 
																  getBoolean(jItems, JSON_KEY_INCLUYE_NO_APLICA), 
																  getString(jItems, JSON_KEY_REVISION), 
																  DataInitializer_Parser.getLogoImage(getString(jItems, JSON_KEY_IMG_LOGO_CERTIFICACION)),
																  getString(jItems, JSON_KEY_VENCIMIENTO_KMHM), 
																  getInt(jItems, JSON_KEY_TIPO_ACCION), 
																  getBoolean(jItems, JSON_KEY_IncluyeNumeroNivelAprobacion),
																  listGrupoPauta, 
																  listItemPauta, 
																  listDocOblEquipo, 
																  listDocOblEmpleado, 
																  listCertExternaEquipo);
					
					db.insertPautasCertificaciones(pautasCertificacion);
					
					
				}
				
			}
			db.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

}
